import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:flutter/material.dart';

class AppTextFieldWithIcon extends StatefulWidget {
  double fontSize = 12;
  FontWeight fontWeight = FontWeight.normal;
  TextEditingController? textEditingController;
  TextInputType? keyboardType;
  String? hintText;
  double? letterSpacing;
  double width;
  double height;
  String? icon;
  bool? obscureText;

  AppTextFieldWithIcon(
      {super.key,
      this.fontSize = 18,
      this.fontWeight = FontWeight.normal,
      this.keyboardType,
      this.hintText,
      this.width = 279,
      this.height = 48,
        this.icon,
        this.obscureText,
      this.textEditingController,
      this.letterSpacing});

  @override
  State<StatefulWidget> createState() {
    return _AppTextFieldWithIconState();
  }
}

class _AppTextFieldWithIconState extends State<AppTextFieldWithIcon> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      alignment: Alignment.center,
      child: TextField(
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        maxLines: widget.keyboardType == TextInputType.multiline ? 4 : 1,
        cursorColor: AppColors.primary,
        textAlign: widget.keyboardType == TextInputType.multiline
            ? TextAlign.left
            : TextAlign.left,
        autofocus: false,
        obscureText: widget.obscureText ?? false,
        decoration: InputDecoration(
          filled: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 16, minWidth: 50),
          prefixIcon: AppImage(
            widget.icon!,
            width: 19,
          ),
          fillColor: AppColors.inputFill,
          hintText: widget.hintText,
          hintStyle: const TextStyle(color: Colors.black54),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(65.0),
              borderSide: BorderSide.none),
          contentPadding:
              const EdgeInsets.only(left: 20, bottom: 5, right: 10, top: 0),
        ),
        style: TextStyle(
            fontWeight: widget.fontWeight,
            fontSize: widget.fontSize,
            letterSpacing: widget.letterSpacing,
            color: Colors.black),
      ),
    );
  }
}
