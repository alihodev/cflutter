import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AppLikeWidget extends StatefulWidget {
  bool show;
  Function onHide;
  AppLikeWidget({Key? key, required this.show, required this.onHide}) : super(key: key);

  @override
  State<AppLikeWidget> createState() => _AppLikeWidgetState();
}

class _AppLikeWidgetState extends State<AppLikeWidget> {
  double _currentValue = 20;
  double _opacity = 0;
  double _width = 0;
  void  showToast() {
    setState(() {
      _opacity = 1;
      _width = 40;
    });
    Future.delayed(const Duration(milliseconds: 5000)).then((value) {
      setState(() {
        _opacity = 0;
        _width = 0;
      });
      widget.onHide();
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.show) {
      showToast();
    }
  }

  @override
  void didUpdateWidget(covariant AppLikeWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.show) {
      showToast();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: AnimatedOpacity(
        opacity: _opacity,
        duration: const Duration(milliseconds: 200),
        child: SizedBox(
          height: 282,
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 150),
            width: _width,
            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(24),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                      color: Colors.black12.withOpacity(0.1),
                      border: Border.all(width: 1,color: AppColors.borderColor),
                      borderRadius: BorderRadius.circular(24)),
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          AppImage("ic_like_plus.png",width: 20, color: AppColors.primary,),
                          const Spacer(),
                          AppImage("ic_like_mines.png", width: 20,color: AppColors.primary,)
                        ],
                      ),
                      RotatedBox(
                        quarterTurns: 3,
                        child:  SliderTheme(
                          data: const SliderThemeData(
                            trackHeight: 3,
                            thumbShape: RoundSliderThumbShape(
                                disabledThumbRadius: 5,
                                enabledThumbRadius: 7
                            ),
                            thumbColor: AppColors.primary,
                          ),
                          child: Slider(
                            value: _currentValue,
                            min: 0,
                            max: 100,
                            activeColor: AppColors.primary,
                            inactiveColor: Colors.white,

                            label: _currentValue.round().toString(),
                            onChanged: (double value) {
                              setState(() {
                                _currentValue = value;
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ) ,)
    );
  }
}
