import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class StepLine extends StatefulWidget {
  StepLineState stepLineState;
  StepLine({Key? key, required this.stepLineState})
      : super(key: key);

  @override
  State<StepLine> createState() => _StepState();
}

class _StepState extends State<StepLine> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _getViewByState(),
        const SizedBox(height: 22,)
      ],
    );
  }

  Widget _getViewByState() {
    switch(widget.stepLineState) {
      case StepLineState.done:
        return Container(
          width: double.maxFinite,
            height: 2,
            decoration: BoxDecoration(color: AppColors.primary, borderRadius: BorderRadius.circular(40)),
        );
      case StepLineState.todo:
        return Container(
            height: 2,
            width: double.maxFinite,
            decoration: BoxDecoration(color: const Color(0xffD9D9D9), borderRadius: BorderRadius.circular(40)),
        );
      default:
        return Container();
    }
  }
}

enum StepLineState { done, todo }
