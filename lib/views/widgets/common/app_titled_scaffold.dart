import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppTitledScaffold extends StatefulWidget {
  String title;
  double titleFontSize;
  Widget body;
  Widget? rightSideWidget;
  Function? rightSideWidgetAction;
  bool isLogo;
  bool logoWithTitle;

  AppTitledScaffold(
      {Key? key,
      required this.title,
      required this.body,
        this.titleFontSize = 24,
      this.rightSideWidget,
      this.rightSideWidgetAction,
        this.logoWithTitle = false,
      this.isLogo = false})
      : super(key: key);

  @override
  State<AppTitledScaffold> createState() => _AppTitledScaffoldState();
}

class _AppTitledScaffoldState extends State<AppTitledScaffold> {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        body: Column(
      children: [
        Container(
          height: 65,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            color: AppColors.actionBarColor,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 8,
                offset: const Offset(0, 4),
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  width: 50,
                  height: 50,
                  padding: const EdgeInsets.all(15),
                  child: AppImage(
                    "ic_arrow_left.png",
                    color: AppColors.h1TextColor,
                  ),
                ),
              ),
              if (widget.isLogo)
                AppImage("logo.png", width: 40, height: 44,),
              _getTitle(),

              InkWell(
                  onTap: () {
                    if (widget.rightSideWidgetAction != null) {
                      widget.rightSideWidgetAction!();
                    }
                  },
                  child: SizedBox(
                    width: 50,
                    child: (widget.rightSideWidget != null)
                        ? widget.rightSideWidget
                        : Container(),
                  ))
            ],
          ),
        ),
        Expanded(child: widget.body)
      ],
    ));
  }

  Widget _getTitle() {
    if (widget.logoWithTitle) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppImage(
            "logo.png",
            height: 20,
          ),
          FittedBox(
            fit: BoxFit.fill,
            child: AppText(widget.title, fontSize: 11, fontWeight: FontWeight.bold,),
          ),
        ],
      );
    }
    if (!widget.isLogo)
      return AppText.h1(
        widget.title,
        fontSize: widget.titleFontSize,
        fontWeight: FontWeight.w500,
      );
    return Container();
  }
}
