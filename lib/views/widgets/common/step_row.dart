import 'package:challenger/views/widgets/common/step_circle.dart';
import 'package:challenger/views/widgets/common/step_line.dart';
import 'package:flutter/material.dart';

class StepRow extends StatefulWidget {
  int stepIndex;
  StepRow({Key? key, required this.stepIndex}) : super(key: key);

  @override
  State<StepRow> createState() => _StepRowState();
}

class _StepRowState extends State<StepRow> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        StepCircle(title: "Step1", stepCircleState: _getState(1, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(2, widget.stepIndex)),),
        StepCircle(title: "Step2", stepCircleState: _getState(2, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(3, widget.stepIndex)),),
        StepCircle(title: "Step3", stepCircleState: _getState(3, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(4, widget.stepIndex)),),
        StepCircle(title: "Step4", stepCircleState: _getState(4, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(5, widget.stepIndex)),),
        StepCircle(title: "Step5", stepCircleState: _getState(5, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(6, widget.stepIndex)),),
        StepCircle(title: "Step6", stepCircleState: _getState(6, widget.stepIndex)),
        Expanded(child: StepLine(stepLineState: _getLineState(7, widget.stepIndex)),),
        StepCircle(title: "Step7", stepCircleState: _getState(7, widget.stepIndex)),
      ],
    );
  }

  StepCircleState _getState(index, currentIndex) {
    if (currentIndex == index) {
      return StepCircleState.doing;
    } else if (currentIndex > index) {
      return StepCircleState.done;
    } else {
      return StepCircleState.todo;
    }
  }

  StepLineState _getLineState(index, currentIndex) {
    if (currentIndex >= index) {
      return StepLineState.done;
    } else {
      return StepLineState.todo;
    }
  }
}
