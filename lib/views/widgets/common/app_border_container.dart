import 'package:challenger/constants/app_colors.dart';
import 'package:flutter/material.dart';

class AppBorderContainer extends StatefulWidget {
  Widget child;
  double borderRadius;
  double height;
  double? width;
  EdgeInsetsGeometry? padding;
  AppBorderContainer({Key? key, required this.child, this.borderRadius = 65, this.height = 48, this.width, this.padding = const EdgeInsets.symmetric(horizontal: 15,vertical: 5,)}) : super(key: key);

  @override
  State<AppBorderContainer> createState() => _AppBorderContainerState();
}

class _AppBorderContainerState extends State<AppBorderContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      padding: widget.padding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.borderRadius),
        border: Border.all(color: AppColors.borderColor),
      ),
      child: widget.child,);
  }
}
