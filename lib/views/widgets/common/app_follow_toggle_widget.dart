import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AppFollowToggle extends StatefulWidget {
  CoAndChallengerModel? coAndChallengeModel;
  ChallengeModel? challengeModel;
  Function follow;
  Function unfollow;
  AppFollowToggle({Key? key, required this.follow, required this.unfollow}) : super(key: key);

  AppFollowToggle.challenger(this.coAndChallengeModel, {super.key, required this.follow, required this.unfollow});
  AppFollowToggle.challenge(this.challengeModel, {super.key, required this.follow, required this.unfollow});

  @override
  State<AppFollowToggle> createState() => _AppFollowToggleState();
}

class _AppFollowToggleState extends State<AppFollowToggle> {
  void toggle() {
    if (widget.coAndChallengeModel != null) {
      widget.coAndChallengeModel!.follow = !(widget.coAndChallengeModel?.follow ?? false);
      if (widget.coAndChallengeModel?.follow ?? false) {
        widget.follow();
      } else {
        widget.unfollow();
      }
      UserRepository().challengerFollowToggle(widget.coAndChallengeModel?.id ?? 0,
          AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
    }

    if (widget.challengeModel != null) {
      widget.challengeModel!.follow = !(widget.challengeModel?.follow ?? false);
      if (widget.challengeModel?.follow ?? false) {
        widget.follow();
      } else {
        widget.unfollow();
      }
      ChallengeRepository().challengeFollowToggle(widget.challengeModel?.id ?? 0,
          AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
    }
    setState(() {

    });
  }

  bool isFollow() {
    if (widget.coAndChallengeModel != null) {
      return widget.coAndChallengeModel?.follow ?? false;
    }
    if (widget.challengeModel != null) {
      return widget.challengeModel?.follow ?? false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.coAndChallengeModel != null) {
      if (widget.coAndChallengeModel?.id == AppStorage.getInstance().getStaticUser().id) {
        return Container();
      }
    }
    return InkWell(
      onTap: () {
        toggle();
      },
      child: AppButton(
          text: isFollow() ? "Unfollow" : "Follow",
          width: 90,
          height: 28,
          fontSize: 10,
          fontWeight: FontWeight.w500,
          onPressed: () {
            toggle();
          }),
    );
    ;
  }
}
