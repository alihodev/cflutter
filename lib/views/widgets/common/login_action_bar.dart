import 'package:challenger/views/widgets/app_image.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class LoginActionBar extends StatelessWidget {
  const LoginActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      height: 75,
      child: Row(
        children: [
          InkWell(
            onTap: () {
              Get.back();
            },
            child: AppImage(
              "ic_arrow_left.png",
              width: 19.2,
              height: 19.2,
            ),
          ),
          Expanded(child: Center(child: AppImage(
            "logo.png",
            width: 40,
          ),)),

          SizedBox(width: 20,)
        ],
      ),
    );
  }
}
