import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AppLikeToggle extends StatefulWidget {
  CoAndChallengerModel? coAndChallengeModel;
  DailyReportModel? dailyReportModel;
  ChallengeModel? challengeModel;
  bool fromVideo = false;
  AppLikeToggle({Key? key}) : super(key: key);

  AppLikeToggle.challenger(this.coAndChallengeModel, {this.fromVideo = false,super.key});
  AppLikeToggle.dailyReport(this.dailyReportModel, {this.fromVideo = false,super.key});
  AppLikeToggle.challenge(this.challengeModel, {this.fromVideo = false,super.key});

  @override
  State<AppLikeToggle> createState() => _AppLikeToggleState();
}

class _AppLikeToggleState extends State<AppLikeToggle> {
  void toggle() {
    if (widget.coAndChallengeModel != null) {
      widget.coAndChallengeModel!.like = !(widget.coAndChallengeModel?.like ?? false);
      if (!widget.coAndChallengeModel!.like!) {
        widget.coAndChallengeModel?.likes = widget.coAndChallengeModel!.likes! - 1;
      } else {
        widget.coAndChallengeModel?.likes = widget.coAndChallengeModel!.likes! + 1;
      }
      UserRepository().challengerLikeToggle(widget.coAndChallengeModel?.id ?? 0,
          AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
    }
    if (widget.dailyReportModel != null) {
      widget.dailyReportModel!.like = !(widget.dailyReportModel?.like ?? false);
      if (!widget.dailyReportModel!.like!) {
        widget.dailyReportModel?.likes = widget.dailyReportModel!.likes! - 1;
      } else {
        widget.dailyReportModel?.likes = widget.dailyReportModel!.likes! + 1;
      }
      UserRepository().dailyReportLikeToggle(widget.dailyReportModel?.id ?? 0,
          AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
    }
    if (widget.challengeModel != null) {
      widget.challengeModel!.like = !(widget.challengeModel?.like ?? false);
      if (!widget.challengeModel!.like!) {
        widget.challengeModel?.likes = widget.challengeModel!.likes! - 1;
      } else {
        widget.challengeModel?.likes = widget.challengeModel!.likes! + 1;
      }
      ChallengeRepository().challengeLikeToggle(widget.challengeModel?.id ?? 0,
          AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
    }
    setState(() {

    });
  }

  int likesCount() {
    if (widget.coAndChallengeModel != null) {
      return widget.coAndChallengeModel?.likes ?? 0;
    }
    if (widget.dailyReportModel != null) {
      return widget.dailyReportModel?.likes ?? 0;
    }
    if (widget.challengeModel != null) {
      return widget.challengeModel?.likes ?? 0;
    }
    return 0;
  }

  bool isLike() {
    if (widget.coAndChallengeModel != null) {
      return widget.coAndChallengeModel?.like ?? false;
    }
    if (widget.dailyReportModel != null) {
      return widget.dailyReportModel?.like ?? false;
    }
    if (widget.challengeModel != null) {
      return widget.challengeModel?.like ?? false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: 40, child: InkWell(
      onTap: () {
        toggle();
      },
      child: Column(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: AppImage(isLike() ? "ic_like_plus.png" : "ic_favorite_empty.png", color: _getColor(),),
          ),
          const SizedBox(
            height: 4,
          ),
          AppText(
            "${likesCount()} ${likesCount() > 1 ? "Likes" : "Like"}",
            color: widget.fromVideo ? Colors.white : null,
            fontSize: widget.fromVideo ? 12 : 9,
          ),
        ],
      ),
    ),);
  }
  Color? _getColor() {
    if (!isLike() && widget.fromVideo) {
      return Colors.white;
    }
    return null;
  }
}
