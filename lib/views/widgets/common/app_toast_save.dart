import 'dart:ui';

import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AppToastSave extends StatefulWidget {
  bool show;
  Function onHide;
  AppToastSave({Key? key, required this.show, required this.onHide}) : super(key: key);

  @override
  State<AppToastSave> createState() => _AppToastSaveState();
}

class _AppToastSaveState extends State<AppToastSave> {
  double _opacity = 0;
  void  showToast() {
    setState(() {
      _opacity = 1;
    });
    Future.delayed(const Duration(milliseconds: 2000)).then((value) {
      setState(() {
        _opacity = 0;
      });
      widget.onHide();
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.show) {
      showToast();
    }
  }

  @override
  void didUpdateWidget(covariant AppToastSave oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.show) {
      showToast();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: AnimatedOpacity(
        opacity: _opacity,
        duration: const Duration(milliseconds: 200),
        child: Container(
        margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(24),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white10.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(24)),

              child:  Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(18),
                    child: Image.network(
                      "https://cdn0.tnwcdn.com/wp-content/blogs.dir/1/files/2019/04/adobe_selling_air.jpg",
                      height: 48,
                      width: 48,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  AppText(
                    "Saved",
                    fontWeight: FontWeight.w600,
                    color: const Color(0xff484848),
                  ),
                  const Spacer(),
                  AppText(
                    "Add to Group",
                    fontWeight: FontWeight.w600,
                    color: const Color(0xff484848),
                  ),
                ],
              ),
            ),
          ),
        ),
      ) ,)
    );
  }
}
