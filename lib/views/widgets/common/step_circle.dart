import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class StepCircle extends StatefulWidget {
  StepCircleState stepCircleState;
  String title;

  StepCircle({Key? key, required this.title, required this.stepCircleState})
      : super(key: key);

  @override
  State<StepCircle> createState() => _StepState();
}

class _StepState extends State<StepCircle> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _getViewByState(),
        const SizedBox(
          height: 5,
        ),
        _getTitleByState()
      ],
    );
  }

  Widget _getViewByState() {
    switch(widget.stepCircleState) {
      case StepCircleState.done:
        return Container(
            width: 32,
            height: 32,
            padding: const EdgeInsets.all(3),
            child: Container(
              width: 24,
              height: 24,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: AppColors.primary, width: 2)
              ),
              child: Container(
                padding: const EdgeInsets.all(4),
                child: AppImage("ic_tik.png"),
              ),
            )
        );
      case StepCircleState.doing:
        return Container(
            width: 32,
            height: 32,
            padding: const EdgeInsets.all(3),
            decoration: BoxDecoration(
                color: const Color(0xfff4ecff),borderRadius: BorderRadius.circular(50)),
            child: Container(
              width: 24,
              height: 24,
              padding: const EdgeInsets.all(7),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: AppColors.primary, width: 1.5),
                  color: const Color(0xffB0C2E5)
              ),
              child: Container(
                width: 8,
                height: 8,
                decoration: BoxDecoration(color: AppColors.primary, borderRadius: BorderRadius.circular(50)),
              ),
            )
        );
      case StepCircleState.todo:
        return Container(
            width: 32,
            height: 32,
            padding: const EdgeInsets.all(3),
            decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(50)),
            child: Container(
              width: 24,
              height: 24,
              padding: const EdgeInsets.all(7),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: const Color(0xffD9D9D9), width: 1.5),
              ),
              child: Container(
                width: 8,
                height: 8,
                decoration: BoxDecoration(color: const  Color(0xffD9D9D9), borderRadius: BorderRadius.circular(50)),
              ),
            )
        );
      default:
        return Container();
    }
  }

  Widget _getTitleByState() {
    switch (widget.stepCircleState) {
      case StepCircleState.done:
        return AppText(
          widget.title,
          color: AppColors.primary,
          fontSize: 10,
          fontWeight: FontWeight.w700,
        );
      case StepCircleState.doing:
        return AppText(
          widget.title,
          color: AppColors.primary,
          fontSize: 10,
          fontWeight: FontWeight.w500,
        );
      case StepCircleState.todo:
        return AppText(
          widget.title,
          color: const Color(0xffD9D9D9),
          fontSize: 10,
          fontWeight: FontWeight.w500,
        );
      default:
        return AppText(widget.title,
            color: AppColors.primary,
            fontSize: 10,
            fontWeight: FontWeight.w700);
    }
  }
}

enum StepCircleState { done, doing, todo }
