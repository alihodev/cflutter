import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:flutter/material.dart';

class AppBuildChallengeTextField extends StatefulWidget {
  String title;
  String label;
  String? hintText;
  String? value;
  Function(String value) onValueChange;

  AppBuildChallengeTextField(
      {Key? key,
      required this.title,
      required this.label,
      required this.onValueChange,
      this.hintText,
      this.value})
      : super(key: key);

  @override
  State<AppBuildChallengeTextField> createState() =>
      _AppBuildChallengeTextFieldState();
}

class _AppBuildChallengeTextFieldState
    extends State<AppBuildChallengeTextField> {
  TextEditingController controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    controller.text = widget.value ?? "";
    controller.addListener(() {
      widget.onValueChange(controller.text);
    });
  }

  @override
  void didUpdateWidget(covariant AppBuildChallengeTextField oldWidget) {
    super.didUpdateWidget(oldWidget);
    controller.text = widget.value ?? "";
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.title.isNotEmpty)
          Container(
            alignment: Alignment.centerLeft,
            child: AppText(
              widget.title,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              color: const Color(0xff1D1D1D),
            ),
          ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: AppText(
            widget.label,
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: const Color(0xff484848),
          ),
        ),
        AppTextField(
          textEditingController: controller,
          keyboardType: TextInputType.multiline,
          hintText: widget.hintText,
          height: 128,
          borderRadius: 8,
          fillColor: const Color(0xfff9fafb),
        ),
        const SizedBox(
          height: 40,
        ),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
}
