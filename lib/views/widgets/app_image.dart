import 'package:flutter/material.dart';

class AppImage extends StatefulWidget {
  double? width;
  double? height;
  Color? color;
  String image;
  BoxFit? fit;

  AppImage(this.image, {this.width, this.height, this.color, this.fit});

  AppImage.asset(this.image, {this.width, this.height, this.color, this.fit});

  @override
  State<StatefulWidget> createState() {
    return _AppImageState();
  }
}

class _AppImageState extends State<AppImage> {
  @override
  Widget build(BuildContext context) {
    return _image()!;
  }

  Widget? _image() {
    if (!widget.image.startsWith("http")) {
      return Image.asset(
        "assets/images/${widget.image}",
        width: widget.width,
        height: widget.height,
        color: widget.color,
        fit: widget.fit,
      );
    }
    return null;
  }
}
