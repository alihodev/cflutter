import 'dart:convert';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/splash/splash_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class AppPaginatedListView<T> extends StatefulWidget {
  String url;
  Widget Function(T item) renderItem;
  List<T> Function(dynamic data) onFetchData;
  AppPaginatedListListener? listener;
  bool isFake;
  int page = 0;
  int limit;
  String query;
  String category;
  String requestParams;

  AppPaginatedListView(
      {Key? key,
      required this.url,
      required this.renderItem,
      required this.onFetchData,
      this.isFake = false,
      this.limit = 10,
      this.query = "",
        this.requestParams = "",
        this.listener,
      this.category = ""})
      : super(key: key);

  @override
  State<AppPaginatedListView> createState() => _AppPaginatedListViewState<T>();
}

class _AppPaginatedListViewState<T> extends State<AppPaginatedListView<T>> {
  final ScrollController _scrollController = ScrollController();
  bool canLoadMore = true;
  List<T> items = [];
  bool isLoading = false;
  bool isRetry = false;
  String errorMessage = "";
  String previousQuery = "";
  String previousCategory = "";

  @override
  void initState() {
    super.initState();
    onPageChange();
    _scrollController.addListener(_onScroll);
    canLoadMore = false;
    _loadMoreData();
  }

  void onPageChange() {
    if (widget.listener != null) {
      if (widget.listener!.onPageChange != null) {
        widget.listener!.onPageChange!(widget.page + 1);
      }
    }
  }

  void onReset() {
    if (widget.listener != null) {
      if (widget.listener!.onReset != null) {
        widget.listener!.onReset!();
      }
    }
  }

  @override
  void didUpdateWidget(covariant AppPaginatedListView<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.query != previousQuery) {
      previousQuery = widget.query;
      widget.page = 0;
      onPageChange();
      onReset();
      setState(() {
        items.clear();
      });
      _loadMoreData();
    }

    if (widget.category != previousCategory) {
      previousCategory = widget.category;
      widget.page = 0;
      onPageChange();
      onReset();
      setState(() {
        items.clear();
      });
      _loadMoreData();
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (canLoadMore) {
        canLoadMore = false;
        if (items.length % widget.limit == 0) {
          _loadMoreData();
        }
      }
    }
  }

  Future<void> _loadMoreData() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
    }
    if (!isRetry) {
      widget.page += 1;
      onPageChange();
    }

    try {
      var response = await _getData();
      setState(() {
        items.addAll(widget.onFetchData(response));
        isRetry = false;
        isLoading = false;
        canLoadMore = true;
      });
    } catch (e) {
      setState(() {
        errorMessage = e.toString();
        isRetry = true;
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isRetry && items.isEmpty && errorMessage != "") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppText.body(errorMessage.toString().replaceAll("Exception: ", "")),
          AppButton.flat(
              text: "Try again",
              onPressed: () {
                errorMessage = "";
                _loadMoreData();
              })
        ],
      );
    }
    return ListView.builder(
      itemCount: items.length + (isLoading ? 1 : 0),
      itemBuilder: (context, index) {
        if (index == items.length) {
          return const Center(child: SpinKitThreeBounce(
            color: AppColors.primary,
            size: 20.0,
          ));
        } else {
          return widget.renderItem(items[index]);
        }
      },
      controller: _scrollController,
    );
  }

  Future<dynamic> _getData() async {
    if (widget.isFake) {
      await Future.delayed(const Duration(milliseconds: 1000));
      if (widget.page == 1) {
        return jsonDecode("[{},{},{},{},{},{},{},{},{},{}]");
      }
      return jsonDecode("[{},{},{},{},{}]");
    }
    var data = await HttpService().get(
        '${widget.url}?page=${widget.page}&query=${widget.query}&limit=${widget.limit}&category=${widget.category}&${widget.requestParams}',
        onSuccess: (data) {
      return data['data'];
    }, onFailure: (message) {
      throw Exception(message);
    }, onUnAuthorized: () {
      AppStorage.getInstance().setUserLogout();
      Get.offAll(SplashPage());
    });
    if (data.statusCode == 200) return jsonDecode(data.body)['data'];
  }
}

class AppPaginatedListListener {
  Function(int page)? onPageChange;
  Function()? onReset;
  AppPaginatedListListener({this.onPageChange, this.onReset});
}