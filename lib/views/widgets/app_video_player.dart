import 'dart:io';

import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:get/get.dart';

class AppVideoPlayer extends StatefulWidget {
  File? file;
  String? url;
  bool enableProgressBar;
  bool fromReels;
  bool dailyDemo = false;
  bool loop = false;
  DailyReportModel? dailyReportModel;
  Function(VideoPlayerController videoPlayerController)? controller;

  AppVideoPlayer.playFile(
      {required this.file,
      this.enableProgressBar = false,
      this.fromReels = false});

  AppVideoPlayer.play(
      {required this.url,
      this.enableProgressBar = false,
        this.controller,
      this.fromReels = false});

  AppVideoPlayer.playDailyDemo(
      {required this.url,
      this.enableProgressBar = false,
      this.fromReels = false,
      this.dailyDemo = true,
        this.dailyReportModel,
        this.controller,
      this.loop = true});

  @override
  _AppVideoPlayerState createState() => _AppVideoPlayerState();
}

class _AppVideoPlayerState extends State<AppVideoPlayer> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    if (widget.file != null) {
      _controller = VideoPlayerController.file(widget.file!)
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
        });
    }
    if (widget.url != null) {
      _controller = VideoPlayerController.networkUrl(Uri.parse(widget.url!))
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
          _controller.play();
        });
      if (widget.controller != null) {
        widget.controller!(_controller);
      }
      _controller.addListener(() {
        setState(() {
          _controller.setLooping(widget.loop);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: () async {
      if (widget.dailyDemo) {
        await _controller.pause();
        await Get.to(VideoPlayerPage.playDailyReportVideo(
           widget.dailyReportModel));
        _controller.play();
      }
    },child: Stack(
      children: [
        Center(
          child: _controller.value.isInitialized
              ? AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: VideoPlayer(_controller),
          )
              : Container(),
        ),
        if (!widget.dailyDemo)
          Center(
            child: InkWell(
              onTap: () {
                setState(() {
                  _controller.value.isPlaying
                      ? _controller.pause()
                      : _controller.play();
                });
              },
              child: _controller.value.isPlaying ? Container(width: double.maxFinite ,height: double.maxFinite,decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(65)),
                ) : Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.white60,
                    borderRadius: BorderRadius.circular(65)),
                child: const Icon(
                  Icons.play_arrow,
                ),
              ),
            ),
          ),
        if (widget.enableProgressBar)
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 10, left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  VideoProgressIndicator(
                    _controller,
                    allowScrubbing: true,
                    colors: const VideoProgressColors(
                        playedColor: Color(0xff5099F4),
                        backgroundColor: Color(0xff323F4B),
                        bufferedColor: Color(0xff52606D)),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      AppText(
                        '${_controller.value.position.inMinutes}:${_controller.value.position.inSeconds}',
                        color: Colors.white,
                        fontSize: 12,
                      ),
                      const Spacer(),
                      AppText(
                        '${_controller.value.duration.inMinutes}:${_controller.value.duration.inSeconds}',
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
      ],
    ),);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
