import 'dart:convert';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/splash/splash_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class AppMagicFutureBuilder extends StatefulWidget {
  Widget? loading;
  String url;
  Type? classModel;
  bool isFake;
  Widget Function(dynamic response) onSuccess;

  AppMagicFutureBuilder(
      {Key? key,
      required this.url,
      required this.onSuccess,
      this.isFake = false})
      : super(key: key);

  @override
  State<AppMagicFutureBuilder> createState() => _AppMagicFutureBuilderState();
}

class _AppMagicFutureBuilderState extends State<AppMagicFutureBuilder> {
  var _data;
  bool isLoading = false;
  bool isRetry = false;
  String errorMessage = "";
  String previousUrl = "";
  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  @override
  void didUpdateWidget(covariant AppMagicFutureBuilder oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("here");
    if (widget.url != previousUrl) {
    print("here2");
      previousUrl = widget.url;
      _data = null;
      isLoading = true;
      _fetchData();
    }
  }

  Future<void> _fetchData() async {
    try {
      _data = await _getData();
      setState(() {

      });
      isRetry = false;
      isLoading = false;
    } catch (e) {
      setState(() {
        errorMessage = e.toString();
        isRetry = true;
        isLoading = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    if (isRetry && errorMessage != "") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppText.body(errorMessage.toString().replaceAll("Exception: ", "")),
          AppButton.flat(
              text: "Try again",
              onPressed: () {
                setState(() {

                errorMessage = "";
                isRetry = false;
                });
                _fetchData();
              })
        ],
      );
    }
    if (_data != null) {
      return widget.onSuccess(_data);
    }
    return const Center(child: SpinKitThreeBounce(
      color: AppColors.primary,
      size: 20.0,
    ));
  }

  Future<dynamic> _getData() async {
    if (widget.isFake) {
      await Future.delayed(const Duration(seconds: 1));
      return jsonDecode("[{},{},{},{},{},{}]");
    }
    var data = await HttpService().get(widget.url, onSuccess: (data) {
      return data['data'];
    }, onFailure: (message) {
      throw Exception(message);
    }, onUnAuthorized: () {
      AppStorage.getInstance().setUserLogout();
      Get.offAll(SplashPage());
    });
    if (data.statusCode == 200) return jsonDecode(data.body)['data'];
  }
}
