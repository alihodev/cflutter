import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/pages/splash/splash_page.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppMagicButton<T> extends StatefulWidget {
  double fontSize = 12;
  FontWeight fontWeight = FontWeight.normal;
  String text;
  Function getData;
  Function(String message) onDone;
  Function(Map<String, dynamic> response)? onDoneWithData;
  bool Function()? beforeProcess;
  double width;
  String? postUrl;
  bool isFake;
  T? data;
  double borderRadius;

  AppMagicButton({
    super.key,
    required this.text,
    required this.getData,
    required this.onDone,
    this.beforeProcess,
    this.onDoneWithData,
    this.fontSize = 16,
    this.fontWeight = FontWeight.w600,
    this.width = 270,
    this.isFake = false,
    this.borderRadius = 24,
    this.postUrl,
  });

  @override
  State<StatefulWidget> createState() {
    return _AppMagicButtonState();
  }
}

class _AppMagicButtonState extends State<AppMagicButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 48,
      width: widget.width,
      child: ElevatedButton(
        onPressed: onClick,
        style: ElevatedButton.styleFrom(
          backgroundColor:
          AppColors.primary, // background color of button
          shape: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.circular(widget.borderRadius), // border radius of button
          ),
        ),
        child: AppText(
          widget.text,
          color: Colors.white,
          fontWeight: widget.fontWeight,
          fontSize: widget.fontSize,
        ),
      ),
    );
  }

  void onClick() {
    if (widget.beforeProcess != null) {
      if (!widget.beforeProcess!.call()) {
        return;
      }
    }
    if (widget.postUrl != null) {
      post();
    }
  }

  void post() async {
    showLoading();
    if (widget.isFake) {
      await Future.delayed(const Duration(seconds: 1));
      hideLoading();
      widget.onDone("Message from fake done");
      if (widget.onDoneWithData != null) {
        Map<String, dynamic> map = <String, dynamic>{};
        widget.onDoneWithData!(map);
      }
      return;
    }
    HttpService().post(widget.postUrl!, body: widget.getData(),
        onSuccess: (response) {
      hideLoading();
      String message = "Action Successfully done";
      if (response["message"] != null) {
        message = response["message"];
      }
      widget.onDone(message);
      if (widget.onDoneWithData != null) {
        widget.onDoneWithData!(response);
      }
    }, onUnAuthorized: () {
      hideLoading();
      AppStorage.getInstance().setUserLogout();
      Get.offAll(SplashPage());
    },
      onFailure: (message) {
        hideLoading();
        showErrorMessage(message);
      },onForbidden: (message) {
          hideLoading();
          showErrorMessage(message);
        }
    );
  }

  void showLoading() {
    AppLoadingDialog.show();
  }

  void hideLoading() {
    AppLoadingDialog.hide();
  }

  void showErrorMessage(message) {
    AppErrorDialog.showErrorDialogWithRetry(message, onRetry: onClick);
  }
}
