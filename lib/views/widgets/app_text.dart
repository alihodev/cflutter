import 'package:challenger/constants/app_colors.dart';
import 'package:flutter/material.dart';

class AppText extends StatefulWidget {
  double fontSize = 12;
  FontWeight fontWeight = FontWeight.normal;
  String text;
  Color? color;
  TextAlign? textAlign;
  int? maxLines;
  TextOverflow? overFlow;

  AppText(this.text,
      {super.key,
      this.fontSize = 16,
      this.fontWeight = FontWeight.normal,
      this.textAlign,
        this.maxLines,
        this.overFlow,
      this.color = Colors.black});

  AppText.body(this.text,
      {super.key,
      this.fontSize = 16,
      this.fontWeight = FontWeight.normal,
      this.textAlign,
        this.maxLines,
        this.overFlow,
      this.color = AppColors.bodyTextColor});

  AppText.h1(this.text,
      {super.key,
      this.fontSize = 24,
      this.fontWeight = FontWeight.w700,
      this.textAlign,
        this.maxLines,
        this.overFlow,
      this.color = AppColors.h1TextColor});

  AppText.h2(this.text,
      {super.key,
      this.fontSize = 22,
      this.fontWeight = FontWeight.bold,
      this.textAlign,
        this.maxLines,
        this.overFlow,
      this.color = AppColors.h2TextColor});

  @override
  State<StatefulWidget> createState() {
    return _AppTextState();
  }
}

class _AppTextState extends State<AppText> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.text,
      textAlign: widget.textAlign,
      textWidthBasis: TextWidthBasis.parent,
      maxLines: widget.maxLines,
      overflow: widget.overFlow,
      style: TextStyle(
          fontWeight: widget.fontWeight,
          fontSize: widget.fontSize,
          height: 1.8,
          color: widget.color),
    );
  }
}
