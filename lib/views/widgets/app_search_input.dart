import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:flutter/material.dart';

class AppSearchTextField extends StatefulWidget {
  Function(String value) onChaneValue;
  double horizontalMargin;
  double borderRadius;
  String hintText;
  AppSearchTextField({Key? key, required this.onChaneValue,this.hintText = "Search", this.horizontalMargin = 10, this.borderRadius = 8}) : super(key: key);

  @override
  State<AppSearchTextField> createState() => _AppSearchTextFieldState();
}

class _AppSearchTextFieldState extends State<AppSearchTextField> {
  final _controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      // Handle text change here
      final text = _controller.text;
      widget.onChaneValue(text);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: EdgeInsets.symmetric(horizontal: widget.horizontalMargin, vertical: 5),
      child: TextField(
        controller: _controller,
        style: const TextStyle(fontSize: 16),
      decoration: InputDecoration(
        hintText: widget.hintText,
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.symmetric(vertical: 0),
        prefixIcon: Container(
          padding: const EdgeInsets.all(14),
          child: AppImage("ic_search.png", width: 10,height: 10,),),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: AppColors.primary, width: 2.0),
          borderRadius: BorderRadius.circular(widget.borderRadius),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(widget.borderRadius),
          borderSide: const BorderSide(color: Color(0xffD0D5DD)),
        ),
      ),
    ),);
  }
}
