import 'package:challenger/views/pages/challenge/build/build_challenge.dart';
import 'package:challenger/views/pages/clubs/clubs_page.dart';
import 'package:challenger/views/pages/passepartout/passepartout_page.dart';
import 'package:challenger/views/pages/prize/build/build_prize_page.dart';
import 'package:challenger/views/pages/profile/personal_profile_page.dart';
import 'package:challenger/views/pages/setting/setting_page.dart';
import 'package:get/get.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AppScaffold extends StatefulWidget {
  Widget body;
  Widget? floatingActionButton;
  Function(BuildContext context)? openDrawer;

  AppScaffold({super.key, required this.body, this.openDrawer, this.floatingActionButton});

  @override
  State<StatefulWidget> createState() {
    return _AppScaffoldState();
  }
}

class _AppScaffoldState extends State<AppScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffF4F4F4),
      // drawer: Drawer(
      //   width: 320,
      //   child: Column(
      //     children: [
      //       const SizedBox(
      //         height: 40,
      //       ),
      //       ListTile(
      //         onTap: () {
      //           Get.to(ProfilePage());
      //         },
      //         leading: const CircleAvatar(
      //           radius: 25,
      //           backgroundImage: NetworkImage(
      //               ''),
      //         ),
      //         title: AppText.body(
      //           "Olivia Rhye",
      //           fontSize: 14,
      //           color: const Color(0xff101828),
      //         ),
      //         subtitle: AppText.body(
      //           "@Rhye-Oliva",
      //           color: const Color(0xff667085),
      //         ),
      //       ),
      //       const SizedBox(height: 10),
      //       AppText("Name of the challenge",
      //           fontSize: 18, color: const Color(0xff101828)),
      //       const SizedBox(height: 20),
      //       const Divider(
      //         height: 1.5,
      //       ),
      //       const SizedBox(height: 20),
      //       Expanded(
      //         child: Container(
      //           padding: const EdgeInsets.symmetric(horizontal: 15),
      //           child: Column(
      //             children: [
      //               _clickable(
      //                   _iconTextButton("New Challenge", "ic_add.png",
      //                       size: 16),
      //                   () {
      //                     Get.to(BuildChallengePage());
      //                   }),
      //               _clickable(
      //                   _iconTextButton("New Prize", "ic_add.png", size: 16),
      //                   () {
      //                 Get.to(BuildPrizePage());
      //               }),
      //               const SizedBox(
      //                 height: 10,
      //               ),
      //               const Divider(
      //                 height: 2,
      //               ),
      //               const SizedBox(
      //                 height: 10,
      //               ),
      //               _clickable(
      //                   _iconTextButtonWithBackground(
      //                     "Passepartout",
      //                     "ic_passepartout.png",
      //                     "10",
      //                   ),
      //                       () {
      //                     Get.to(PassepartoutPage(initialTabIndex: 0));
      //                       }),
      //               _clickable(
      //                   _iconTextButtonWithBackground(
      //                     "Debate",
      //                     "ic_room.png",
      //                     "8"
      //                   ),
      //                       () {
      //                         Get.to(ClubsPage(initialTabIndex: 2,));
      //                       }),
      //               _clickable(
      //                   _iconTextButtonWithBackground(
      //                     "Rooms",
      //                     "ic_debate2.png",
      //                     "12"
      //                   ),
      //                       () {
      //                         Get.to(ClubsPage(initialTabIndex: 1,));
      //                       }),
      //               _clickable(
      //                   _iconTextButtonWithBackground(
      //                     "Chats",
      //                     "ic_chat.png",
      //                     "12"
      //                   ),
      //                       () {
      //                     Get.to(ClubsPage(initialTabIndex: 0,));
      //                       }),
      //               Expanded(child: Container()),
      //               _clickable(
      //                   _iconTextButton(
      //                     "Saved Challengers",
      //                     "ic_bookmark.png",
      //                   ),
      //                   () {}),
      //               _clickable(
      //                   _iconTextButton(
      //                     "Settings",
      //                     "ic_setting.png",
      //                   ),
      //                   () {
      //                     Get.to(SettingPage());
      //                   }),
      //               const SizedBox(
      //                 height: 10,
      //               ),
      //               Row(
      //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                 children: [AppText("Challenger"), AppText("V 0.1")],
      //               ),
      //               const SizedBox(
      //                 height: 20,
      //               ),
      //             ],
      //           ),
      //         ),
      //       )
      //     ],
      //   ),
      // ),
      floatingActionButton: widget.floatingActionButton,
      body: SafeArea(child: Container(color: Colors.white, child: widget.body)),
    );
  }

  Widget _iconTextButtonWithBackground(String text, String icon, String count,
      {double size = 20}) {
    return Container(
      decoration:  BoxDecoration(color: Color(0xfff9fafb), borderRadius: BorderRadius.circular(4)),
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          AppImage(
            icon,
            width: size,
            height: size,
          ),
          const SizedBox(
            width: 15,
          ),
          AppText(text),
          Expanded(child: Container()),
          Container(padding: const EdgeInsets.symmetric(horizontal: 10), decoration: BoxDecoration(color: const Color(0xffF2F4F7) , borderRadius: BorderRadius.circular(50)),child: AppText(count),),
         const SizedBox( width: 10,),
        ],
      ),
    );
  }

  Widget _iconTextButton(String text, String icon, {double size = 20}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          AppImage(
            icon,
            width: size,
            height: size,
          ),
          const SizedBox(
            width: 15,
          ),
          AppText(text)
        ],
      ),
    );
  }

  Widget _clickable(Widget child, Function onTap) {
    return InkWell(
      onTap: () {
        Get.back();
        onTap();
      },
      child: child,
    );
  }
}
