import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppButton extends StatefulWidget {
  double fontSize = 12;
  FontWeight fontWeight = FontWeight.normal;
  String text;
  VoidCallback onPressed;
  double width;
  double height;
  double borderRadius;
  bool isFlat = false;
  bool isFocused = false;
  bool isUnfocused = false;
  bool isNextButton = false;
  bool isPreviousButton = false;
  Color? textColor;
  Widget? icon;

  AppButton(
      {super.key,
      required this.text,
      required this.onPressed,
      this.fontSize = 16,
      this.fontWeight = FontWeight.w600,
      this.width = 311,
      this.borderRadius = 24,
      this.icon,
      this.height = 48});

  AppButton.flat(
      {required this.text,
      required this.onPressed,
      this.fontSize = 16,
      this.fontWeight = FontWeight.w700,
      this.width = 311,
      this.textColor = AppColors.primary,
      this.height = 48,
      this.borderRadius = 24,
      this.isFlat = true});

  AppButton.focused(
      {required this.text,
      required this.onPressed,
      this.fontSize = 14,
      this.fontWeight = FontWeight.w700,
      this.width = 140,
      this.height = 40,
      this.isNextButton = false,
      this.borderRadius = 8,
      this.textColor = AppColors.primary,
      this.isFocused = true});

  AppButton.unFocused(
      {required this.text,
      required this.onPressed,
      this.fontSize = 14,
      this.fontWeight = FontWeight.w700,
      this.width = 140,
      this.height = 40,
      this.borderRadius = 8,
      this.isPreviousButton = false,
      this.textColor = AppColors.primary,
      this.isFlat = false,
      this.isUnfocused = true});

  @override
  State<StatefulWidget> createState() {
    return _AppButtonState();
  }
}

class _AppButtonState extends State<AppButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      width: widget.width,
      child: widget.isFlat
          ? TextButton(
              onPressed: widget.onPressed,
              child:
                  AppText(
                    widget.text,
                    fontWeight: widget.fontWeight,
                    fontSize: widget.fontSize,
                    color: widget.textColor,
                  ),
            )
          : ElevatedButton(
              onPressed: widget.onPressed,
              style: ElevatedButton.styleFrom(
                  backgroundColor:
                      widget.isUnfocused ? Colors.white : AppColors.primary,
                  // background color of button
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                        width: (widget.isFocused || widget.isUnfocused) ? 1 : 0,
                        color: AppColors.borderColor),
                    borderRadius: BorderRadius.circular(
                        widget.borderRadius), // border radius of button
                  ),
                  shadowColor: widget.isFocused || widget.isUnfocused
                      ? Colors.transparent
                      : null),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (widget.icon != null)
                    Container(margin: const EdgeInsets.only(right: 10), child: widget.icon,),
                  if (widget.isPreviousButton)
                    Container(
                      margin: const EdgeInsets.only(right: 5),
                      child: const Icon(
                        Icons.arrow_back,
                        color: Color(0xff484848),
                        size: 20,
                      ),
                    ),
                  AppText(
                    widget.text,
                    color: widget.isUnfocused
                        ? const Color(0xff344054)
                        : Colors.white,
                    fontWeight: widget.fontWeight,
                    fontSize: widget.fontSize,
                  ),
                  if (widget.isNextButton)
                    Container(
                      margin: const EdgeInsets.only(left: 5),
                      child: const Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                        size: 20,
                      ),
                    )
                ],
              ),
            ),
    );
  }
}
