import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextField extends StatefulWidget {
  double fontSize = 12;
  FontWeight fontWeight = FontWeight.normal;
  TextEditingController? textEditingController;
  TextInputType? keyboardType;
  String? hintText;
  double? letterSpacing;
  double width;
  double height;
  String? icon;
  bool? obscureText;
  double borderRadius;
  Color borderColor;
  String? label;
  Color fillColor;
  bool enabled;
  bool denySpace;
  Function(String text)? onChange;

  AppTextField(
      {super.key,
        this.fontSize = 18,
        this.fontWeight = FontWeight.normal,
        this.keyboardType,
        this.hintText,
        this.width = double.maxFinite,
        this.height = 48,
        this.icon,
        this.obscureText,
        this.textEditingController,
        this.borderColor = const Color(0xffD0D5DD),
        this.borderRadius = 65,
        this.fillColor = Colors.white,
        this.label,
        this.onChange,
        this.enabled = true,
        this.denySpace = false,
        this.letterSpacing});

  AppTextField.round(
      {super.key,
        this.fontSize = 18,
        this.fontWeight = FontWeight.normal,
        this.keyboardType,
        this.hintText,
        this.width = double.maxFinite,
        this.height = 48,
        this.icon,
        this.obscureText,
        this.borderColor = const Color(0xffD0D5DD),
        this.textEditingController,
        this.borderRadius = 8,
        this.label,
        this.onChange,
        this.fillColor = Colors.white,
        this.enabled = true,
        this.denySpace = false,
        this.letterSpacing});

  @override
  State<StatefulWidget> createState() {
    return _AppTextFieldState();
  }
}

class _AppTextFieldState extends State<AppTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
      if (widget.label != null)
        Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.only(bottom: 5),
          child:
        AppText(widget.label!, fontSize: 14, textAlign: TextAlign.start,),),
     Container(
      width: widget.width,
      height: widget.height,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.borderRadius),
          border: Border.all(color: widget.borderColor)
      ),
      child: TextField(
        inputFormatters: [
          if (widget.denySpace)
          FilteringTextInputFormatter.deny(
              RegExp(r'\s')),
        ],
        enabled: widget.enabled,
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        maxLines: widget.keyboardType == TextInputType.multiline ? 4 : 1,
        cursorColor: AppColors.primary,
        textAlign: widget.keyboardType == TextInputType.multiline
            ? TextAlign.left
            : TextAlign.left,
        autofocus: false,
        onChanged: widget.onChange,
        obscureText: widget.obscureText ?? false,
        decoration: widget.icon != null ? InputDecoration(
          filled: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 16, minWidth: 50),
          prefixIcon: AppImage(
            widget.icon!,
            width: 19,
          ),
          fillColor: Colors.white,
          hintText: widget.hintText,
          hintStyle: const TextStyle(color: Colors.black54),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(widget.borderRadius),
              borderSide: BorderSide.none),
          contentPadding:
          const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        ) : InputDecoration(
          filled: true,
          fillColor: widget.fillColor,
          hintText: widget.hintText,
          hintStyle: const TextStyle(color: Colors.black54),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(widget.borderRadius),
              borderSide: BorderSide.none),
          contentPadding:
          const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        ),
        style: TextStyle(
            fontWeight: widget.fontWeight,
            fontSize: widget.fontSize,
            height: 1.5,
            letterSpacing: widget.letterSpacing,
            color: Colors.black),
      ),
    ),],);
  }
}
