import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/challenge_role_model.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/modal_bottom_sheet/app_modal_bottom_sheet.dart';
import 'package:challenger/views/pages/challenge/wish_list/wish_list_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class AppCoChallengerModalBottomSheet {
  ChallengeModel challengeModel;

  AppCoChallengerModalBottomSheet.show({required this.challengeModel}) {
    AppModalBottomSheet.show(
        isScrollControlled: true,
        body: AppCoChallengerModalBottomSheetWidget(
          challengeModel: challengeModel,
        ));
  }
}

class AppCoChallengerModalBottomSheetWidget extends StatefulWidget {
  ChallengeModel challengeModel;

  AppCoChallengerModalBottomSheetWidget(
      {Key? key, required this.challengeModel})
      : super(key: key);

  @override
  State<AppCoChallengerModalBottomSheetWidget> createState() =>
      _AppCoChallengerModalBottomSheetWidgetState();
}

class _AppCoChallengerModalBottomSheetWidgetState
    extends State<AppCoChallengerModalBottomSheetWidget> {
  String _query = "";
  List<ChallengeRoleModel> categories = [];
  bool _isCoChallenge = true;
  String _description = "";

  Widget _action() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: AppMagicButton(
          postUrl: "/challenge-role/",
          getData: () {
            List<int> roleIds = [];
            for (var value in categories) {
              roleIds.add(value.id!);
            }
            return RequestBodyBuilder()
                .addField("roles", roleIds)
                .addField("challengeId", widget.challengeModel.id)
                .addField("message", _description)
                .build();
          },
          width: double.maxFinite,
          text: "Send",
          onDone: (response) {
            Get.back();
            AppMessage.showDialogMessage(response);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_isCoChallenge) {
      return Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [
              const SizedBox(
                height: 5,
              ),
              Container(
                padding: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: AppColors.inputFill,
                    borderRadius: BorderRadius.circular(24)),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AppButton(
                        text: "Co challenge", width: 140, onPressed: () {}),
                    AppButton.flat(
                        text: "Sponsor",
                        width: 140,
                        onPressed: () {
                          setState(() {
                            _isCoChallenge = false;
                          });
                        }),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              AppSearchTextField(
                  hintText: "Search your role...",
                  borderRadius: 50,
                  onChaneValue: (value) {
                    setState(() {
                      _query = value;
                    });
                  }),
              Stack(
                children: [
                  Column(
                    children: [
                      const SizedBox(
                        height: 28,
                      ),
                      SizedBox(
                        height: 32,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [...categories.map((e) => _clipItem(e))],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: AppTextField(
                          hintText: "Write your message...",
                          borderRadius: 50,
                          onChange: (value) {
                            print(value);
                            setState(() {
                              _description = value;
                            });
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      _action(),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                  Container(
                    width: double.maxFinite,
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: const [
                          BoxShadow(
                              offset: Offset(0, 3),
                              color: Colors.black12,
                              blurRadius: 2)
                        ]),
                    child: AppMagicFutureBuilder(
                        url: "/challenge-role/?query=$_query",
                        onSuccess: (response) {
                          if (_query.isEmpty) {
                            return Container();
                          }
                          List<ChallengeRoleModel> list = [];
                          for (var item in response) {
                            list.add(ChallengeRoleModel(
                                id: item['id'], title: item['title']));
                          }
                          return SizedBox(
                            height: 120,
                            child: ListView.separated(
                              itemCount: list.length,
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      const Divider(height: 1),
                              itemBuilder: (context, index) {
                                return InkWell(
                                    onTap: () {
                                      for (var item in categories) {
                                        if (item.id == list[index].id) {
                                          return;
                                        }
                                      }
                                      setState(() {
                                        _query = "";
                                        categories.add(list[index]);
                                      });
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 10),
                                      child: AppText(list[index].title!),
                                    ));
                              },
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ],
          ));
    }
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            const SizedBox(
              height: 5,
            ),
            Container(
              padding: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  color: AppColors.inputFill,
                  borderRadius: BorderRadius.circular(24)),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppButton.flat(
                      text: "Co challenge",
                      width: 140,
                      onPressed: () {
                        setState(() {
                          _isCoChallenge = true;
                        });
                      }),
                  AppButton(text: "Sponsor", width: 140, onPressed: () {}),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                AppMagicFutureBuilder(url: "/challenge/${widget.challengeModel.id!}/wishes", onSuccess: (response) {
                  List<Widget> list = [];
                  for(var item in response) {
                    list.add(_wishItem(item['title'],item['description']));
                  }
                  return Column(
                    children: [
                      ...list,
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  );
                }),
                Container(
                  height: 50,
                  margin: EdgeInsets.only(bottom: 40),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: FloatingActionButton.extended(
                      label: AppText(
                        "Wish List",
                        color: Colors.white,
                      ),
                      backgroundColor: AppColors.primary,
                      onPressed: () {
                        Get.back();
                        Get.to(WishListPage(id: widget.challengeModel.id!,));
                      },
                      icon: const Icon(Icons.add),
                    ),
                  ),
                )
              ],
            ),
          ],
        ));
  }

  Widget _wishItem(String title, String description) {
    return Container(
      padding: const EdgeInsets.symmetric(
          horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color(0xffDDCA76), Color(0xffD9DCDE)])),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppText(
            title,
            fontSize: 18,
            color: const Color(0xff1d1d1d),
            fontWeight: FontWeight.w700,
          ),
          AppText(
            description,
            fontSize: 14,
          ),
          Container(
            alignment: Alignment.centerRight,
            child: AppImage(
              "lucide_share.png",
              width: 20,
              height: 20,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _clipItem(ChallengeRoleModel categoryModel) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      child: Container(
        height: 32,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(50)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppText(
              categoryModel.title!,
              fontSize: 12,
            ),
            const SizedBox(
              width: 5,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  categories.remove(categoryModel);
                });
              },
              child: const Icon(
                Icons.close,
                size: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
