import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppModalBottomSheet {
  AppModalBottomSheet.show({required Widget body, bool isScrollControlled = false}) {
    showModalBottomSheet(
      isScrollControlled: isScrollControlled,
      context: Get.overlayContext!,
      barrierColor: Colors.black.withOpacity(0.00),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(24
        )),
      ),
      builder: (BuildContext context) {
        return Padding(
            padding: MediaQuery.of(context).viewInsets,child: Container(
          decoration: const BoxDecoration(
          color: Color(0xffD9DCDE),
            borderRadius: BorderRadius.vertical(top: Radius.circular(24))
          ),
          child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Transform.translate(
              offset: const Offset(0, -16),
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(color: AppColors.primary, borderRadius: BorderRadius.circular(50),border: Border.all(width: 4, color: Colors.white)),
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(8),
                  child: AppImage("ic_arrow_down.png"),
                ),),
            ),
            Container(child: body,)
          ],
        ),),);
      },
    );
  }
}
