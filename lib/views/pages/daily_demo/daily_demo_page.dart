import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/pages/home/home_item.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class DailyDemoPage extends StatefulWidget {
  DailyReportModel dailyReportModel;

  DailyDemoPage({Key? key, required this.dailyReportModel}) : super(key: key);

  @override
  State<DailyDemoPage> createState() => _DailyDemoPageState();
}

class _DailyDemoPageState extends State<DailyDemoPage> {
  List<DailyReportModel> dailies = [];
  DailyReportModel? mainChallengerDailyReport;
  DailyReportModel? selectedDailyReport;
  bool playing = true;
  bool getData = false;
  VideoPlayerController? _controller;

  void updateView() async {
    await Future.delayed(const Duration(milliseconds: 100));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: AppColors.actionBarColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 8,
                  offset: const Offset(0, 4),
                ),
              ],
            ),
            child: Container(
              padding:
                  const EdgeInsets.only(left: 7, right: 7, top: 10, bottom: 10),
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Builder(builder: (BuildContext context) {
                    return InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: 50,
                        height: 50,
                        child: AppImage(
                          "ic_arrow_left.png",
                          color: AppColors.h1TextColor,
                        ),
                      ),
                    );
                  }),
                  Column(
                    children: [
                      InkWell(
                        onTap: () async {
                          if (_controller == null) {
                            return;
                          }
                          _controller!.pause();
                          await Get.to(ChallengePage(
                              initialTabIndex: 0,
                              id: widget.dailyReportModel.parentChallengeId ??
                                  widget.dailyReportModel.challengeId!,
                              title: widget.dailyReportModel.challengeName ??
                                  "Challenge Name"));
                          _controller!.play();
                        },
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: AppText(
                            widget.dailyReportModel.challengeName ??
                                "Challenge Name",
                            fontSize: 11,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          if (_controller == null) {
                            return;
                          }
                          _controller!.pause();
                          await Get.to(ProfilePage(
                              id: mainChallengerDailyReport!.challenger!.id!));
                          _controller!.play();
                        },
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: AppText(
                            "${mainChallengerDailyReport?.challenger?.firstname} ${mainChallengerDailyReport?.challenger?.lastname}",
                            fontSize: 11,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    width: 50,
                    height: 50,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          AppMagicFutureBuilder(
              url:
                  "/daily-report/${widget.dailyReportModel.id!}/main-and-co-challengers-daily-reports?stage=${widget.dailyReportModel.stage}&index=${widget.dailyReportModel.index}",
              onSuccess: (response) {
                if (!getData) {
                  mainChallengerDailyReport =
                      DailyReportModel.fromJson(response[0]);
                  selectedDailyReport = widget.dailyReportModel;

                  for (var i = 1; i < response.length; i++) {
                    dailies.add(DailyReportModel.fromJson(response[i]));
                  }
                  updateView();
                  getData = true;
                }
                return Container();
              }),
          if (selectedDailyReport != null)
            Stack(
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 120,
                          child: AppText(
                            Utils.formatCalendarDatePickerForDailyReport(
                                DateTime.parse(widget.dailyReportModel.date!)),
                            fontSize: 10,
                          ),
                        ),
                        const Spacer(),
                        AppText(
                          selectedDailyReport!.challenger!.username!,
                          color: _stateColor(selectedDailyReport!),
                          fontSize: 12,
                        ),
                        const Spacer(),
                        const SizedBox(
                          width: 120,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                              color: _stateColor(
                                selectedDailyReport!,
                              ),
                              width: 2)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(6),
                        child: SizedBox(
                          height: (MediaQuery.of(context).size.height - 200) *
                              2 /
                              3,
                          child: playing
                              ? AppVideoPlayer.playDailyDemo(
                                  url: selectedDailyReport!.dailyVideoUrl,
                                  dailyReportModel: selectedDailyReport,
                                  controller: (controller) {
                                    _controller = controller;
                                  },
                                )
                              : Center(
                                  child: CircularProgressIndicator(),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          if (mainChallengerDailyReport != null)
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                AppText(
                  mainChallengerDailyReport!.challenger!.username!,
                  color: _stateColor(mainChallengerDailyReport!),
                  fontSize: 10,
                ),
                InkWell(
                  onTap: () {
                    if (mainChallengerDailyReport!.dailyVideoCoverUrl != null) {
                      _select(mainChallengerDailyReport!);
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(
                            color: _stateColor(
                              mainChallengerDailyReport!,
                            ),
                            width: 2)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(6),
                      child: mainChallengerDailyReport!.dailyVideoCoverUrl !=
                              null
                          ? Image.network(
                              mainChallengerDailyReport!.dailyVideoCoverUrl!,
                              height: 90,
                              fit: BoxFit.cover,
                            )
                          : const SizedBox(
                              height: 90,
                            ),
                    ),
                  ),
                ),
              ],
            ),
          Expanded(
              child: GridView.count(
            mainAxisSpacing: 5,
            crossAxisCount: 3,
            crossAxisSpacing: 15,
            childAspectRatio: 2 / 2,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            children: List.generate(dailies.length, (index) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppText(
                    dailies[index].challenger!.username!,
                    color: _stateColor(dailies[index]),
                    fontSize: 10,
                  ),
                  InkWell(
                    onTap: () {
                      _select(dailies[index]);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                              color: _stateColor(
                                dailies[index],
                              ),
                              width: 2)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(6),
                        child: Image.network(
                          dailies[index].dailyVideoCoverUrl!,
                          height: 90,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }),
          ))
        ],
      ),
    );
  }

  Color _stateColor(DailyReportModel dailyReportModel) {
    switch (dailyReportModel.result) {
      case "NOT_REACHED":
        return const Color(0xffE66969);
      case "REACHED":
        return const Color(0xff78db76);
      case "REACHED_BUT_WITH_PROBLEM":
        return const Color(0xffDEC557);
      default:
        return const Color(0xffE66969);
    }
  }

  void _select(DailyReportModel daily) async {
    // int index = dailies.indexOf(daily);
    // dailies.remove(daily);
    // dailies.insert(index, selectedDailyReport!);
    selectedDailyReport = daily;
    playing = false;
    setState(() {});
    await Future.delayed(Duration(milliseconds: 100));
    playing = true;
    setState(() {});
  }
}
