import 'package:challenger/models/all_explorer_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/pages/passepartout/views/media/media_view.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:flutter/material.dart';

class ReelsPage extends StatefulWidget {
  List<AllExplorerModel> allExplorerModel;
  int selectedModelIndex;
  ReelsPage({Key? key, required this.allExplorerModel, required this.selectedModelIndex}) : super(key: key);

  @override
  State<ReelsPage> createState() => _ReelsPageState();
}

class _ReelsPageState extends State<ReelsPage> {

  late PageController _controller;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = PageController(initialPage: widget.selectedModelIndex);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(
        scrollDirection: Axis.vertical,
        itemCount: widget.allExplorerModel.length,
        controller: _controller,
        itemBuilder: (context, index) {
          if (widget.allExplorerModel[index].type == 1) {
            return VideoPlayerPage.playChallengerIntroVideo(widget.allExplorerModel[index].coAndChallengerModel, fromReels: false,);
          }
          return VideoPlayerPage.playChallengeVideo(widget.allExplorerModel[index].challengeModel,fromReels: false,);
        },
      )
    );
  }
}
