import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/pages/challenge/build/build_challenge.dart';
import 'package:challenger/views/pages/challenge/build_story/build_story_page.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/challenge/stage/stage_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/home/home_page.dart';
import 'package:challenger/views/pages/passepartout/views/media/media_view.dart';
import 'package:challenger/views/pages/reels/reels_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:get/get.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;
  int _lastSelectedIndex = 0;
  List<int> _backStack = [0];

  void _onItemTapped(int index) {
    if (index == 2) {
      showCreationDialog();
      return;
    }
    setState(() {
      _backStack.add(index);
      _lastSelectedIndex = _selectedIndex;
      _selectedIndex = index;
    });
  }

  void setStory() {
    Get.back();
    AppLoadingDialog.show();
    ChallengeRepository().currentChallenges(AppDataListener(onSuccess: (data) {
      AppLoadingDialog.hide();
      if (data.isEmpty) {
        Get.to(BuildChallengePage());
      } else {
        if (data.length == 1) {
          Get.to(ChallengePage(
              initialTabIndex: 2,
              id: data[0]['id'],
              title: data[0]['title']));
          return;
        }
        AppDialog(
            body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppText("Select challenge"),
            ...data.map(
              (e) => Container(
                margin: const EdgeInsets.symmetric(vertical: 5),
                child: AppButton(text: e['title'], onPressed: () {
                  Get.back();
                  Get.to(ChallengePage(
                      initialTabIndex: 2,
                      id: e['id'],
                      title: e['title']));
                }),
              ),
            )
          ],
        ));
      }

    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        setStory();
      });
    }));
  }

  void showCreationDialog() {
    AppDialog(
        body: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          height: 10,
        ),
        AppText.body(
          "By pressing on these buttons you can build a challenge or you are able to build a prize",
          fontSize: 14,
          textAlign: TextAlign.center,
        ),
        const SizedBox(
          height: 10,
        ),
        _button("New Challenge", onClick: () {
          Get.to(BuildChallengePage());
        }),
        _button("New Storyline", onClick: () {
          setStory();
        }),
        const SizedBox(
          height: 5,
        ),
      ],
    ));
  }

  @override
  void initState() {
    super.initState();
    initUserModel();
  }

  UserModel? userModel;

  void initUserModel() async {
    userModel = await AppStorage.getInstance().getUser();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          _backStack.removeLast();
          if (_backStack.isEmpty) {
            return true;
          }
          setState(() {
            _lastSelectedIndex = _selectedIndex;
            _selectedIndex = _backStack.last;
          });
          return false;
        },
        child: Scaffold(
            body: _getSelectedPage(),
            backgroundColor: Colors.white,
            bottomNavigationBar: Stack(
              children: [
                ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                    child: SizedBox(
                      child: BottomNavigationBar(
                        type: BottomNavigationBarType.fixed,
                        backgroundColor: const Color(0xfff4f4f4),
                        items: [
                          BottomNavigationBarItem(
                            icon: AppImage(
                              "ic_home.png",
                              color: _selectedIndex == 0
                                  ? AppColors.primary
                                  : const Color(0xff1d1d1d),
                              width: 20,
                            ),
                            label: 'Home',
                          ),
                          BottomNavigationBarItem(
                            icon: AppImage(
                              "ic_search2.png",
                              color: _selectedIndex == 1
                                  ? AppColors.primary
                                  : const Color(0xff1d1d1d),
                              width: 20,
                            ),
                            label: 'Home',
                          ),
                          BottomNavigationBarItem(
                            icon: Container(),
                            label: 'Home',
                          ),
                          BottomNavigationBarItem(
                            icon: AppImage(
                              "ic_reels.png",
                              color: _selectedIndex == 3
                                  ? AppColors.primary
                                  : const Color(0xff1d1d1d),
                              width: 20,
                            ),
                            label: 'Home',
                          ),
                          BottomNavigationBarItem(
                            icon: FutureBuilder(
                              future: AppStorage.getInstance().getUser(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<UserModel> snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Container(
                                    width: 20,
                                  );
                                } else if (snapshot.hasError) {
                                  return Text('Error: ${snapshot.error}');
                                } else {
                                  return CircleAvatar(
                                    radius: 10,
                                    backgroundImage: NetworkImage(
                                        snapshot.data!.profileUrl!),
                                  );
                                }
                              },
                            ),
                            label: 'Home',
                          ),
                        ],
                        currentIndex: _selectedIndex,
                        onTap: _onItemTapped,
                        showSelectedLabels: false,
                        showUnselectedLabels: false,
                      ),
                    )),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      InkWell(
                        onTap: () {
                          _onItemTapped(2);
                        },
                        child: Container(
                          height: 44,
                          width: 44,
                          margin: const EdgeInsets.only(top: 5),
                          decoration: BoxDecoration(
                              color: AppColors.primary,
                              borderRadius: BorderRadius.circular(50)),
                          child: const Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ])
              ],
            )));
  }

  Widget _getSelectedPage() {
    switch (_selectedIndex) {
      case 0:
        return HomePage();
      case 1:
        return ExplorerPage();
      case 3:
        return MediaView();
      case 4:
        return ProfilePage(
          isCurrentUser: true,
          id: userModel!.id!,
        );
      default:
        return Container();
    }
  }

  Widget _button(String text, {required Function onClick}) {
    return InkWell(
      onTap: () {
        Get.back();
        onClick();
      },
      child: Container(
        height: 48,
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        decoration: BoxDecoration(
            color: AppColors.primary, borderRadius: BorderRadius.circular(24)),
        child: Row(
          children: [
            const SizedBox(
              width: 20,
            ),
            const Icon(
              Icons.add,
              color: Colors.white,
            ),
            const SizedBox(
              width: 10,
            ),
            AppText(
              text,
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
