import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/pages/daily_demo/daily_demo_page.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_like_toggle_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeItem extends StatefulWidget {
  DailyReportModel dailyReportModel;
  EdgeInsetsGeometry? margin;

  HomeItem({
    Key? key,
    required this.dailyReportModel,
    this.margin = const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
  }) : super(key: key);

  @override
  State<HomeItem> createState() => _HomeItemState();
}

class _HomeItemState extends State<HomeItem> {
  bool _showLikeWidget = false;

  void coChallenge(int id) {
    AppLoadingDialog.show();
    ChallengeRepository().coChallenge(
        id,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          Get.to(ChallengePage(
              initialTabIndex: 0,
              id: response['id'],
              title: response['title']));
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            coChallenge(id);
          });
        }));
  }

  void openChallenge(int id) {
    AppLoadingDialog.show();
    ChallengeRepository().getChallengeById(
        id,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          Get.to(ChallengePage(
              initialTabIndex: 0,
              id: response['id'],
              title: response['title']));
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            openChallenge(id);
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Column(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 10, left: 10),
                child: InkWell(onTap: (){
                  Get.to(ProfilePage(id: widget.dailyReportModel!.challenger!.id!));
                },child: Row(
                  children: [
                    SizedBox(
                      width: 32,
                      height: 32,
                      child: Stack(
                        children: [
                          CircleAvatar(
                            radius: 24,
                            backgroundImage: NetworkImage(widget
                                .dailyReportModel.challenger!.profileUrl!),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.body(
                          "${widget.dailyReportModel.challenger?.firstname ?? ""} ${widget.dailyReportModel.challenger?.lastname ?? ""}",
                          fontSize: 12,
                          color: const Color(0xff344054),
                        ),
                        AppText.body(
                          "@${widget.dailyReportModel.challenger!.username!}",
                          fontSize: 12,
                          color: const Color(0xff667085),
                        ),
                      ],
                    ),
                    const Spacer(),
                     InkWell(onTap: (){
                       openChallenge(widget.dailyReportModel.challengeId!);
                     },child: Row(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         AppText(
                           widget.dailyReportModel.challengeName ?? "",
                           fontSize: 12,
                           fontWeight: FontWeight.bold,
                         ),
                         const SizedBox(width: 10,),
                         AppImage(
                           "ic_crown.png", width: 17,
                           color: widget.dailyReportModel.parentChallengeId == null ? Colors.orange : Colors.black54,
                         )

                       ],),)

                  ],
                ),),
              ),
              InkWell(
                onTap: () {
                  Get.to(DailyDemoPage(dailyReportModel: widget.dailyReportModel));
                },
                child: Container(
                  padding: EdgeInsets.all(2),
                  color: _stateColor(),
                  child: Image.network(
                    widget.dailyReportModel.dailyVideoCoverUrl ??
                        "https://cdn0.tnwcdn.com/wp-content/blogs.dir/1/files/2019/04/adobe_selling_air.jpg",
                    height: 213,
                    width: double.maxFinite,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _actionItem(
                      icon: "ic_send.png",
                      text: "Send",
                      onClick: () {
                        AppShare.shareDailyReport(widget.dailyReportModel);
                      }),
                  _actionItem(
                      icon: "logo.png",
                      text: widget.dailyReportModel.challengeName ?? "Challenge Name",
                      onClick: () {
                        openChallenge(widget.dailyReportModel.challengeId!);
                      }),
                  _actionItem(
                      icon: "ic_co_challenge.png",
                      text: "Co Challenge",
                      onClick: () {
                        AppDialog.showAlertDialog(
                            description:
                            "Do you want to be co challenger with this challenge?",
                            onAction: () {
                              coChallenge(widget.dailyReportModel.challengeId!);
                            });
                      }),
                  AppLikeToggle.dailyReport(widget.dailyReportModel),
                  // _actionItem(
                  //     icon: "ic_favorite_empty.png",
                  //     text: "Like",
                  //     onClick: () {
                  //       setState(() {
                  //         _showLikeWidget = true;
                  //       });
                  //     }),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              const Divider(
                color: Color(0xffD8D8D8),
              ),
              const SizedBox(
                height: 5,
              ),
            ],
          ),
          // AppLikeWidget(
          //   show: _showLikeWidget,
          //   onHide: () {
          //     setState(() {
          //       _showLikeWidget = false;
          //     });
          //   },
          // ),
        ],
      ),
    );
  }

  Widget _actionItem(
      {required String icon, required String text, required Function onClick}) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Column(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: AppImage(icon),
          ),
          const SizedBox(
            height: 4,
          ),
          AppText(
            text,
            fontSize: 9,
          ),
        ],
      ),
    );
  }

  Color _stateColor() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return const Color(0xffE66969);
      case "REACHED":
        return const Color(0xff78db76);
      case "REACHED_BUT_WITH_PROBLEM":
        return const Color(0xffDEC557);
      default:
        return const Color(0xffE66969);
    }
  }
}
