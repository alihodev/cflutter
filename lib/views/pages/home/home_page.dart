import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/home_model.dart';
import 'package:challenger/services/amplify_chat_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/chat_list/chat_list_page.dart';
import 'package:challenger/views/pages/home/home_item.dart';
import 'package:challenger/views/pages/passepartout/passepartout_page.dart';
import 'package:challenger/views/pages/passepartout/views/chat/chat_view.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ChallengeModel> challenges = [];
  String query = "";
  bool _hasNewMessage = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    _hasNewMessage = await hasNewMessage(
        (await AppStorage.getInstance().getUser()).id.toString());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Column(
        children: [
          Container(
            height: 65,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: AppColors.actionBarColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 8,
                  offset: const Offset(0, 4),
                ),
              ],
            ),
            child: Row(
              children: [
                const SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () {
                    Get.to(PassepartoutPage(initialTabIndex: 0));
                  },
                  child: Container(
                    width: 44,
                    height: 44,
                    padding: const EdgeInsets.all(11),
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(color: Colors.black12, offset: Offset(0, 3))
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                    child: AppImage(
                      "ic_bot2.png",
                      color: AppColors.primary,
                      width: 24,
                    ),
                  ),
                ),
                const Spacer(),
                AppImage(
                  "logo.png",
                  height: 40,
                ),
                const Spacer(),
                InkWell(
                  onTap: () async {
                    await Get.to(ChatListPage());
                    init();
                  },
                  child: Container(
                    width: 44,
                    height: 44,
                    padding: const EdgeInsets.all(11),
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(color: Colors.black12, offset: Offset(0, 3))
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                    child: Stack(
                      children: [
                        const Icon(
                          Icons.email_rounded,
                          color: AppColors.primary,
                          size: 22,
                        ),
                        if (_hasNewMessage)
                          Container(
                            width: 7,
                            height: 7,
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(50)),
                          )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 3,
          ),
          Expanded(
              child: AppPaginatedListView<DailyReportModel>(
            url: "/daily-report/",
            onFetchData: (response) {
              return response
                  .map<DailyReportModel>((e) => DailyReportModel.fromJson(e))
                  .toList();
            },
            renderItem: (item) => HomeItem(dailyReportModel: item),
          )),
        ],
      ),
    );
  }
}
