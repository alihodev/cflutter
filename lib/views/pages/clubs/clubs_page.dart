import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/pages/clubs/views/chats/chat_view.dart';
import 'package:challenger/views/pages/clubs/views/debates/debates_view.dart';
import 'package:challenger/views/pages/clubs/views/rooms/rooms_view.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ClubsPage extends StatefulWidget {
  int initialTabIndex;

  ClubsPage({Key? key, required this.initialTabIndex}) : super(key: key);

  @override
  State<ClubsPage> createState() => _ClubsPageState();
}

class _ClubsPageState extends State<ClubsPage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: 3, vsync: this, initialIndex: widget.initialTabIndex);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.actionBarColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 8,
                    offset: const Offset(0, 4),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        left: 7, right: 7, top: 10, bottom: 10),
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Builder(builder: (BuildContext context) {
                          return InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              width: 50,
                              height: 50,
                              child: AppImage(
                                "ic_arrow_left.png",
                                color: AppColors.h1TextColor,
                              ),
                            ),
                          );
                        }),
                        AppText.h1("Clubs"),
                        Container(
                          padding: const EdgeInsets.all(10),
                          width: 50,
                          height: 50,
                        ),
                      ],
                    ),
                  ),
                  PreferredSize(
                    preferredSize: const Size.fromHeight(kToolbarHeight),
                    child: TabBar(
                      controller: _tabController,
                      labelColor: AppColors.primary,
                      labelStyle: const TextStyle(
                        fontSize: 16,
                      ),
                      unselectedLabelColor: AppColors.unSelectedTabColor,
                      indicatorColor: AppColors.primary,
                      indicatorSize: TabBarIndicatorSize.label,
                      tabs: const [
                        Tab(text: 'Chats'),
                        Tab(text: 'Rooms'),
                        Tab(text: 'Debates'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: const [
                  ChatsView(),
                  RoomsView(),
                  DebatesView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
