import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/debates_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class DebateItem extends StatefulWidget {
  DebateModel debateModel;

  DebateItem({Key? key, required this.debateModel}) : super(key: key);

  @override
  State<DebateItem> createState() => _DebateItemState();
}

class _DebateItemState extends State<DebateItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.debateCardBackground,
      shadowColor: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
      ),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      child: Container(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 5),
        child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: [
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.h2(
                          'Title of the debate',
                          fontSize: 18,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppText.body(
                          'The name of the owner',
                          color: AppColors.h1TextColor,
                          fontSize: 14,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            AppImage(
                              "ic_debate.png",
                              width: 20,
                              height: 20,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            AppText.body(
                              '48 Member',
                              fontSize: 14,
                              color: AppColors.h1TextColor,
                            ),
                          ],
                        ),
                        const SizedBox(height: 15)
                      ],
                    )),
                Container(
                  color: const Color(0xff484848),
                  height: 90,
                  width: 1,
                ),
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Column(
                              children: [
                                _leftUserItem("ic_favorite.png"),
                                const SizedBox(height: 5,),
                                _leftUserItem("ic_like.png", paddingTop: 1),
                              ],
                            )),
                            Expanded(
                                child: Column(
                              children: [
                                _leftUserItem("ic_vessel.png"),
                              ],
                            )),
                            Expanded(
                                child: Column(
                              children: [
                                _rightUserItem("ic_unfavorite.png"),
                                const SizedBox(height: 5,),
                                _rightUserItem("ic_dislike.png", paddingBottom: 1),
                              ],
                            ))
                          ],
                        ),
                        InkWell(
                            child: AppImage(
                              "lucide_share.png",
                              height: 22,
                              width: 22,
                            ),
                            onTap: () {}),
                      ],
                    ))
              ],
            )),
      ),
    );
  }

  Widget _leftUserItem(String icon, {double paddingTop = 0}) {
    double width = 40;
    return Stack(
      children: [
        Container(
          width: width,
          height: 30,
          alignment: Alignment.centerRight,
          padding: EdgeInsets.only(top: paddingTop),
          child: AppImage("placeholder_left.png" , width: 15,),
        ),
        Container(
          width: width,
          height: 30,
          padding:const EdgeInsets.only(right: 3),
          alignment: Alignment.centerRight,
          child: AppImage(icon, width: 7,),
        ),
        Container(
          alignment: Alignment.centerLeft,
          width: width,
          child: const CircleAvatar(
            radius: 14,
            backgroundColor: Color(0xff18B915),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnxlbnwwfHwwfHx8MA%3D%3D&w=300&q=80"),
              radius: 12,
            ),
          ),
        ),
        FittedBox(

            child:
        Container(
          width: width,
          margin: const EdgeInsets.only(top: 30),
          padding: const  EdgeInsets.only(right: 8),
          alignment: Alignment.center,
          child: AppText.body("Dean kendy", fontSize: 6,),
        )),
      ],
    );
  }

  Widget _rightUserItem(String icon, {double paddingBottom = 0}) {
    double width = 40;
    return Stack(
      children: [
        Container(
          width: width,
          height: 30,
          padding: EdgeInsets.only(bottom: paddingBottom),
          alignment: Alignment.centerLeft,
          child: AppImage("placeholder_right.png" , width: 15,),
        ),
        Container(
          width: width,
          height: 30,
          padding: const EdgeInsets.only(left: 3),
          alignment: Alignment.centerLeft,
          child: AppImage(icon, width: 7,),
        ),
        Container(
          alignment: Alignment.centerRight,
          width: width,
          child: const CircleAvatar(
            radius: 14,
            backgroundColor: Color(0xff18B915),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnxlbnwwfHwwfHx8MA%3D%3D&w=300&q=80"),
              radius: 12,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 30),
          alignment: Alignment.center,
          child: AppText.body("Dean kendy", fontSize: 6,),
        )
      ],
    );
  }
}
