import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/chat_list_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/pages/chat/chat_page.dart';
import 'package:get/get.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get_time_ago/get_time_ago.dart';

class ChatItem extends StatefulWidget {
  ChatListModel chatModel;
  Function(ChatListModel chatModel) onClick;
  ChatItem({Key? key, required this.chatModel, required this.onClick}) : super(key: key);

  @override
  State<ChatItem> createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        setState(() {
        widget.chatModel.hasNewMessage = false;
        });
        widget.onClick(widget.chatModel);
      },
      child:  Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          ListTile(
            onTap: () {
              widget.chatModel.hasNewMessage = false;
              widget.onClick(widget.chatModel);
            },
            leading: SizedBox(
              width: 50,
              height: 50,
              child: Stack(
                children:  [
                  CircleAvatar(
                    radius: 24,
                    backgroundImage: NetworkImage(
                        widget.chatModel.user?.profileUrl ?? ""),
                  ),
                  // Align(
                  //   alignment: Alignment.bottomRight,
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //         color: widget.chatModel.isOnline ? const Color(0xff11B76A) : const Color(0xffD0D5DD),
                  //         border: Border.all(color: Colors.white, width: 2),
                  //         borderRadius: BorderRadius.circular(10)),
                  //     width: 14,
                  //     height: 14,
                  //   ),
                  // )
                ],
              ),
            ),
            title: Row(
              children: [
                AppText.body(
                  "${widget.chatModel.user?.firstname} ${widget.chatModel.user?.lastname}",
                  fontSize: 14,
                  color: const Color(0xff101828),
                ),
                const SizedBox(
                  width: 10,
                ),
                // AppText.body(
                //   GetTimeAgo.parse(DateTime.parse(widget.chatModel.lastMessageDate ?? "")),
                //   fontSize: 12,
                //   color: const Color(0xff667085),
                // ),
                const Spacer(),
                if (widget.chatModel.hasNewMessage ?? false)
                Container(
                  decoration: BoxDecoration(
                      color: const Color(0xff11B76A),
                      borderRadius: BorderRadius.circular(10)),
                  width: 8,
                  height: 8,
                )
              ],
            ),
            subtitle: AppText.body(
              "@${widget.chatModel.user?.username}", fontSize: 14,
              color: const Color(0xff667085),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 80),
            alignment: Alignment.centerLeft,
            child: AppText.body(
            widget.chatModel.lastMessage!,overFlow: TextOverflow.ellipsis, fontSize: 14,
            color: const Color(0xff667085),
          ),),
          const SizedBox( height: 10,),
          const Divider(height: 2,),
          const SizedBox( height: 10,),
        ],
      ),),
    );
  }
}
