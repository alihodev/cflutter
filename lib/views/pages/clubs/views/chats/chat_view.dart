import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/chat_list_model.dart';
import 'package:challenger/views/pages/clubs/views/chats/chat_item.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatsView extends StatefulWidget {
  const ChatsView({Key? key}) : super(key: key);

  @override
  State<ChatsView> createState() => _ChatsViewState();
}

class _ChatsViewState extends State<ChatsView> {
  List<ChatListModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            Expanded(
                child: AppPaginatedListView<ChatListModel>(
              url: "/google.com",
              query: query,
              isFake: true,
              onFetchData: (response) {
                return response.map<ChatListModel>((e) => ChatListModel()).toList();
              },
              renderItem: (item) => ChatItem(chatModel: item, onClick: (item){}),
            )),
          ],
        ),
        Container(
          margin: const EdgeInsets.all(16),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton.extended(
              label: AppText(
                "New Chat",
                color: Colors.white,
              ),
              backgroundColor: AppColors.primary,
              onPressed: () {
                Get.to(BuildDebatePage());
              },
              icon: const Icon(Icons.add),
            ),
          ),
        )
      ],
    );
  }
}
