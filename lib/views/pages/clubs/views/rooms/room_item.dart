import 'package:challenger/models/room_model.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class RoomItem extends StatefulWidget {
  RoomModel roomModel;

  RoomItem({Key? key, required this.roomModel}) : super(key: key);

  @override
  State<RoomItem> createState() => _RoomItemState();
}

class _RoomItemState extends State<RoomItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(
                'https://anzsog.edu.au/app/uploads/2022/06/public-sector-innovation-19.jpg',
            width: 106,height: 90, fit: BoxFit.cover,),
            
          ),
            const SizedBox(height: 2,),
          AppText("Innovation", fontSize: 14,)
        ],
      ),
    );
  }
}
