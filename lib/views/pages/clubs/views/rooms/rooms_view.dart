import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/room_model.dart';
import 'package:challenger/views/pages/clubs/views/rooms/room_item.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RoomsView extends StatefulWidget {
  const RoomsView({Key? key}) : super(key: key);

  @override
  State<RoomsView> createState() => _RoomsViewState();
}

class _RoomsViewState extends State<RoomsView> {
  List<RoomModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            Expanded(
                child:
                    //     AppPaginatedListView<RoomModel>(
                    //   url: "/google.com",
                    //   query: query,
                    //   isFake: true,
                    //   onFetchData: (response) {
                    //     return response.map<RoomModel>((e) => RoomModel()).toList();
                    //   },
                    //   renderItem: (item) => RoomItem(roomModel: item),
                    // ),
                    SingleChildScrollView(child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            RoomItem(roomModel: RoomModel()),
                            RoomItem(roomModel: RoomModel()),
                            RoomItem(roomModel: RoomModel()),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            RoomItem(roomModel: RoomModel()),
                            RoomItem(roomModel: RoomModel()),
                            RoomItem(roomModel: RoomModel()),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Divider(height: 2,),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(children: [
                          const SizedBox(width: 10,),
                          AppText("Name of the challenge", fontWeight: FontWeight.w700,),
                          Spacer(),
                          AppButton.flat(width: 90, text: "See All", onPressed: (){}),
                          const SizedBox(width: 10,),
                        ],),
                        const SizedBox(
                          height: 10,
                        ),
                        SingleChildScrollView(scrollDirection: Axis.horizontal, child: Row(children: [
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                        ],),),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(children: [
                          const SizedBox(width: 10,),
                          AppText("Name of the challenge", fontWeight: FontWeight.w700,),
                          Spacer(),
                          AppButton.flat(width: 90, text: "See All", onPressed: (){}),
                          const SizedBox(width: 10,),
                        ],),
                        const SizedBox(
                          height: 10,
                        ),
                        SingleChildScrollView(scrollDirection: Axis.horizontal, child: Row(children: [
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                          RoomItem(roomModel: RoomModel()),
                        ],),)
                      ],
                    ),)),
          ],
        ),
      ],
    );
  }
}
