import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/services/amplify_chat_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/chat_list/chat_list_page.dart';
import 'package:challenger/views/pages/login/intro/image_banner.dart';
import 'package:challenger/views/pages/login/sign_in/sign_in_page.dart';
import 'package:challenger/views/pages/login/sign_up/sign_up_page.dart';
import 'package:challenger/views/pages/explorer/views/all/all_view.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenges_view.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challengers_view.dart';
import 'package:challenger/views/pages/explorer/views/debates/debates_view.dart';
import 'package:challenger/views/pages/explorer/views/news/news_view.dart';
import 'package:challenger/views/pages/explorer/views/prizes/prizes_view.dart';
import 'package:challenger/views/pages/explorer/views/top_challengers/top_challengers_view.dart';
import 'package:challenger/views/pages/passepartout/passepartout_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ExplorerPage extends StatefulWidget {
  const ExplorerPage({Key? key}) : super(key: key);

  @override
  State<ExplorerPage> createState() => _ExplorerPageState();
}

class _ExplorerPageState extends State<ExplorerPage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  bool _hasNewMessage = false;

  void init() async {
    _hasNewMessage = await hasNewMessage(
        AppStorage.getInstance().getStaticUser().id.toString());
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    init();
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.actionBarColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 1,
                    offset: const Offset(0, 4),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    height: 65,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: AppColors.actionBarColor,
                    ),
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          onTap: () {
                            Get.to(PassepartoutPage(initialTabIndex: 0));
                          },
                          child: Container(
                            width: 44,
                            height: 44,
                            padding: const EdgeInsets.all(11),
                            decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black12,
                                      offset: Offset(0, 3))
                                ],
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(50)),
                            child: AppImage(
                              "ic_bot2.png",
                              color: AppColors.primary,
                              width: 24,
                            ),
                          ),
                        ),
                        const Spacer(),
                        AppImage(
                          "logo.png",
                          height: 40,
                        ),
                        const Spacer(),
                        InkWell(
                          onTap: () async {
                            await Get.to(ChatListPage());
                            init();
                          },
                          child: Container(
                            width: 44,
                            height: 44,
                            padding: const EdgeInsets.all(11),
                            decoration: BoxDecoration(
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black12,
                                      offset: Offset(0, 3))
                                ],
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(50)),
                            child: Stack(
                              children: [
                                const Icon(
                                  Icons.email_rounded,
                                  color: AppColors.primary,
                                  size: 22,
                                ),
                                if (_hasNewMessage)
                                  Container(
                                    width: 7,
                                    height: 7,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                  )
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ),
                  PreferredSize(
                    preferredSize: Size.fromHeight(kToolbarHeight),
                    child: Container(
                      child: TabBar(
                        controller: _tabController,
                        isScrollable: true,
                        labelColor: AppColors.primary,
                        labelStyle: const TextStyle(
                          fontSize: 16,
                        ),
                        unselectedLabelColor: AppColors.unSelectedTabColor,
                        indicatorColor: AppColors.primary,
                        indicatorSize: TabBarIndicatorSize.label,
                        tabs: const [
                          Tab(text: 'All'),
                          Tab(text: 'Top Challengers'),
                          Tab(text: 'Challenge'),
                          Tab(text: 'Challenger'),
                          // Tab(text: 'Prize'),
                          // Tab(text: 'Debates'),
                          // Tab(text: 'News'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: const [
                  AllView(),
                  TopChallengersView(),
                  ChallengesView(),
                  CoAndChallengersView(),
                  // PrizesView(),
                  // DebatesView(),
                  // NewsView(),
                ],
              ),
            ),
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //     backgroundColor: Colors.white,
      //     child: AppImage("ic_bot2.png",color: AppColors.primary,width: 24,),
      //     onPressed: (){Get.to(PassepartoutPage(initialTabIndex: 0));}),
    );
  }
}
