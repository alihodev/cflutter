import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/category_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/top_challenger_model.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenge_item.dart';
import 'package:challenger/views/pages/explorer/views/top_challengers/category_list.dart';
import 'package:challenger/views/pages/explorer/views/top_challengers/top_challenger_item.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class TopChallengersView extends StatefulWidget {
  const TopChallengersView({Key? key}) : super(key: key);

  @override
  State<TopChallengersView> createState() => _TopChallengersViewState();
}

class _TopChallengersViewState extends State<TopChallengersView> {
  List<ChallengeModel> challenges = [];
  String query = "";
  CategoryModel? category;

  void onCategoryUpdate(value) {
    setState(() {
      category = value;
    });
  }
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            // const SizedBox(
            //   height: 15,
            // ),
            // Container(
            //   margin: const EdgeInsets.symmetric(horizontal: 10),
            //   alignment: Alignment.centerLeft,
            //   child: AppText.h1("Category2", fontSize: 18,),
            // ),
            // const SizedBox(
            //   height: 15,
            // ),
            // CategoryList(
            //   onSelectCategory: onCategoryUpdate,
            // ),
            // const SizedBox(
            //   height: 15,
            // ),
            // category == null ? Container() :
            Expanded(
              child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                      color: AppColors.actionBarColor,
                      borderRadius: BorderRadius.circular(8)),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Expanded(
                        child: AppPaginatedListView<CoAndChallengerModel>(
                          url: "/challenger/top",
                          query: query,
                          onFetchData: (response) {
                            return response
                                .map<CoAndChallengerModel>(
                                    (e) => CoAndChallengerModel.fromJson(e))
                                .toList();
                          },
                          listener: AppPaginatedListListener(onReset: (){
                            index = 0;
                          }),
                          renderItem: (item) =>
                              TopChallengerItem(coAndChallengerModel: item, index: ++index,),
                        ),
                      ),
                    ],
                  )),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ],
    );
  }
}
