import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/top_challenger_model.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TopChallengerItem extends StatefulWidget {
  final CoAndChallengerModel coAndChallengerModel;
  int index;
  TopChallengerItem({Key? key, required this.coAndChallengerModel, required this.index})
      : super(key: key);

  @override
  State<TopChallengerItem> createState() => _TopChallengerItemState();
}

class _TopChallengerItemState extends State<TopChallengerItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: (){
      Get.to(ProfilePage(id: widget.coAndChallengerModel.id!,));
    },child: Column(
      children: [
        const SizedBox( height: 3,),
        Row(
          children: [
            const SizedBox(
              width: 20,
            ),
            // Container(
            //   width: 25,
            //   alignment: Alignment.center,
            //   child: AppText.h1(
            //   "${widget.index}",
            //   fontSize: 16,
            //   fontWeight: FontWeight.w600,
            // ),),
            // const SizedBox(
            //   width: 15,
            // ),
      CircleAvatar(
        radius: 20,
        backgroundImage:
        NetworkImage(widget.coAndChallengerModel.profileUrl!),
      ),
            const SizedBox(
              width: 15,
            ),
            AppText.body(
              "${widget.coAndChallengerModel.firstname} ${widget.coAndChallengerModel.lastname}",
              color: AppColors.primary,
              fontSize: 16,
            ),
            const Spacer(),
            Column(
              children: [
                const Icon(Icons.person_outline, size: 12,),
                const SizedBox(
                  height: 5,
                ),
                AppText.body(
                  '${widget.coAndChallengerModel.followers ?? 0} Followers',
                  fontSize: 12,
                ),
              ],
            ),
            const SizedBox(
              width: 20,
            ),
          ],
        ),
        const SizedBox(height: 3,),
        const Divider(endIndent: 20, indent: 20),
        const SizedBox(height: 3,)
      ],
    ),);
  }
}
