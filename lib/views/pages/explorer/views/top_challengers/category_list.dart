import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/category_model.dart';
import 'package:challenger/models/top_challenger_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CategoryList extends StatefulWidget {
  final Function(CategoryModel category) onSelectCategory;

  const CategoryList({Key? key, required this.onSelectCategory})
      : super(key: key);

  @override
  State<CategoryList> createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  List<CategoryModel> _list = [];
  int _selectedId = -1;
  bool _check = true;
  void onFirstCategorySelect() async {
    await Future.delayed(const Duration(milliseconds: 10));
    widget.onSelectCategory(_list[0]);
    setState(() {
      _selectedId = _list[0].id;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      child: _list.isEmpty & _check ? AppMagicFutureBuilder(
          url: "/category/",
          onSuccess: (result) {
            _list.clear();
            _list.add(CategoryModel(-2, "all"));
            for(var item in result) {
              _list.add(CategoryModel(item["id"], item["title"]));
            }
            if (_selectedId == -1 && _check) {
              _check = false;
              onFirstCategorySelect();
            }
            return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [..._list.map((e) => _categoryItem(e))],
              ),
            );
          }) : SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [..._list.map((e) => _categoryItem(e))],
        ),
      ),
    );
  }

  Widget _categoryItem(CategoryModel categoryModel) {
    print(_selectedId);
    return InkWell(
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        margin: const EdgeInsets.symmetric(horizontal: 3),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(
                color: categoryModel.id == (_selectedId == -1 ? 0 : _selectedId)
                    ? AppColors.primary
                    : AppColors.unSelectedCategory,
                width: 1)),
        child: AppText(categoryModel.value,
            fontSize: 14,
            color: categoryModel.id == (_selectedId == -1 ? 0 : _selectedId)
                ? AppColors.primary
                : AppColors.unSelectedCategory),
      ),
      onTap: () {
        setState(() {
          _selectedId = categoryModel.id;
        });
        widget.onSelectCategory(categoryModel);
      },
    );
  }
}
