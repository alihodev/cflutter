import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/news_model.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenge_item.dart';
import 'package:challenger/views/pages/explorer/views/news/news_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class NewsView extends StatefulWidget {
  const NewsView({Key? key}) : super(key: key);

  @override
  State<NewsView> createState() => _NewsViewState();
}

class _NewsViewState extends State<NewsView> {
  List<NewsModel> news = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            Expanded(
                child: AppPaginatedListView<NewsModel>(
              url: "/google.com",
              query: query,
              isFake: true,
              onFetchData: (response) {
                return response.map<NewsModel>((e) => NewsModel()).toList();
              },
              renderItem: (item) => NewsItem(newsModel: item),
            )),
          ],
        ),
      ],
    );
  }
}
