import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/news_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class NewsItem extends StatefulWidget {
  NewsModel newsModel;

  NewsItem({Key? key, required this.newsModel}) : super(key: key);

  @override
  State<NewsItem> createState() => _NewsItemState();
}

class _NewsItemState extends State<NewsItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.challengeCardBackground,
      shadowColor: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
      ),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      child: Container(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 5),
        child: Column(
          children: [
            AppText.h2(
              "Metmoi is big dark ui kit with 120+ screen for ios ",
              color: AppColors.primary,
            ),
            const SizedBox(
              height: 20,
            ),
            AppText.body(
                "The author, vice chairman of Ogilvy, shares why what’s irrational often works better than what’s considered to be rational. \n"
                "The author, vice chairman of Ogilvy, shares why what’s irrational often works better than what’s considered to be rational. Rory explains we take some actions based on a psychological rather than logical level. As marketers, we should appeal to this irrational side of our thinkingpsychological rather than "),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
