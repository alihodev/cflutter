import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/pages/contributor/build/build_countributor_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:get/get.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class CoAndChallengersView extends StatefulWidget {
  const CoAndChallengersView({Key? key}) : super(key: key);

  @override
  State<CoAndChallengersView> createState() => _CoAndChallengersViewState();
}

class _CoAndChallengersViewState extends State<CoAndChallengersView> {
  List<CoAndChallengerModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          AppSearchTextField(onChaneValue: (value){
            setState(() {
              query = value;
            });
          },),
          Expanded(
              child: AppPaginatedListView<CoAndChallengerModel>(
                url: "/challenger/",
                query: query,
                onFetchData: (response) {
                  return response
                      .map<CoAndChallengerModel>((e) => CoAndChallengerModel.fromJson(e))
                      .toList();
                },
                renderItem: (item) => CoAndChallengerItem(userModel: item),
              )),
        ],
      ),
      // Container(
      //   margin: EdgeInsets.all(16),
      //   child:
      //   Align(alignment: Alignment.bottomCenter, child: FloatingActionButton.extended(label: AppText("Co & Challengers" ,color: Colors.white,), backgroundColor: AppColors.primary, onPressed: (){
      //     Get.to(ProfilePage());
      //   },icon: const Icon(Icons.add),),),)
    ],);
  }
}
