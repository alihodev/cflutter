import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_follow_toggle_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CoAndChallengerItem extends StatefulWidget {
  CoAndChallengerModel userModel;

  CoAndChallengerItem({Key? key, required this.userModel})
      : super(key: key);

  @override
  State<CoAndChallengerItem> createState() => _CoAndChallengerItemState();
}

class _CoAndChallengerItemState extends State<CoAndChallengerItem> {
  bool isChallenger() {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.to(ProfilePage(id: widget.userModel.id!,));
      },
      child: Card(
        color: isChallenger()
            ? AppColors.challengerCardBackground
            : AppColors.coChallengerCardBackground,
        shadowColor: Colors.transparent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
        child: Container(
          padding:
              const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(
                      widget.userModel.profileUrl!,
                      width: 73,
                      height: 86,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppText.h2(
                            "${widget.userModel.firstname} ${widget.userModel.lastname}",
                            fontSize: 18,
                          ),
                          Row(
                            children: [
                              AppText.body(
                                "@${widget.userModel.username}",
                                color: AppColors.h1TextColor,
                                fontSize: 14,
                              ),
                              const Spacer(),
                              Transform.translate(
                                offset: Offset(0, -10),
                                child: AppFollowToggle.challenger(widget.userModel, follow: (){
                                  setState(() {
                                    widget.userModel.followers =( widget.userModel.followers ?? 0) + 1;
                                  });
                                },unfollow: (){
                                  setState(() {
                                    widget.userModel.followers = (widget.userModel.followers ?? 0) - 1;
                                  });
                                },),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Transform.translate(
                                offset: Offset(0, -5),
                                child: AppImage(
                                  "ic_followers.png",
                                  width: 16,
                                  height: 16,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              AppText.body(
                                '${widget.userModel.followers ?? 0} Follower${(widget.userModel.followers ?? 0) > 1 ? 's':''}',
                                fontSize: 14,
                                color: AppColors.h1TextColor,
                              ),
                              Expanded(child: Container()),
                              // InkWell(
                              //     child: AppImage("ic_bookmark.png",
                              //         width: 20, height: 20),
                              //     onTap: () {}),
                              const SizedBox(
                                width: 20,
                              ),
                              InkWell(
                                  child: Row(
                                    children: [
                                      AppText("Share", fontSize: 12,),
                                      const SizedBox(width: 5,),
                                      AppImage(
                                        "ic_send.png",
                                        height: 20,
                                        width: 20,
                                      ),
                                    ],
                                  ),
                                  onTap: () {
                                    AppShare.shareChallenger(widget.userModel);
                                  }),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
