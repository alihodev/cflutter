import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/all_explorer_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenge_item.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/pages/explorer/views/debates/debate_item.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AllExplorerItem extends StatefulWidget {
  AllExplorerModel allModel;
  AllExplorerItem({Key? key, required this.allModel}) : super(key: key);

  @override
  State<AllExplorerItem> createState() => _AllExplorerItemState();
}

class _AllExplorerItemState extends State<AllExplorerItem> {
  @override
  Widget build(BuildContext context) {
    switch(widget.allModel.type) {
      case 0 : return ChallengeItem(challengeModel: widget.allModel.challengeModel);
      case 1 : return CoAndChallengerItem(userModel: widget.allModel.coAndChallengerModel);
      // case 2 : return DebateItem(debateModel: widget.allModel.debateModel);
      default: return ChallengeItem(challengeModel: widget.allModel.challengeModel);
    }
  }
}
