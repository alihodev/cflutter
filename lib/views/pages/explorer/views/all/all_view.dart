import 'package:challenger/models/all_explorer_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/pages/explorer/views/all/all_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:flutter/material.dart';

class AllView extends StatefulWidget {
  const AllView({Key? key}) : super(key: key);

  @override
  State<AllView> createState() => _AllViewState();
}

class _AllViewState extends State<AllView> {
  List<AllExplorerModel> all = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 3,
        ),
        Expanded(
            child: AppPaginatedListView<AllExplorerModel>(
          url: "/challenge/all-challenge-challenger",
          query: query,
          onFetchData: (response) {
            return response.map<AllExplorerModel>((e) {
              AllExplorerModel all = AllExplorerModel();
              if (e['username'] != null) {
                all.coAndChallengerModel = CoAndChallengerModel.fromJson(e);
                all.type = 1;
              } else if (e['title'] != null) {
                all.challengeModel = ChallengeModel.fromJson(e);
                all.type = 0;
              }
              return all;
            }).toList();
          },
          renderItem: (item) => AllExplorerItem(allModel: item),
        )),
      ],
    );
  }
}
