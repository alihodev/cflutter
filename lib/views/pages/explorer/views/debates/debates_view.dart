import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/debates_model.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/pages/explorer/views/debates/debate_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DebatesView extends StatefulWidget {
  const DebatesView({Key? key}) : super(key: key);

  @override
  State<DebatesView> createState() => _DebatesViewState();
}

class _DebatesViewState extends State<DebatesView> {
  List<DebateModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            Expanded(
                child: AppPaginatedListView<DebateModel>(
              url: "/google.com",
              query: query,
              isFake: true,
              onFetchData: (response) {
                return response.map<DebateModel>((e) => DebateModel()).toList();
              },
              renderItem: (item) => DebateItem(debateModel: item),
            )),
          ],
        ),
        Container(
          margin: const EdgeInsets.all(16),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton.extended(
              label: AppText(
                "New Debate",
                color: Colors.white,
              ),
              backgroundColor: AppColors.primary,
              onPressed: () {
                Get.to(BuildDebatePage());
              },
              icon: const Icon(Icons.add),
            ),
          ),
        )
      ],
    );
  }
}
