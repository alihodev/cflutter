import 'dart:convert';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/category_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/views/pages/challenge/build/build_challenge.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenge_item.dart';
import 'package:challenger/views/pages/explorer/views/top_challengers/category_list.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChallengesView extends StatefulWidget {
  const ChallengesView({Key? key}) : super(key: key);

  @override
  State<ChallengesView> createState() => _ChallengesViewState();
}

class _ChallengesViewState extends State<ChallengesView> {
  List<ChallengeModel> challenges = [];
  String query = "";
  CategoryModel? category;

  void onCategoryUpdate(value) {
    setState(() {
      category = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          AppSearchTextField(onChaneValue: (value) {
            setState(() {
              query = value;
            });
          },),
          const SizedBox(
            height: 15,
          ),
          CategoryList(
            onSelectCategory: onCategoryUpdate,
          ),
          const SizedBox(
            height: 15,
          ),
          category == null ? Container() :
          Expanded(
              child: AppPaginatedListView<ChallengeModel>(
                url: "/challenge/",
                query: query,
                category: category!.id < 0 ? "" : category!.id.toString(),
                onFetchData: (response) {
                  return response
                      .map<ChallengeModel>((e) => ChallengeModel.fromJson(e))
                      .toList();
                },
                renderItem: (item) => ChallengeItem(challengeModel: item),
              )),
        ],
      ),
      // Container(
      //   margin: EdgeInsets.all(16),
      //   child:
      // Align(alignment: Alignment.bottomCenter, child: FloatingActionButton.extended(label: AppText("New Challenge" ,color: Colors.white,), backgroundColor: AppColors.primary, onPressed: (){
      //   Get.to(BuildChallengePage());
      // },icon: const Icon(Icons.add),),),)
    ],);
  }
}
