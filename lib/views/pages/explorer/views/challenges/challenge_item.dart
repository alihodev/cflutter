import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_follow_toggle_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChallengeItem extends StatefulWidget {
  ChallengeModel challengeModel;
  EdgeInsetsGeometry? margin;
  ChallengeItem({Key? key, required this.challengeModel, this.margin = const EdgeInsets.symmetric(horizontal: 10, vertical: 7),}) : super(key: key);

  @override
  State<ChallengeItem> createState() => _ChallengeItemState();
}

class _ChallengeItemState extends State<ChallengeItem> {

  void coChallenge(int id) {
    AppLoadingDialog.show();
    ChallengeRepository().coChallenge(
        id,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          Get.to(ChallengePage(
              initialTabIndex: 0,
              id: response['id'],
              title: response['title']));
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            coChallenge(id);
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: (){
      Get.to(ChallengePage(initialTabIndex: 0, id: widget.challengeModel.id!, title: widget.challengeModel.title!,));
    },child: Card(
      color: AppColors.challengeCardBackground,
      shadowColor: Colors.transparent,
      margin: widget.margin,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
      ),
      child: Container(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 5),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: (widget.challengeModel.coverUrl!.startsWith("http")) ? Image.network(
                    widget.challengeModel.coverUrl!,
                    width: 73,
                    height: 86,
                    fit: BoxFit.cover,
                  ) : Container(),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          AppText.h2(widget.challengeModel.title ?? "", fontSize: 18,),
                          const Spacer(),
                          SizedBox(width: 31,child:
                          InkWell(
                              child: AppImage(
                                "ic_co_challenge.png",
                                height: 20,
                                width: 16,
                              ),
                              onTap: () {
                                AppDialog.showAlertDialog(
                                    description:
                                    "Do you want to be co challenger with this challenge?",
                                    onAction: () {
                                      coChallenge(widget.challengeModel.id!);
                                    });
                              }),),
                          SizedBox(width: 31,child:
                          InkWell(
                              child: AppImage(
                                "ic_send.png",
                                height: 20,
                                width: 16,
                              ),
                              onTap: () {
                                AppShare.shareChallenge(widget.challengeModel);
                              }),),
                        ],),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            InkWell(onTap: (){
                              Get.to(ProfilePage(id: widget.challengeModel.challenger!.id!));
                            },child: AppText.body(
                              "@${widget.challengeModel.challenger!.username!}" ,
                              color: AppColors.h1TextColor,
                              fontSize: 14,
                            ),),
                            Spacer(),
                            AppText.body("${widget.challengeModel.coChallengers ?? 0} Co Challenger${(widget.challengeModel.coChallengers ?? 0) > 1 ? "s":""}", fontSize: 12,)
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            AppImage(
                              "ic_followers.png",
                              width: 15,
                              height: 15,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            AppText.body(
                              '${widget.challengeModel.followers ?? 0} Follower${(widget.challengeModel.followers ?? 0) > 1 ? 's':''}',
                              fontSize: 14,
                              color: AppColors.h1TextColor,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Transform.translate(
              offset: Offset(0, -8),
              child: Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: LinearProgressIndicator(
                          value: (widget.challengeModel.progress ?? 0) / 100,
                          minHeight: 4,
                          valueColor:
                          const AlwaysStoppedAnimation<Color>(AppColors.primary),
                          backgroundColor: AppColors.secondary,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 45, child: Transform.translate(
                      offset: Offset(0, 5),
                      child: Container(
                        child: AppText.body(
                          "${widget.challengeModel.progress ?? 0}%",
                          fontSize: 14,
                        ),
                      ),
                    ),),
                    const SizedBox(
                      width: 15,
                    ),
                    Transform.translate(
                      offset: Offset(0, -15),
                      child: AppFollowToggle.challenge(widget.challengeModel, follow: (){
                        setState(() {
                          widget.challengeModel.followers = (widget.challengeModel.followers ?? 0) + 1;
                        });
                      }, unfollow: (){
                        setState(() {
                          widget.challengeModel.followers = (widget.challengeModel.followers ?? 0) - 1;
                        });
                      },),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    ),);
  }
}
