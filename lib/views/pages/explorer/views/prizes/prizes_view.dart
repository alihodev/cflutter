import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/prize_model.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/pages/prize/build/build_prize_page.dart';
import 'package:get/get.dart';
import 'package:challenger/views/pages/explorer/views/prizes/prize_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class PrizesView extends StatefulWidget {
  const PrizesView({Key? key}) : super(key: key);

  @override
  State<PrizesView> createState() => _PrizesViewState();
}

class _PrizesViewState extends State<PrizesView> {
  List<PrizeModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
              onChaneValue: (value) {
                setState(() {
                  query = value;
                });
              },
            ),
            Expanded(
                child: AppPaginatedListView<PrizeModel>(
              url: "/google.com",
              query: query,
              isFake: true,
              onFetchData: (response) {
                return response.map<PrizeModel>((e) => PrizeModel()).toList();
              },
              renderItem: (item) => PrizeItem(prizeModel: item),
            )),
          ],
        ),
        Container(
          margin: const EdgeInsets.all(16),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton.extended(
              label: AppText(
                "New Prize",
                color: Colors.white,
              ),
              backgroundColor: AppColors.primary,
              onPressed: () {
                Get.to(BuildPrizePage());
              },
              icon: const Icon(Icons.add),
            ),
          ),
        )
      ],
    );
  }
}
