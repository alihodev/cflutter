import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/prize_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class PrizeItem extends StatefulWidget {
  PrizeModel prizeModel;
  PrizeItem({Key? key, required this.prizeModel}) : super(key: key);

  @override
  State<PrizeItem> createState() => _PrizeItemState();
}

class _PrizeItemState extends State<PrizeItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.challengeCardBackground,
      shadowColor: Colors.transparent,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
      ),
      child: Container(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AppText.h2('Title', fontSize: 18,),

                        AppText.body(
                          'This is a summery text of description...',
                          fontSize: 14,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            AppText.h1(
                              "\$  1,000.00   USD",
                              fontSize: 16,
                              color: AppColors.primary,
                            ),
                            const Spacer(),
                            InkWell(
                                child: AppImage(
                                  "lucide_share.png",
                                  height: 22,
                                  width: 22,
                                ),
                                onTap: () {}),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
