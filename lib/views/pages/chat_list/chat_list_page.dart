import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/ModelProvider.dart';
import 'package:challenger/models/chat_list_model.dart' as chatList;
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:get/get.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/chat/chat_page.dart';
import 'package:challenger/views/pages/clubs/views/chats/chat_item.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class ChatListPage extends StatefulWidget {
  const ChatListPage({Key? key}) : super(key: key);

  @override
  State<ChatListPage> createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  List<chatList.ChatListModel> challenges = [];
  String query = "";
  StreamSubscription<QuerySnapshot<Chat>>? _stream;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    observeQuery();
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "Chats",
        logoWithTitle: true,
        body: Stack(
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                    child: ListView.builder(
                  itemCount: challenges.length,
                  itemBuilder: (context, index) {
                    final reversedIndex = challenges.length - 1 - index;
                    final item = challenges[reversedIndex];
                    if (item.user == null) {
                      return Container();
                    }
                    return ChatItem(
                      chatModel: item,
                      onClick: (chatModel) async {
                        AppLoadingDialog.show();
                        if (_stream != null) {
                          await _stream!.cancel();
                        }
                        AppLoadingDialog.hide();
                        await Get.to(ChatPage(
                          coAndChallengerModel: CoAndChallengerModel.fromJson(
                              chatModel.user!.toJson()),
                          chat: chatModel.chat,
                        ));
                        observeQuery();
                      },
                    );
                  },
                )),
              ],
            ),
          ],
        ));
  }

  void observeQuery() {
    _stream = Amplify.DataStore.observeQuery(Chat.classType,
        where: Chat.SENDERID.eq(AppStorage.getInstance().getStaticUser().id).or(
            Chat.RECEIVERID.eq(AppStorage.getInstance().getStaticUser().id)),
        sortBy: [
          Chat.LASTMESSAGEDATE.ascending()
        ]).listen((QuerySnapshot<Chat> snapshot) {
      if (snapshot.items.isNotEmpty) {
        updateList(snapshot.items);
        _stream!.cancel();
      }
    });
  }

  void updateList(List<Chat> list) {
    String currentUserId =
        AppStorage.getInstance().getStaticUser().id.toString();
    challenges.clear();
    List<String> ids = [];
    for (var i = 0; i < list.length; i++) {
      if (list[i].senderId == currentUserId &&
          list[i].receiverId == currentUserId) {
        ids.add(list[i].senderId!);
      } else {
        if (list[i].senderId == currentUserId) {
          ids.add(list[i].receiverId!);
        } else {
          ids.add(list[i].senderId!);
        }
      }

      chatList.ChatListModel chatListModel = chatList.ChatListModel();
      chatListModel.user1Id = list[i].senderId;
      chatListModel.user2Id = list[i].receiverId;

      if (list[i].senderId == currentUserId &&
          list[i].receiverId == currentUserId) {
        chatListModel.hasNewMessage = false;
      } else {
        if (currentUserId == list[i].senderId) {
          chatListModel.hasNewMessage = list[i].senderHasNewMessage;
        } else {
          chatListModel.hasNewMessage = list[i].receiverHasNewMessage;
        }
      }
      chatListModel.id = list[i].id;
      chatListModel.lastMessage = list[i].lastMessage;
      chatListModel.createdAt = list[i].lastMessageDate.toString();

      chatListModel.chat = list[i];
      challenges.add(chatListModel);
    }
    UserRepository().getUsersById(
        ids,
        AppDataListener<List<CoAndChallengerModel>>(
            onSuccess: (data) {
              for (var value in challenges) {
                for (var user in data) {
                  if (user.id.toString() == value.user1Id &&
                      user.id.toString() == value.user2Id) {
                    value.user = chatList.User.fromJson(user.toJson());
                  } else {
                    if ((user.id.toString() == value.user1Id &&
                            currentUserId == value.user2Id) ||
                        (user.id.toString() == value.user2Id &&
                            currentUserId == value.user1Id)) {
                      value.user = chatList.User.fromJson(user.toJson());
                    }
                  }
                }
              }

              setState(() {});
            },
            onFailure: (message) {}));
  }

  @override
  void dispose() async {
    if (_stream != null) {
      await _stream!.cancel();
    }
    super.dispose();
  }
}
