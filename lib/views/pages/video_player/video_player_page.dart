import 'dart:io';

import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/data/repository/daily_report_reporitory.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/chat/chat_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_follow_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerPage extends StatefulWidget {
  bool fromReels;
  String? path;
  ChallengeModel? challengeModel;
  String title = "The world, your life";
  String subTitle = "June Cook";
  String videoUrl =
      "https://assets.mixkit.co/videos/preview/mixkit-young-man-missing-a-bowling-shot-49117-large.mp4";
  String userProfile =
      '';
  String username = "phoenix";
  String name = "Olivia Rhye";
  bool isDaily = false;
  bool isChallengerIntro = false;
  bool isChallengeIntro = false;
  int? userId;
  CoAndChallengerModel? coAndChallengerModel;
  DailyReportModel? dailyReportModel;
  CoAndChallengerModel? userModel;

  VideoPlayerPage(
      {Key? key, this.fromReels = false, this.path, this.challengeModel})
      : super(key: key);

  @override
  State<VideoPlayerPage> createState() => _VideoPlayerPageState();

  VideoPlayerPage.playDailyReportVideo(this.dailyReportModel,
      {this.fromReels = false}) {
    userModel = CoAndChallengerModel.fromJson(dailyReportModel!.challenger!.toJson());
    videoUrl = dailyReportModel!.dailyVideoUrl!;
    username = dailyReportModel!.challenger!.username!;
    userProfile = dailyReportModel!.challenger!.profileUrl!;
    title = dailyReportModel!.challengeName ?? "Challenge Name";
    subTitle = "";
    isDaily = true;
    name = "${dailyReportModel!.challenger!.firstname!} ${dailyReportModel!.challenger!.lastname!}";
    userId = dailyReportModel!.challenger!.id;
  }

  VideoPlayerPage.playChallengerIntroVideo(this.coAndChallengerModel,
      {this.fromReels = false}) {
    userModel = coAndChallengerModel;
    videoUrl = coAndChallengerModel!.introVideoUrl!;
    username = coAndChallengerModel!.username!;
    userProfile = coAndChallengerModel!.profileUrl!;
    title = "";
    subTitle = "";
    isChallengerIntro = true;
    userId = coAndChallengerModel!.id;
    name = "${coAndChallengerModel!.firstname!} ${coAndChallengerModel!.lastname!}";
  }

  VideoPlayerPage.playChallengeVideo(this.challengeModel,
      {this.fromReels = false}) {
    userModel = CoAndChallengerModel.fromJson(challengeModel!.challenger!.toJson());
    videoUrl = challengeModel!.introVideoUrl!;
    title = challengeModel!.title!;
    subTitle = challengeModel!.description!;
    username = challengeModel!.challenger!.username!;
    userProfile = challengeModel!.challenger!.profileUrl!;
    isChallengeIntro = true;
    userId = challengeModel!.challenger!.id;
    name = "${challengeModel!.challenger!.firstname!} ${challengeModel!.challenger!.lastname!}";
  }

  VideoPlayerPage.playFromFile(String this.path, {this.fromReels = false});
}

class _VideoPlayerPageState extends State<VideoPlayerPage> {
  bool _showLikeWidget = false;
  late VideoPlayerController _controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
                color: Colors.black,
                child: widget.path != null
                    ? AppVideoPlayer.playFile(
                        file: File.fromUri(Uri.parse(widget.path!)),
                        enableProgressBar: true,
                      )
                    : AppVideoPlayer.play(
                        url: widget.videoUrl,
                        // "https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/720/Big_Buck_Bunny_720_10s_5MB.mp4",
                        enableProgressBar: true,
                        fromReels: widget.fromReels,
                  controller: (controller) {
                          _controller = controller;
                  },

                      )),
            Column(
              children: [
                if (!widget.fromReels)
                  Row(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          width: 50,
                          height: 50,
                          margin: const EdgeInsets.only(left: 10, top: 10),
                          padding: const EdgeInsets.all(15),
                          child: InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: AppImage(
                              "ic_arrow_left.png",
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      const Spacer(),
                      InkWell(
                          onTap: () {
                            AppDialog.showReportDialog(
                                title: "Report",
                                description: "Why do you want to report this content?", onAction: (String description){
                              AppLoadingDialog.show();
                              if (widget.isChallengerIntro) {
                                UserRepository().reportUser(widget.coAndChallengerModel!.id!, description,
                                    AppDataListener(onSuccess: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }, onFailure: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }));
                              }else if (widget.isChallengeIntro) {
                                ChallengeRepository().reportChallenge(widget.challengeModel!.id!, description,
                                    AppDataListener(onSuccess: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }, onFailure: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }));
                              } else if (widget.isDaily) {
                                DailyReportRepository().reportDailyReport(widget.dailyReportModel!.id!, description,
                                    AppDataListener(onSuccess: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }, onFailure: (message) {
                                      AppLoadingDialog.hide();
                                      AppMessage.showDialogMessage(message);
                                    }));
                              } else {
                                AppLoadingDialog.hide();
                              }
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 10),
                            width: 50,
                            child: Icon(Icons.report_gmailerrorred, color: Colors.white,),
                          ))
                    ],
                  ),
                const SizedBox(
                  height: 10,
                ),
                if (widget.path == null)
                  InkWell(onTap: () async {
                    if (widget.dailyReportModel != null) {
                      _controller.pause();
                      await Get.to(ChallengePage(initialTabIndex: 0, id: widget.dailyReportModel!.challengeId!, title: widget.dailyReportModel!.challengeName ?? "Challenge name"));
                      _controller.play();
                    } else if (widget.challengeModel != null) {
                      _controller.pause();
                      await Get.to(ChallengePage(initialTabIndex: 0, id: widget.challengeModel!.id!, title: widget.challengeModel!.title!));
                      _controller.play();
                    }
                  },child:
                  AppText(
                    widget.title,
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                  ),),
                if (widget.path == null)
                  AppText(
                    widget.subTitle,
                    color: Colors.white,
                    fontSize: 16,
                    maxLines: 1,
                    overFlow: TextOverflow.ellipsis,
                    fontWeight: FontWeight.w400,
                  ),
                const Spacer(),
                if (widget.path == null)
                  Container(
                    margin: const EdgeInsets.only(right: 20),
                    alignment: Alignment.centerRight,
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              _showLikeWidget = true;
                            });
                          },
                          child: Column(
                            children: [
                              if (widget.isChallengerIntro)
                                AppLikeToggle.challenger(
                                  widget.coAndChallengerModel,
                                  fromVideo: true,
                                ),
                              if (widget.isChallengeIntro)
                                AppLikeToggle.challenge(widget.challengeModel,
                                    fromVideo: true),
                              if (widget.isDaily)
                                AppLikeToggle.dailyReport(
                                    widget.dailyReportModel,
                                    fromVideo: true),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        InkWell(
                          onTap: () async {
                            _controller.pause();
                            await Get.to(ChatPage(coAndChallengerModel: widget.userModel!,));
                            _controller.play();
                          },
                          child: Column(
                            children: [
                              AppImage(
                                "ic_message.png",
                                color: Colors.white,
                                width: 20,
                              ),
                              AppText(
                                "Message",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        InkWell(
                          onTap: () {
                            if (widget.isChallengerIntro) {
                              AppShare.shareChallenger(
                                  widget.coAndChallengerModel!);
                            } else if (widget.isChallengeIntro) {
                              AppShare.shareChallenge(widget.challengeModel!);
                            } else if (widget.isDaily) {
                              AppShare.shareDailyReport(
                                  widget.dailyReportModel!);
                            }
                          },
                          child: Column(
                            children: [
                              AppImage(
                                "ic_send.png",
                                color: Colors.white,
                                width: 18,
                              ),
                              AppText(
                                "Share",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                const SizedBox(
                  height: 10,
                ),
                if (widget.path == null)
                  InkWell(onTap: () async {
                    _controller.pause();
                    await Get.to(ProfilePage(id: widget.userId!));
                    _controller.play();
                  },child: Container(
                    margin: const EdgeInsets.only(right: 20, left: 20),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 48,
                          height: 48,
                          child: Stack(
                            children: [
                              CircleAvatar(
                                radius: 24,
                                backgroundImage:
                                NetworkImage(widget.userProfile),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppText.body(
                              widget.name,
                              fontSize: 16,
                              color: Colors.white,
                            ),
                            AppText.body(
                              "@${widget.username}",
                              fontSize: 14,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        const Spacer(),
                        if (widget.isChallengerIntro)
                          AppFollowToggle.challenger(
                              widget.coAndChallengerModel, follow: (){}, unfollow: (){},)
                      ],
                    ),
                  ),),
                SizedBox(
                  height: widget.fromReels ? 60 : 60,
                )
              ],
            ),
            AppLikeWidget(
              show: _showLikeWidget,
              onHide: () {
                setState(() {
                  _showLikeWidget = false;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
