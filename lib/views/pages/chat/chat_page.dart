import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/chat_reporitory.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/ModelProvider.dart';
import 'package:challenger/models/chat_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/amplify_chat_service.dart';
import 'package:challenger/services/amplify_message_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/chat/chat_item.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChatPage extends StatefulWidget {
  CoAndChallengerModel coAndChallengerModel;
  Chat? chat;

  ChatPage({Key? key, required this.coAndChallengerModel, this.chat = null})
      : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final textController = TextEditingController();
  List<ChatModel> chats = [];
  bool isDone = false;
  late UserModel userModel;
  bool _amplifyUserConfig = false;
  bool _isBlock = true;

  StreamSubscription<QuerySnapshot<Message>>? _stream;
  late String receiverId;
  late String senderId;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void _getUser() async {
    userModel = await AppStorage.getInstance().getUser();
  }

  void updateView() async {
    if (isDone) {
      return;
    }
    isDone = true;
    _getMessages();
    await Future.delayed(const Duration(milliseconds: 100));
    setState(() {});
  }

  void _getMessages() async {
    print('start get messages');
    await Future.delayed(Duration(seconds: 4));
    ChatRepository().getMessages(
        widget.coAndChallengerModel.id!,
        AppDataListener(onSuccess: (response) {
          chats.clear();
          for (var i = 0; i < response.length; i++) {
            ChatModel chatModel = ChatModel.fromJson(response[i]);
            chatModel.isCurrentUser = chatModel.senderId == userModel.id;
            chats.add(chatModel);
          }
          setState(() {});
          _getMessages();
        }, onFailure: (message) {
          _getMessages();
        }));
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title:
            "${widget.coAndChallengerModel.firstname} ${widget.coAndChallengerModel.lastname}",
        body: _amplifyUserConfig
            ? Stack(
                children: [
                  Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                          child: ListView.builder(
                        reverse: true,
                        itemCount: chats.length,
                        itemBuilder: (context, index) {
                          final reversedIndex = chats.length - 1 - index;
                          final item = chats[reversedIndex];
                          return ChatItem(
                            chatModel: item,
                            coAndChallengerModel: widget.coAndChallengerModel,
                          );
                        },
                      )),
                      const Divider(
                        height: 2,
                        color: Color(0xff323F4B),
                      ),
                      if (!_isBlock)
                        Row(
                          children: [
                            Expanded(
                              child: AppTextField(
                                borderRadius: 0,
                                textEditingController: textController,
                                borderColor: Colors.transparent,
                                hintText: "Type something ...",
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            InkWell(
                                onTap: () async {
                                  if (textController.text.isNotEmpty) {
                                    await createMessage(
                                        chatId: widget.chat!.id,
                                        message: textController.text,
                                        senderId: senderId,
                                        receiverId: receiverId);
                                    widget.chat = await updateChat(
                                        AppStorage.getInstance()
                                            .getStaticUser()
                                            .id
                                            .toString(),
                                        widget.chat!,
                                        textController.text);
                                    setState(() {
                                      textController.text = "";
                                      // chats.add(model);
                                    });
                                  }
                                },
                                child: const SizedBox(
                                  width: 50,
                                  height: 40,
                                  child: Icon(Icons.send),
                                ))
                          ],
                        )
                    ],
                  ),
                ],
              )
            : amplifyUserConfig());
  }

  Widget amplifyUserConfig() {
    configUsers();
    return const Center(
        child: SpinKitThreeBounce(
      color: AppColors.primary,
      size: 20.0,
    ));
  }

  void configUsers() async {
    UserRepository().checkBlock(widget.coAndChallengerModel.id!,
        AppDataListener(onSuccess: (isBlock) {
          if (_amplifyUserConfig) {
            setState(() {
              _isBlock = isBlock;
            });
          } else {
            _isBlock = isBlock;
          }
        }, onFailure: (message) {}));
    safePrint("starting get current user id");
    senderId = AppStorage.getInstance().getStaticUser().id.toString();
    receiverId = widget.coAndChallengerModel.id.toString();
    safePrint(receiverId);
    if (widget.chat == null) {
      Chat? chat = await getAmplifyChatId(senderId, receiverId);
      widget.chat = chat!;
    }
    safePrint("chatID: ${widget.chat!.id}");
    setState(() {
      _amplifyUserConfig = true;
    });
    observeQuery();
  }

  void observeQuery() {
    _stream = Amplify.DataStore.observeQuery(Message.classType,
            where: Message.CHATID.eq(widget.chat!.id),
            sortBy: [Message.CREATEDAT.ascending()])
        .listen((QuerySnapshot<Message> snapshot) {
      print(snapshot.items);
      chats.clear();
      for (var i = 0; i < snapshot.items.length; i++) {
        ChatModel chatModel = ChatModel();
        chatModel.senderId = snapshot.items[i].senderId;
        chatModel.receiverId = snapshot.items[i].receiverId;
        chatModel.message = snapshot.items[i].message;
        chatModel.id = snapshot.items[i].id;
        chatModel.conversationId = snapshot.items[i].chatId;
        chatModel.createdAt = snapshot.items[i].createdAt.toString();
        chatModel.isCurrentUser = chatModel.senderId ==
            AppStorage.getInstance().getStaticUser().id.toString();
        setState(() {
          chats.add(chatModel);
        });
      }
    });
  }

  @override
  void dispose() {
    if (_stream != null) {
      _stream!.cancel();
    }
    seenChat(
        AppStorage.getInstance().getStaticUser().id.toString(), widget.chat!);
    super.dispose();
    textController.dispose();
  }
}
