import 'package:challenger/models/chat_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get_time_ago/get_time_ago.dart';

class ChatItem extends StatefulWidget {
  ChatModel chatModel;
  CoAndChallengerModel coAndChallengerModel;

  ChatItem({Key? key, required this.chatModel, required this.coAndChallengerModel}) : super(key: key);

  @override
  State<ChatItem> createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    if (!widget.chatModel.isCurrentUser) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 40,
                  height: 40,
                  child: Stack(
                    children: [
                      CircleAvatar(
                        radius: 20,
                        backgroundImage: NetworkImage(
                            widget.coAndChallengerModel.profileUrl!),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width - 100,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,

                        children: [
                        Container(
                          padding:
                          const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          decoration: const BoxDecoration(
                              color: Color(0xfff4f4f4),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(24),
                                  bottomRight: Radius.circular(24),
                                  topRight: Radius.circular(24))),
                          child:  AppText.body(
                            widget.chatModel.message!,
                            color: const Color(0xff101828),
                          ),),
                      ],)
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        AppText.body(
                          GetTimeAgo.parse(DateTime.parse(widget.chatModel.createdAt!)),
                          fontSize: 12,
                          color: const Color(0xff667085),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                      ],
                    )
                  ],
                )
              ],
            )
          ],
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      margin: const EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          const SizedBox(
            width: 80,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 5,
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: const BoxDecoration(
                      color: Color(0xfff4f4f4),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24),
                          bottomLeft: Radius.circular(24),
                          topRight: Radius.circular(24))),
                  child: AppText.body(
                    widget.chatModel.message!,
                    color: const Color(0xff101828),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AppText.body(
                      GetTimeAgo.parse(DateTime.parse(widget.chatModel.createdAt!)),
                      fontSize: 12,
                      color: const Color(0xff667085),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
