import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/login/sign_in/sign_in_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field_with_icon.dart';
import 'package:challenger/views/widgets/common/login_action_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final rePasswordController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  var privacyCheck = false;

  Widget _action() {
    return AppMagicButton<String>(
      text: "Sign Up Now",
      postUrl: "/account/register",
      beforeProcess: () {
        if (!privacyCheck) {
          AppMessage.showDialogMessage(
              "You should accept the privacy policy to continue");
          return false;
        }
        return true;
      },
      getData: () {
        return RequestBodyBuilder()
            .addField("username", usernameController.text)
            .addField("email", emailController.text)
            .addField("password", passwordController.text)
            .addField("confirmPassword", rePasswordController.text)
            .addField("phone", phoneController.text)
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) async{
        AppStorage.getInstance().setUserToken(response['data']['token']);
        AppStorage.getInstance().setUser(UserModel.fromJson(response['data']['user']));
        AppStorage.getInstance().setUserLogin();
        await Future.delayed(const Duration(milliseconds: 400));
        Get.offAll(() => MainPage());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: SingleChildScrollView(
              child: SizedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      width: 330,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              AppButton.flat(
                                text: "SIGN UP",
                                onPressed: () {},
                                width: 80,
                              ),
                              AppButton.flat(
                                text: "LOG IN",
                                onPressed: () {
                                  Get.to(const SignInPage());
                                },
                                textColor: const Color(0xff8C8C8C),
                                width: 80,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 30),
                            width: 330,
                            decoration: BoxDecoration(
                                color: AppColors.secondary,
                                borderRadius: BorderRadius.circular(16)),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AppTextFieldWithIcon(
                                  hintText: "Username",
                                  icon: "ic_user.png",
                                  textEditingController: usernameController,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                AppTextFieldWithIcon(
                                  hintText: "Your Email",
                                  icon: "ic_email.png",
                                  keyboardType: TextInputType.emailAddress,
                                  textEditingController: emailController,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                AppTextFieldWithIcon(
                                  hintText: "Password",
                                  icon: "ic_padlock.png",
                                  textEditingController: passwordController,
                                  obscureText: true,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                AppTextFieldWithIcon(
                                  hintText: "Reenter Your Password",
                                  icon: "ic_padlock.png",
                                  textEditingController: rePasswordController,
                                  obscureText: true,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                AppTextFieldWithIcon(
                                  hintText: "Phone Number",
                                  icon: "ic_smartphone.png",
                                  textEditingController: phoneController,
                                  keyboardType: TextInputType.phone,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                _action(),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            width: 330,
                            child: CheckboxListTile(
                              title: AppText.body(
                                "By continuing, you agree to Term of Use and confirm that you have read Privacy Policy.",
                                fontSize: 14,
                              ),
                              value: privacyCheck,
                              onChanged: (value) {
                                setState(() {
                                  privacyCheck = value!;
                                });
                              },
                              controlAffinity: ListTileControlAffinity.leading,
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: const [
              LoginActionBar(),
              SizedBox(
                height: 20,
              ),
              Expanded(child: SizedBox())
            ],
          ),
        ],
      ),
    );
  }
}
