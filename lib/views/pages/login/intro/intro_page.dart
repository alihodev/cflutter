import 'dart:io';

import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/firebase_options.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/login/intro/image_banner.dart';
import 'package:challenger/views/pages/login/sign_in/sign_in_page.dart';
import 'package:challenger/views/pages/login/sign_up/sign_up_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/pages/profile/personal_profile_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_core/firebase_core.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        body: Center(
      child: Column(
        children: [
          Expanded(
              child: Center(
            child: Stack(
              children: [
                RotatedBox(
                  quarterTurns: 1,
                  child: AppImage(
                    "logo.png",
                    height: 104,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 80, top: 35),
                  child: AppImage(
                    "logo_text.png",
                    width: 119,
                  ),
                ),
              ],
            ),
          )),
          AppButton(
              text: "Login by email",
              onPressed: () {
                Get.to(const SignInPage());
              }),
          const SizedBox(
            height: 10,
          ),
          if (Platform.isAndroid)
            AppButton.flat(
                text: "Login by Gmail",
                onPressed: () {
                  //Get.to(const SignUpPage());
                  signInWithGoogle();
                }),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    ));
  }

  Future<void> signInWithGoogle() async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      UserCredential result = await auth.signInWithCredential(authCredential);
      User? user = result.user;

      if (user != null) {
        AppLoadingDialog.show();
        var t = await user.getIdToken();
        UserRepository().googleConfirm(
            user.email!,
            t!,
            AppDataListener(onSuccess: (response) async {
              AppStorage.getInstance().setUserToken(response['data']['token']);
              AppStorage.getInstance()
                  .setUser(UserModel.fromJson(response['data']['user']));
              AppStorage.getInstance().setUserLogin();
              AppLoadingDialog.hide();
              await Future.delayed(const Duration(milliseconds: 400));
              CoAndChallengerModel challenger = CoAndChallengerModel.fromJson(response["data"]["user"]);
              if (!challenger.challenger!) {
                Get.offAll(() => PersonalProfilePage(coAndChallengerModel: challenger, fromRegister: true,));
              } else {
                Get.offAll(() => MainPage());
              }
            }, onFailure: (message) {
              AppLoadingDialog.hide();
              AppMessage.showDialogMessage(message);
            }));
      }
    }
  }
}
