import 'package:challenger/models/province_model.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/cupertino.dart';

class ImageBanner extends StatefulWidget {
  const ImageBanner({Key? key}) : super(key: key);

  @override
  State<ImageBanner> createState() => _ImageBannerState();
}

class _ImageBannerState extends State<ImageBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: AppMagicFutureBuilder(
          url: "/provincse/1",
          onSuccess: (result) {
            var provicne = ProvinceModel.fromJson(result["province"]);
            return Center(
              child: AppText(provicne.name!),
            );
          }),
    );
  }
}
