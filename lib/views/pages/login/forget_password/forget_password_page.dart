import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/login_action_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgetPasswordPage extends StatefulWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgetPasswordPage> createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  final _emailController = TextEditingController();

  Widget _action() {
    return AppMagicButton<String>(
      text: "Reset Password",
      postUrl: "/auth/reset-password",
      width: double.maxFinite,
      getData: () {
        return RequestBodyBuilder()
            .addField("email", _emailController.text)
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) {
        AppMessage.showDialogMessage(response['message'], onOkAction: () {
          Get.back();
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        body: Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          const LoginActionBar(),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 55,
            height: 55,
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: AppColors.primary,
                borderRadius: BorderRadius.circular(50)),
            child: AppImage("ic_key.png"),
          ),
          const SizedBox(
            height: 10,
          ),
          AppText.h1("Forgot password?"),
          AppText("No worries, we’ll send you reset instructions."),
          const SizedBox(
            height: 20,
          ),
          AppTextField(
            label: "Email",
            borderRadius: 8,
            hintText: "Enter your email",
            textEditingController: _emailController,
          ),
          const SizedBox(
            height: 20,
          ),
          const Spacer(),
          _action(),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    ));
  }
}
