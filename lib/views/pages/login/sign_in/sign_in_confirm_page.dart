import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/login/forget_password/forget_password_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/pages/profile/personal_profile_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field_with_icon.dart';
import 'package:challenger/views/widgets/common/login_action_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignInConfirmPage extends StatefulWidget {
  String email;
  String responseMessage;
  SignInConfirmPage({Key? key, required this.email, required this.responseMessage}) : super(key: key);

  @override
  State<SignInConfirmPage> createState() => _SignInConfirmPageState();
}

class _SignInConfirmPageState extends State<SignInConfirmPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  Widget _action() {
    return AppMagicButton<String>(
      text: "Log In",
      postUrl: "/account/confirm",
      getData: () {
        return RequestBodyBuilder()
            .addField("email", widget.email)
            .addField("code", usernameController.text)
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) async{
        AppStorage.getInstance().setUserToken(response['data']['token']);
        AppStorage.getInstance().setUser(UserModel.fromJson(response['data']['user']));
        AppStorage.getInstance().setUserLogin();
        await Future.delayed(const Duration(milliseconds: 400));
        CoAndChallengerModel challenger = CoAndChallengerModel.fromJson(response["data"]["user"]);
        if (!challenger.challenger!) {
          Get.offAll(() => PersonalProfilePage(coAndChallengerModel: challenger, fromRegister: true,));
        } else {
          Get.offAll(() => MainPage());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Stack(
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const LoginActionBar(),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(
                height: 20,
              ),
              AppText.h1("Verify your email"),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 330,
                child: AppText.body(
                  widget.responseMessage,
                  textAlign: TextAlign.center,
                ),
              ),
              const Expanded(child: SizedBox())
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SingleChildScrollView(
              child: SizedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                          color: AppColors.secondary,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          // Container(
                          //     padding: const EdgeInsets.all(2),
                          //     decoration: BoxDecoration(
                          //         color: AppColors.inputFill,
                          //         borderRadius: BorderRadius.circular(24)),
                          //     child: Row(
                          //       mainAxisSize: MainAxisSize.min,
                          //       children: [
                          //         AppButton.flat(
                          //             text: "Face ID",
                          //             width: 109,
                          //             onPressed: () {}),
                          //         AppButton(
                          //             text: "Login with username",
                          //             fontSize: 12,
                          //             width: 165,
                          //             onPressed: () {}),
                          //       ],
                          //     )),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 30),
                            width: 330,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AppTextFieldWithIcon(
                                  hintText: "Code",
                                  icon: "ic_padlock.png",
                                  textEditingController: usernameController,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                // AppTextFieldWithIcon(
                                //   hintText: "Password",
                                //   icon: "ic_padlock.png",
                                //   textEditingController: passwordController,
                                //   obscureText: true,
                                // ),
                                // const SizedBox(
                                //   height: 20,
                                // ),
                                _action(),
                                const SizedBox(
                                  height: 10,
                                ),
                                // AppButton.flat(
                                //     text: "Forget Password?",
                                //     onPressed: () {
                                //       Get.to(ForgetPasswordPage());
                                //     })
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
