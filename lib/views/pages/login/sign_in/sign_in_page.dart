import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/services/app_url_launcher.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/login/forget_password/forget_password_page.dart';
import 'package:challenger/views/pages/login/sign_in/sign_in_confirm_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field_with_icon.dart';
import 'package:challenger/views/widgets/common/login_action_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  var privacyCheck = false;
  var termAndConditionsCheck = false;
  Widget _action() {
    return AppMagicButton<String>(
      text: "Send verification code",
      postUrl: "/account/authenticate",
      beforeProcess: () {
        if (!privacyCheck) {
          AppMessage.showDialogMessage(
              "You should accept the EULA to continue");
          return false;
        }

        if (!termAndConditionsCheck) {
          AppMessage.showDialogMessage(
              "You should accept the terms and conditions to continue");
          return false;
        }
        return true;
      },
      getData: () {
        return RequestBodyBuilder()
            .addField("email", usernameController.text)
            .addField("password", passwordController.text)
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) async{
        // AppStorage.getInstance().setUserToken(response['data']['token']);
        // AppStorage.getInstance().setUser(UserModel.fromJson(response['data']['user']));
        // AppStorage.getInstance().setUserLogin();
        await Future.delayed(const Duration(milliseconds: 400));
        Get.to(() => SignInConfirmPage(email: usernameController.text, responseMessage: response['data']));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Stack(
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const LoginActionBar(),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(
                height: 20,
              ),
              AppText.h1("Log In"),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 330,
                child: AppText.body(
                  "Insert your email address. we will send you a verification code ",
                  textAlign: TextAlign.center,
                ),
              ),
              const Expanded(child: SizedBox())
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SingleChildScrollView(
              child: SizedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                          color: AppColors.secondary,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          // Container(
                          //     padding: const EdgeInsets.all(2),
                          //     decoration: BoxDecoration(
                          //         color: AppColors.inputFill,
                          //         borderRadius: BorderRadius.circular(24)),
                          //     child: Row(
                          //       mainAxisSize: MainAxisSize.min,
                          //       children: [
                          //         AppButton.flat(
                          //             text: "Face ID",
                          //             width: 109,
                          //             onPressed: () {}),
                          //         AppButton(
                          //             text: "Login with username",
                          //             fontSize: 12,
                          //             width: 165,
                          //             onPressed: () {}),
                          //       ],
                          //     )),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 30),
                            width: 330,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AppTextFieldWithIcon(
                                  hintText: "Email",
                                  icon: "ic_email.png",
                                  textEditingController: usernameController,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(children: [

                                SizedBox(
                                  width: 50,
                                  child: CheckboxListTile(
                                    title: AppText.body(
                                      "",
                                      fontSize: 12,
                                    ),
                                    value: privacyCheck,
                                    onChanged: (value) {
                                      setState(() {
                                        privacyCheck = value!;
                                      });
                                    },
                                    controlAffinity: ListTileControlAffinity.leading,
                                  ),
                                ),
                                  InkWell(
                                    onTap: (){
                                      AppUrlLauncher.loadUrl("https://metachallengers.ca/elua");
                                    },
                                    child:
                                    Container(
                                      width: 270,
                                      child: AppText.body("I have read and agree to the End-User License Agreement (EULA)", fontSize: 12,color: Colors.blue,),),),
                                ]),

                                Row(children: [
                                SizedBox(
                                  width: 50,
                                  child: CheckboxListTile(
                                    title: AppText.body(
                                      "",
                                      fontSize: 12,
                                    ),
                                    value: termAndConditionsCheck,
                                    onChanged: (value) {
                                      setState(() {
                                        termAndConditionsCheck = value!;
                                      });
                                    },
                                    controlAffinity: ListTileControlAffinity.leading,
                                  ),
                                ),
                                InkWell(
                                  onTap: (){
                                    AppUrlLauncher.loadUrl("https://metachallengers.ca/terms-and-conditions");
                                  },
                                  child:
                                  Container(
                                    width: 270,
                                    child: AppText.body("I have read and agree to the Terms and Conditions.", fontSize: 12,color: Colors.blue,),),),
                                ]),
                                // AppTextFieldWithIcon(
                                //   hintText: "Password",
                                //   icon: "ic_padlock.png",
                                //   textEditingController: passwordController,
                                //   obscureText: true,
                                // ),
                                // const SizedBox(
                                //   height: 10,
                                // ),
                                // AppButton.flat(text: "Click here to see EULA", onPressed: (){
                                //   AppUrlLauncher.loadUrl("${getBaseUrl()}/eula");
                                // }),
                                // AppButton.flat(text: "Click here to see terms and conditions", onPressed: (){
                                //   AppUrlLauncher.loadUrl("${getBaseUrl()}/terms-and-conditions");
                                // }),
                                const SizedBox(
                                  height: 20,
                                ),
                                _action(),
                                const SizedBox(
                                  height: 10,
                                ),
                                // AppButton.flat(
                                //     text: "Forget Password?",
                                //     onPressed: () {
                                //       Get.to(ForgetPasswordPage());
                                //     })
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
