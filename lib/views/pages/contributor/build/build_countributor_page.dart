import 'dart:io';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BuildContributorPage extends StatefulWidget {
  CoAndChallengerModel coAndChallengerModel;

  BuildContributorPage({Key? key, required this.coAndChallengerModel})
      : super(key: key);

  @override
  State<BuildContributorPage> createState() => _BuildContributorPageState();
}

class _BuildContributorPageState extends State<BuildContributorPage> {
  String? _videoPath;
  String? _imagePath;
  String? _description;

  String selectedSocial = "facebook";

  final summeryController = TextEditingController();
  final facebookController = TextEditingController();
  final youtubeController = TextEditingController();
  final tweeterController = TextEditingController();
  final handController = TextEditingController();
  final instagramController = TextEditingController();
  final linkedinController = TextEditingController();

  Widget _action() {
    return AppButton(
      text: "Save",
      onPressed: () {
        uploadCover();
      },
    );
  }

  int? cover;
  int? introVideo;

  void uploadCover() {
    if (_imagePath == null) {
      uploadIntroVideo();
      return;
    }
    if (cover == null) {
      AppLoadingDialog.show();
      uploadFile("/file/image", _imagePath!,
          appDataListener: AppDataListener(onSuccess: (response) {
            cover = response['data']['id'];
            AppLoadingDialog.hide();
            uploadIntroVideo();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      uploadIntroVideo();
    }
  }

  void uploadIntroVideo() {
    if (_videoPath == null) {
      upload();
      return;
    }
    if (introVideo == null) {
      AppLoadingDialog.show();
      uploadFile("/file/video", _videoPath!,
          appDataListener: AppDataListener(onSuccess: (response) {
            introVideo = response['data']['id'];
            AppLoadingDialog.hide();
            upload();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      upload();
    }
  }

  void upload() async {
    AppLoadingDialog.show();
    HttpService().post("/challenger/update/",
        body: RequestBodyBuilder()
            .addField("videoId", introVideo)
            .addField("videoCoverId", cover)
            .addField("description", summeryController.text)
            .addField("facebook", facebookController.text)
            .addField("tweeter", tweeterController.text)
            .addField("instagram", instagramController.text)
            .addField("clubhouse", handController.text)
            .addField("linkedin", linkedinController.text)
            .addField("youtube", youtubeController.text)
            .build(), onSuccess: (response) {
      AppLoadingDialog.hide();
      AppMessage.showDialogMessage(response['message'], onOkAction: () {
        Get.back();
      });
    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    }, onForbidden: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    });
  }

  @override
  void dispose() {
    summeryController.dispose();
    facebookController.dispose();
    youtubeController.dispose();
    tweeterController.dispose();
    handController.dispose();
    instagramController.dispose();
    linkedinController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    summeryController.text = widget.coAndChallengerModel.description ?? "";
    youtubeController.text = widget.coAndChallengerModel.youtubeUrl ?? "";
    facebookController.text = widget.coAndChallengerModel.facebookUrl ?? "";
    handController.text = widget.coAndChallengerModel.clubhouseUrl ?? "";
    linkedinController.text = widget.coAndChallengerModel.linkedinUrl ?? "";
    instagramController.text = widget.coAndChallengerModel.instagramUrl ?? "";
    tweeterController.text = widget.coAndChallengerModel.tweeterUrl ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
      title:
          "${widget.coAndChallengerModel.firstname ?? ""} ${widget.coAndChallengerModel.lastname ?? ""}",
      body: SingleChildScrollView(
          child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: AppText.body(
                "Please record your intro and select your profile picture ",
                fontSize: 12,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const SizedBox(
              height: 10,
            ),
            AppBorderContainer(
              height: 180,
              width: double.maxFinite,
              borderRadius: 24,
              padding: const EdgeInsets.all(1),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(24),
                child: Container(
                  color: Colors.black,
                  child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                    children: [
                      InkWell(
                        onTap: () {
                          if (_videoPath == null) {
                            Get.to(VideoPlayerPage.playChallengerIntroVideo(
                                widget.coAndChallengerModel));
                          } else {
                            Get.to(VideoPlayerPage.playFromFile(_videoPath!));
                          }
                        },
                        child: const Icon(
                          Icons.play_circle,
                          color: Colors.white60,
                          size: 40,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      AppButton(
                          width: 130,
                          fontSize: 12,
                          height: 30,
                          text: "Upload new video",
                          onPressed: () {
                            AppMediaPicker()
                                .pickVideoFromGallery((pickedFile, thumbnail) {
                              setState(() {
                                _videoPath = pickedFile;
                              });
                            });
                          }),
                    ],
                  )),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            AppBorderContainer(
              height: 175,
              width: 175,
              borderRadius: 250,
              padding: const EdgeInsets.all(1),
              child: InkWell(
                onTap: () {
                  AppMediaPicker().pickImageFromGallery((pickedFile) {
                    setState(() {
                      _imagePath = pickedFile;
                    });
                  });
                },
                child: _imagePath == null
                    ? CircleAvatar(
                        backgroundImage: NetworkImage(
                            widget.coAndChallengerModel.introVideoCoverUrl!),
                      )
                    : CircleAvatar(
                        backgroundImage: FileImage(File(_imagePath!)),
                      ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            AppText(
              "Select your intro video cover",
              color: const Color(0xff484848),
              fontSize: 11,
              fontWeight: FontWeight.w400,
            ),
            const SizedBox(
              height: 15,
            ),
            const Divider(
              height: 2,
              color: AppColors.borderColor,
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                AppText(
                  "Your Social Media",
                  fontSize: 12,
                ),
                Expanded(child: Container()),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "facebook";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem(
                        "ic_facebook.png", selectedSocial == "facebook"),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "youtube";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem(
                        "ic_youtube.png", selectedSocial == "youtube"),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "tweeter";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem(
                        "ic_tweeter.png", selectedSocial == "tweeter"),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "hand";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem("ic_hand.png", selectedSocial == "hand"),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "instagram";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem(
                        "ic_instagram.png", selectedSocial == "instagram"),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedSocial = "linkedin";
                    });
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: _socialItem(
                        "ic_linkedin.png", selectedSocial == "linkedin"),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            if (selectedSocial == "facebook")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: facebookController,
              ),
            if (selectedSocial == "youtube")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: youtubeController,
              ),
            if (selectedSocial == "tweeter")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: tweeterController,
              ),
            if (selectedSocial == "hand")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: handController,
              ),
            if (selectedSocial == "instagram")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: instagramController,
              ),
            if (selectedSocial == "linkedin")
              AppTextField(
                hintText: "Enter the link ...",
                textEditingController: linkedinController,
              ),
            const SizedBox(
              height: 15,
            ),
            AppTextField(
              hintText: "Write a summery about yourself ...",
              borderRadius: 24,
              textEditingController: summeryController,
              keyboardType: TextInputType.multiline,
              height: 104,
            ),
            const SizedBox(
              height: 15,
            ),
            _action(),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      )),
    );
  }

  Widget _socialItem(String image, bool isOriginal) {
    if (isOriginal) {
      return AppImage(image);
    }
    return ColorFiltered(
      colorFilter: const ColorFilter.matrix(<double>[
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
      ]),
      child: AppImage(image),
    );
  }
}
