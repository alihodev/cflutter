import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class FollowingsPage extends StatefulWidget {
  int id;
  FollowingsPage({Key? key, required this.id}) : super(key: key);

  @override
  State<FollowingsPage> createState() => _FollowingsPageState();
}

class _FollowingsPageState extends State<FollowingsPage> {

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(title: "Followings", body: Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
            child: AppPaginatedListView<CoAndChallengerModel>(
              url: "/challenger/${widget.id}/followings",
              onFetchData: (response) {
                return response
                    .map<CoAndChallengerModel>((e) => CoAndChallengerModel.fromJson(e))
                    .toList();
              },
              renderItem: (item) => CoAndChallengerItem(userModel: item),
            )),
      ],
    ));
  }
}
