import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/pages/contributor/build/build_countributor_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:get/get.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class CoChallengersPage extends StatefulWidget {
  int id;
  bool coChallenge = false;
  CoChallengersPage({Key? key, required this.id}) : super(key: key);

  CoChallengersPage.challenge({required this.id}) {
    coChallenge = true;
  }

  @override
  State<CoChallengersPage> createState() => _CoChallengersPageState();
}

class _CoChallengersPageState extends State<CoChallengersPage> {

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(title: "Co Challengers", body: Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
            child: AppPaginatedListView<CoAndChallengerModel>(
              url: "/${widget.coChallenge ? "challenge" : "challenger"}/${widget.id}/co-challengers",
              onFetchData: (response) {
                return response
                    .map<CoAndChallengerModel>((e) => CoAndChallengerModel.fromJson(e))
                    .toList();
              },
              renderItem: (item) => CoAndChallengerItem(userModel: item),
            )),
      ],
    ));
  }
}
