import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/pages/contributor/build/build_countributor_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:get/get.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class FollowersPage extends StatefulWidget {
  int id;
  bool coChallenge = false;
  FollowersPage({Key? key, required this.id}) : super(key: key);
  FollowersPage.challenge({required this.id}){
    coChallenge = true;
  }
  @override
  State<FollowersPage> createState() => _FollowersPageState();
}

class _FollowersPageState extends State<FollowersPage> {

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(title: "Followers", body: Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
            child: AppPaginatedListView<CoAndChallengerModel>(
              url: "/${widget.coChallenge ? "challenge" : "challenger"}/${widget.id}/followers",
              onFetchData: (response) {
                return response
                    .map<CoAndChallengerModel>((e) => CoAndChallengerModel.fromJson(e))
                    .toList();
              },
              renderItem: (item) => CoAndChallengerItem(userModel: item),
            )),
      ],
    ));
  }
}
