import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/services/app_url_launcher.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/chat/chat_page.dart';
import 'package:challenger/views/pages/contributor/build/build_countributor_page.dart';
import 'package:challenger/views/pages/contributor/co_challengers/co_challengers_page.dart';
import 'package:challenger/views/pages/contributor/followers/followers_page.dart';
import 'package:challenger/views/pages/contributor/followings/followings_page.dart';
import 'package:challenger/views/pages/explorer/views/challenges/challenge_item.dart';
import 'package:challenger/views/pages/profile/personal_profile_page.dart';
import 'package:challenger/views/pages/setting/setting_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_follow_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_widget.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  bool isCurrentUser;
  int id;

  ProfilePage({Key? key, this.isCurrentUser = false, required this.id})
      : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool _showLikeWidget = false;
  int check = 0;

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        body: AppMagicFutureBuilder(
      url: "/challenger/${widget.id}?${check}",
      onSuccess: (response) {
        CoAndChallengerModel model = CoAndChallengerModel.fromJson(response);
        return Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  if (widget.isCurrentUser)
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: 40,
                            child: PopupMenuButton<int>(
                              icon: const Icon(Icons.more_vert_rounded),
                              itemBuilder: (context) => [
                                const PopupMenuItem(
                                  value: 1,
                                  child: Text('Setting'),
                                ),
                              ],
                              onSelected: (value) {
                                if (value == 1) {
                                  Get.to(SettingPage());
                                }
                              },
                            ),
                          ),
                          const Spacer(),
                          AppImage(
                            "logo.png",
                            height: 40,
                          ),
                          const Spacer(),
                          const SizedBox(
                            width: 50,
                          ),
                        ],
                      ),
                    ),
                  if (!widget.isCurrentUser)
                    Container(
                      height: 65,
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      margin: const EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                        color: AppColors.actionBarColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 8,
                            offset: const Offset(0, 4),
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              width: 50,
                              height: 50,
                              padding: const EdgeInsets.all(15),
                              child: AppImage(
                                "ic_arrow_left.png",
                                color: AppColors.h1TextColor,
                              ),
                            ),
                          ),
                          const Spacer(),
                          AppImage(
                            "logo.png",
                            height: 40,
                          ),
                          const Spacer(),
                          SizedBox(
                            width: 40,
                            child: PopupMenuButton<int>(
                              icon: const Icon(Icons.more_vert_rounded),
                              itemBuilder: (context) => [
                                const PopupMenuItem(
                                  value: 1,
                                  child: Text('Report this user'),
                                ),
                                const PopupMenuItem(
                                  value: 2,
                                  child: Text('Block this user', style: TextStyle(color: Colors.red),),
                                ),
                              ],
                              onSelected: (value) {
                                if (value == 1) {
                                  AppDialog.showReportDialog(
                                      title: "Report User",
                                      description:
                                      "Why do you want to report this user?",
                                      onAction: (String description) {
                                        AppLoadingDialog.show();
                                        UserRepository().reportUser(
                                            widget.id, description,
                                            AppDataListener(onSuccess: (message) {
                                              AppLoadingDialog.hide();
                                              AppMessage.showDialogMessage(
                                                  message);
                                            }, onFailure: (message) {
                                              AppLoadingDialog.hide();
                                              AppMessage.showDialogMessage(
                                                  message);
                                            }));
                                      });
                                } else if (value == 2) {
                                  AppDialog.showAlertDialog(
                                      title: "Block User",
                                      description:
                                      "Are you sure to block this user?",
                                      onAction: () {
                                        AppLoadingDialog.show();
                                        UserRepository().blockUser(
                                            widget.id,
                                            AppDataListener(onSuccess: (message) {
                                              AppLoadingDialog.hide();
                                              AppMessage.showDialogMessage(
                                                  message);
                                            }, onFailure: (message) {
                                              AppLoadingDialog.hide();
                                              AppMessage.showDialogMessage(
                                                  message);
                                            }));
                                      });
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        SizedBox(
                          width: 120,
                          height: 120,
                          child: Stack(
                            children: [
                              CircleAvatar(
                                radius: 60,
                                backgroundImage:
                                    NetworkImage(model.profileUrl!),
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: widget.isCurrentUser
                                    ? InkWell(
                                        onTap: () async {
                                          await Get.to(PersonalProfilePage(
                                              coAndChallengerModel: model));
                                          setState(() {
                                            check += 1;
                                          });
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.all(6),
                                          width: 28,
                                          height: 28,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: AppColors.primary),
                                          child: AppImage("ic_edit.png"),
                                        ),
                                      )
                                    : FollowWidget(coAndChallengerModel: model),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        AppText.h1(
                          "${model.firstname ?? "ٔNot Set"} ${model.lastname ?? ""}",
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),
                        AppText.body(
                          "@${model.username}",
                          fontWeight: FontWeight.w400,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.to(FollowingsPage(id: model.id!));
                              },
                              child: Column(
                                children: [
                                  AppText(
                                    "${model.followings ?? 0}",
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                  ),
                                  AppText(
                                    "Following",
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(CoChallengersPage(id: model.id!));
                              },
                              child: Column(
                                children: [
                                  AppText(
                                    "${model.coChallengers ?? 0}",
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                  ),
                                  AppText("Co Challengers",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(FollowersPage(id: model.id!));
                              },
                              child: Column(
                                children: [
                                  AppText(
                                    "${model.followers ?? 0}",
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                  ),
                                  AppText("Followers",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                ],
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          color: AppColors.borderColor,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      _socialIcon(
                          icon: "ic_instagram.png",
                          url: model.instagramUrl ?? "",
                          isOriginal: model.instagramUrl?.isNotEmpty ?? false),
                      _socialIcon(
                          icon: "ic_linkedin.png",
                          url: model.linkedinUrl ?? "",
                          isOriginal: model.linkedinUrl?.isNotEmpty ?? false),
                      _socialIcon(
                          icon: "ic_facebook.png",
                          url: model.facebookUrl ?? "",
                          isOriginal: model.facebookUrl?.isNotEmpty ?? false),
                      _socialIcon(
                          icon: "ic_tweeter.png",
                          url: model.tweeterUrl ?? "",
                          isOriginal: model.tweeterUrl?.isNotEmpty ?? false),
                      _socialIcon(
                          icon: "ic_youtube.png",
                          url: model.youtubeUrl ?? "",
                          isOriginal: model.youtubeUrl?.isNotEmpty ?? false),
                      _socialIcon(
                          icon: "ic_hand.png",
                          url: model.clubhouseUrl ?? "",
                          isOriginal: model.clubhouseUrl?.isNotEmpty ?? false),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Stack(
                    children: [
                      InkWell(
                        onTap: () {
                          Get.to(
                              VideoPlayerPage.playChallengerIntroVideo(model));
                        },
                        child: Image.network(
                          model.introVideoCoverUrl!,
                          height: 229,
                          width: double.maxFinite,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      _actionItem(
                          icon: "ic_send.png", text: "Send", onClick: () {}),
                      _actionItem(
                          icon: "ic_message.png",
                          text: "Message",
                          onClick: () {
                            Get.to(ChatPage(
                              coAndChallengerModel: model,
                            ));
                          }),
                      AppLikeToggle.challenger(model),
                      // _actionItem(
                      //     icon: "ic_favorite_empty.png",
                      //     text: "Like",
                      //     onClick: () {
                      //       setState(() {
                      //         _showLikeWidget = true;
                      //       });
                      //     }),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Divider(
                    color: Colors.black45,
                    height: 1,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: AppText.h1(
                            "Main Challenges",
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppMagicFutureBuilder(
                            url: "/challenger/${widget.id}/challenges",
                            onSuccess: (response) {
                              List<ChallengeItem> challenges = [];
                              for (var item in response) {
                                challenges.add(ChallengeItem(
                                  challengeModel: ChallengeModel.fromJson(item),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                ));
                              }
                              if (challenges.isEmpty) {
                                return AppText("No challenges found!");
                              }
                              return Column(
                                children: [...challenges],
                              );
                            }),
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          color: Colors.black45,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: AppText.h1(
                            "Co Challenges",
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppMagicFutureBuilder(
                            url: "/challenger/${widget.id}/co-challenges",
                            onSuccess: (response) {
                              List<ChallengeItem> challenges = [];
                              for (var item in response) {
                                challenges.add(ChallengeItem(
                                  challengeModel: ChallengeModel.fromJson(item),
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5),
                                ));
                              }
                              if (challenges.isEmpty) {
                                return AppText("No challenges found!");
                              }
                              return Column(
                                children: [...challenges],
                              );
                            }),
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          color: AppColors.borderColor,
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: AppText(model.description ?? ""),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            AppLikeWidget(
              show: _showLikeWidget,
              onHide: () {
                setState(() {
                  _showLikeWidget = false;
                });
              },
            ),
          ],
        );
      },
    ));
  }

  Widget _actionItem(
      {required String icon, required String text, required Function onClick}) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Column(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: AppImage(icon),
          ),
          const SizedBox(
            height: 4,
          ),
          AppText(
            text,
            fontSize: 9,
          ),
        ],
      ),
    );
  }

  Widget _socialIcon(
      {required String icon, required String url, required bool isOriginal}) {
    return InkWell(
      onTap: () {
        if (isOriginal) {
          AppUrlLauncher.loadUrl(url);
        }
      },
      child: SizedBox(
        width: 24,
        height: 24,
        child: _socialItem(icon, isOriginal),
      ),
    );
  }

  Widget _socialItem(String image, bool isOriginal) {
    if (isOriginal) {
      return AppImage(image);
    }
    return ColorFiltered(
      colorFilter: const ColorFilter.matrix(<double>[
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
      ]),
      child: AppImage(image),
    );
  }
}

class FollowWidget extends StatefulWidget {
  CoAndChallengerModel coAndChallengerModel;

  FollowWidget({Key? key, required this.coAndChallengerModel})
      : super(key: key);

  @override
  State<FollowWidget> createState() => _FollowWidgetState();
}

class _FollowWidgetState extends State<FollowWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.coAndChallengerModel.follow =
            !(widget.coAndChallengerModel.follow ?? false);
        setState(() {});
        UserRepository().challengerFollowToggle(
            widget.coAndChallengerModel.id ?? 0,
            AppDataListener(onSuccess: (response) {}, onFailure: (message) {}));
      },
      child: Container(
        padding: const EdgeInsets.all(6),
        width: 28,
        height: 28,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: AppColors.primary),
        child: AppImage(
          (widget.coAndChallengerModel.follow ?? false)
              ? "ic_follow_1.png"
              : "ic_unfollow_1.png",
          color: Colors.white,
        ),
      ),
    );
  }
}
