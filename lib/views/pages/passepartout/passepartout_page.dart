import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/pages/clubs/views/debates/debates_view.dart';
import 'package:challenger/views/pages/clubs/views/rooms/rooms_view.dart';
import 'package:challenger/views/pages/passepartout/views/chat/chat_view.dart';
import 'package:challenger/views/pages/passepartout/views/media/media_view.dart';
import 'package:challenger/views/pages/passepartout/views/shared/shared_view.dart';
import 'package:challenger/views/pages/passepartout/views/stared/stared_view.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PassepartoutPage extends StatefulWidget {
  int initialTabIndex;

  PassepartoutPage({Key? key, required this.initialTabIndex}) : super(key: key);

  @override
  State<PassepartoutPage> createState() => _PassepartoutPageState();
}

class _PassepartoutPageState extends State<PassepartoutPage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: 1, vsync: this, initialIndex: widget.initialTabIndex);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: DefaultTabController(
        length: 4,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.actionBarColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 8,
                    offset: const Offset(0, 4),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        left: 7, right: 7, top: 10, bottom: 10),
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Builder(builder: (BuildContext context) {
                          return InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              width: 50,
                              height: 50,
                              child: AppImage(
                                "ic_arrow_left.png",
                                color: AppColors.h1TextColor,
                              ),
                            ),
                          );
                        }),
                        Column(
                          children: [
                            AppImage(
                              "ic_passepartout.png",
                              height: 20,
                            ),
                            FittedBox(
                              fit: BoxFit.fill,
                              child: AppText("Passepartout", fontSize: 11, fontWeight: FontWeight.bold,),
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          width: 50,
                          height: 50,
                        ),
                      ],
                    ),
                  ),
                  // PreferredSize(
                  //   preferredSize: const Size.fromHeight(kToolbarHeight),
                  //   child: TabBar(
                  //     controller: _tabController,
                  //     labelColor: AppColors.primary,
                  //     labelStyle: const TextStyle(
                  //       fontSize: 16,
                  //     ),
                  //     unselectedLabelColor: AppColors.unSelectedTabColor,
                  //     indicatorColor: AppColors.primary,
                  //     indicatorSize: TabBarIndicatorSize.label,
                  //     tabs: const [
                  //       Tab(text: 'Chat'),
                  //       Tab(text: 'Stared'),
                  //       Tab(text: 'Media'),
                  //       Tab(text: 'Shared'),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: const [
                  ChatView(),
                  // StaredView(),
                  // MediaView(),
                  // SharedView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
