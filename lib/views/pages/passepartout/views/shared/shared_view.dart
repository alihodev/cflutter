import 'package:challenger/models/passepartout_chat_model.dart';
import 'package:challenger/views/pages/passepartout/views/chat/chat_item.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:flutter/material.dart';

class SharedView extends StatefulWidget {
  const SharedView({Key? key}) : super(key: key);

  @override
  State<SharedView> createState() => _SharedViewState();
}

class _SharedViewState extends State<SharedView> {
  List<PassepartoutChatModel> chats = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child: AppPaginatedListView<PassepartoutChatModel>(
            url: "/google.com",
            isFake: true,
            onFetchData: (response) {
              List<PassepartoutChatModel> list = [];
              for(var i = 0; i<4 ; i++) {
                PassepartoutChatModel item = PassepartoutChatModel();
                item.isPassepartout = true;
                list.add(item);
              }
              return list;
            },
            renderItem: (item) => ChatItem(chatModel: item),
          ),
        ),
      ],
    );
  }
}
