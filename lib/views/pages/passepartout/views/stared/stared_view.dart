import 'package:challenger/models/passepartout_chat_model.dart';
import 'package:challenger/views/pages/passepartout/views/chat/chat_item.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:flutter/material.dart';

class StaredView extends StatefulWidget {
  const StaredView({Key? key}) : super(key: key);

  @override
  State<StaredView> createState() => _StaredViewState();
}

class _StaredViewState extends State<StaredView> {
  List<PassepartoutChatModel> chats = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child: AppPaginatedListView<PassepartoutChatModel>(
            url: "/google.com",
            isFake: true,
            onFetchData: (response) {
              List<PassepartoutChatModel> list = [];
              for(var i = 0; i<3 ; i++) {
                PassepartoutChatModel item = PassepartoutChatModel();
                item.isPassepartout = true;
                item.stared = true;
                list.add(item);
              }
              return list;
            },
            renderItem: (item) => ChatItem(chatModel: item),
          ),
        ),
      ],
    );
  }
}
