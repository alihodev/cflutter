import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/all_explorer_model.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/passepartout_chat_model.dart';
import 'package:challenger/models/room_model.dart';
import 'package:challenger/views/pages/clubs/views/rooms/room_item.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/pages/reels/reels_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MediaView extends StatefulWidget {
  const MediaView({Key? key}) : super(key: key);

  @override
  State<MediaView> createState() => _MediaViewState();
}

class _MediaViewState extends State<MediaView> {
  List<AllExplorerModel> reels = [];

  void updateView() async {
    await Future.delayed(const Duration(milliseconds: 100));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            AppMagicFutureBuilder(
                url: "/challenge/reels?limit=39",
                onSuccess: (response) {
                  if (reels.isEmpty) {
                    reels = response.map<AllExplorerModel>((e) {
                      AllExplorerModel all = AllExplorerModel();
                      if (e['username'] != null) {
                        all.coAndChallengerModel = CoAndChallengerModel.fromJson(e);
                        all.type = 1;
                      } else if (e['title'] != null) {
                        all.challengeModel = ChallengeModel.fromJson(e);
                        all.type = 0;
                      }
                      return all;
                    }).toList();

                    updateView();
                  }
                  return Container();
                }),
            Expanded(
              child:
              GridView.count(
                mainAxisSpacing: 5,
                crossAxisCount: 3,
                crossAxisSpacing: 5,
                childAspectRatio: 1,
                children: List.generate(reels.length, (index) {
                  return InkWell(onTap: (){
                    Get.to(ReelsPage(selectedModelIndex:  index, allExplorerModel: reels,));
                  }, child: Center(
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.network(
                      reels[index].type == 0 ? reels[index].challengeModel.coverUrl ?? "" : reels[index].coAndChallengerModel.introVideoCoverUrl ?? "",
                      height: double.maxFinite,
                      width: double.maxFinite,
                      fit: BoxFit.cover,
                    ),)),
                  );
                }),
              )
            ),
          ],
        ),
      ],
    );
  }
}
