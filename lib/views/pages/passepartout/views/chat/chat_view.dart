import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/passpartout_reporitory.dart';
import 'package:challenger/models/Message.dart';
import 'package:challenger/models/passepartout_chat_model.dart';
import 'package:challenger/services/amplify_message_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/passepartout/views/chat/chat_item.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChatView extends StatefulWidget {
  const ChatView({Key? key}) : super(key: key);

  @override
  State<ChatView> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  final textController = TextEditingController();
  List<PassepartoutChatModel> chats = [];
  bool isDone = false;
  bool isTyping = false;
  void updateView () async {
    if (isDone) {
      return;
    }
    isDone = true;
    _getMessages();
    await Future.delayed(const Duration(milliseconds: 100));
    setState(() {

    });
  }

  void _getMessages() async {
    print('start get messages');
    await Future.delayed(Duration(seconds: 10));
    PasspartoutRepository().getMessages(
        AppDataListener(onSuccess: (response) {

          _getMessages();
        }, onFailure: (message) {
          _getMessages();
        }));
    setState(() {
      isTyping = false;
    });
  }

  StreamSubscription<QuerySnapshot<Message>>? _stream;
  late String chatId;
  @override
  void initState() {
    super.initState();
    chatId = "passepartout_${AppStorage.getInstance().getStaticUser().id}";
    observeQuery();
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Expanded(
                child: ListView.builder(
                  reverse: true,
              itemCount: chats.length,
              itemBuilder: (context, index) {
                final reversedIndex = chats.length - 1 - index;
                final item = chats[reversedIndex];
                return ChatItem(chatModel: item);
              },
            )),
            if (isTyping)
            Row(children: [
              const SizedBox(width: 10,),
              AppText("Passepartout is typing ...", fontSize: 12,),
              const SizedBox( width: 10,),
              ]),
            const SizedBox(height: 5,),
            const Divider(
              height: 2,
              color: Color(0xff323F4B),
            ),
            Row(
              children: [
                Expanded(
                  child: AppTextField(
                    borderRadius: 0,
                    textEditingController: textController,
                    borderColor: Colors.transparent,
                    hintText: "Type something ...",
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                InkWell(
                    onTap: () {
                      if (textController.text.isNotEmpty) {
                        createMessage(chatId: chatId, message: textController.text, senderId: AppStorage.getInstance().getStaticUser().id.toString(), receiverId: "passepartout");
                        PasspartoutRepository().sendMessage(
                            textController.text,
                            AppDataListener(
                                onSuccess: (response) {
                                  createMessage(chatId: chatId, message: response, senderId: "passepartout", receiverId: AppStorage.getInstance().getStaticUser().id.toString());
                                  setState(() {
                                    isTyping = false;
                                  });
                                },
                                onFailure: (message) {
                                  setState(() {
                                    isTyping = false;
                                  });
                                }));

                        setState(() {
                          isTyping = true;
                          textController.text = "";
                        });
                        // PassepartoutChatModel model = PassepartoutChatModel();
                        // model.message = textController.text;
                        // model.isPassepartout = false;
                        // setState(() {
                        //   textController.text = "";
                        //   chats.add(model);
                        // });
                      }
                    },
                    child: const SizedBox(
                      width: 50,
                      height: 40,
                      child: Icon(Icons.send),
                    ))
              ],
            )
          ],
        ),
      ],
    );
  }

  void observeQuery() {
    _stream = Amplify.DataStore.observeQuery(
        Message.classType,
        where: Message.CHATID.eq(chatId)
        ,
        sortBy: [Message.CREATEDAT.ascending()]
    ).listen((QuerySnapshot<Message> snapshot) {
      chats.clear();
      for (var i = 0; i < snapshot.items.length; i++) {
        PassepartoutChatModel chatModel = PassepartoutChatModel();
        chatModel.message = snapshot.items[i].message ?? "";
        chatModel.isPassepartout = snapshot.items[i].senderId != AppStorage.getInstance().getStaticUser().id.toString();
        chatModel.date = snapshot.items[i].createdAt.toString();
        chats.add(chatModel);
      }
      // chats = chats.reversed.toList();
      setState(() {

      });
    });
  }

  @override
  void dispose() {
    if (_stream != null) {
      _stream!.cancel();
    }
    super.dispose();
    textController.dispose();
  }
}
