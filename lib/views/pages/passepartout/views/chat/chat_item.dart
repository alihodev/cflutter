import 'package:challenger/models/passepartout_chat_model.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get_time_ago/get_time_ago.dart';

class ChatItem extends StatefulWidget {
  PassepartoutChatModel chatModel;

  ChatItem({Key? key, required this.chatModel}) : super(key: key);

  @override
  State<ChatItem> createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    if (widget.chatModel.isPassepartout) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(10),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: const Color(0xffd9d9d9),
                      borderRadius: BorderRadius.circular(50)),
                  child: AppImage("ic_bot.png"),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(height: 5,),
                    SizedBox(
                      width: MediaQuery.of(context).size.width - 100,
                      child: AppText.body(
                          widget.chatModel.message , color: const Color(0xff101828),),
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        AppText.body(
                          GetTimeAgo.parse(DateTime.parse(widget.chatModel.date)),
                          fontSize: 12,
                          color: const Color(0xff667085),
                        ),
                        const SizedBox( width: 5,),
                        // InkWell(onTap: (){},child: Icon(widget.chatModel.stared ? Icons.star_rounded: Icons.star_border_rounded),)
                      ],
                    )
                  ],
                )
              ],
            )
          ],
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      margin: const EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          const SizedBox(width: 80,),
          Expanded(child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 5,),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: const Offset(0, 1),
                      ),
                    ],
                    color: const Color(0xffF2F4F7), borderRadius: const BorderRadius.only(topLeft: Radius.circular(8), bottomLeft:Radius.circular(8),bottomRight: Radius.circular(8) )),
                child: AppText.body(
                  widget.chatModel.message, color: const Color(0xff101828),),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AppText.body(
                    GetTimeAgo.parse(DateTime.parse(widget.chatModel.date)),
                    fontSize: 12,
                    color: const Color(0xff667085),
                  ),
                ],
              )
            ],
          ),),
        ],
      ),
    );
  }
}
