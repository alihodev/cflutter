import 'dart:io';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/data/repository/file_reporitory.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PersonalProfilePage extends StatefulWidget {
  CoAndChallengerModel coAndChallengerModel;
  bool fromRegister;
  PersonalProfilePage({Key? key, required this.coAndChallengerModel, this.fromRegister = false})
      : super(key: key);

  @override
  State<PersonalProfilePage> createState() => _PersonalProfilePageState();
}

class _PersonalProfilePageState extends State<PersonalProfilePage> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final usernameController = TextEditingController();
  final nikNameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneNumberController = TextEditingController();
  String? _profileUrl;
  int? uploadedImageId;

  Widget _action() {
    return AppButton(
      text: "Save Changes",
      width: 142,
      fontSize: 14,
      borderRadius: 8,
      onPressed: () {
        uploadProfile();
      },
    );
  }

  void uploadProfile() {
    if (_profileUrl!.startsWith("http")) {
      updateProfile();
    } else {
      AppLoadingDialog.show();
      FileRepository().uploadImage(
          _profileUrl!,
          AppDataListener(onSuccess: (response) {
            AppLoadingDialog.hide();
            uploadedImageId = response['id'];
            updateProfile();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
              uploadProfile();
            });
          }));
    }
  }

  void updateProfile() {
    AppLoadingDialog.show();
    UserRepository().updateProfile(
        RequestBodyBuilder()
            .addField("username", usernameController.text)
            .addField("firstname", firstNameController.text)
            .addField("lastname", lastNameController.text)
            .addField("email", emailController.text)
            .addField("phone", phoneNumberController.text)
            .addField("profileId", uploadedImageId)
            .build(),
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppStorage.getInstance().setUser(UserModel.fromJson(response));

          uploadCover();
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            updateProfile();
          });
        }));
  }

  String? _videoPath;
  String? _imagePath;
  String? _description;

  String selectedSocial = "facebook";

  final summeryController = TextEditingController();
  final facebookController = TextEditingController();
  final youtubeController = TextEditingController();
  final tweeterController = TextEditingController();
  final handController = TextEditingController();
  final instagramController = TextEditingController();
  final linkedinController = TextEditingController();

  int? cover;
  int? introVideo;

  void uploadCover() {
    if (_imagePath == null) {
      uploadIntroVideo();
      return;
    }
    if (cover == null) {
      AppLoadingDialog.show();
      FileRepository().uploadImage( _imagePath!,
          AppDataListener(onSuccess: (response) {
            cover = response['id'];
            AppLoadingDialog.hide();
            uploadIntroVideo();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      uploadIntroVideo();
    }
  }

  void uploadIntroVideo() {
    if (_videoPath == null) {
      upload();
      return;
    }
    if (introVideo == null) {
      AppLoadingDialog.show();
      FileRepository().uploadVideo(
          _videoPath!,
          AppDataListener(onSuccess: (response) {
            introVideo = response['id'];
            AppLoadingDialog.hide();
            upload();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      upload();
    }
  }

  void upload() async {
    AppLoadingDialog.show();
    HttpService().post("/challenger/update/",
        body: RequestBodyBuilder()
            .addField("videoId", introVideo)
            .addField("videoCoverId", cover)
            .addField("description", summeryController.text)
            .addField("facebook", facebookController.text)
            .addField("tweeter", tweeterController.text)
            .addField("instagram", instagramController.text)
            .addField("clubhouse", handController.text)
            .addField("linkedin", linkedinController.text)
            .addField("youtube", youtubeController.text)
            .build(), onSuccess: (response) {
      AppLoadingDialog.hide();
      AppMessage.showDialogMessage(response['message'], onOkAction: () {
        if (widget.fromRegister) {
          Get.offAll(MainPage());
        } else {
          Get.back();
        }
      });
    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    }, onForbidden: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    usernameController.dispose();
    nikNameController.dispose();
    emailController.dispose();
    phoneNumberController.dispose();

    summeryController.dispose();
    facebookController.dispose();
    youtubeController.dispose();
    tweeterController.dispose();
    handController.dispose();
    instagramController.dispose();
    linkedinController.dispose();
  }

  @override
  void initState() {
    super.initState();
    getData();
    summeryController.text = widget.coAndChallengerModel.description ?? "";
    youtubeController.text = widget.coAndChallengerModel.youtubeUrl ?? "";
    facebookController.text = widget.coAndChallengerModel.facebookUrl ?? "";
    handController.text = widget.coAndChallengerModel.clubhouseUrl ?? "";
    linkedinController.text = widget.coAndChallengerModel.linkedinUrl ?? "";
    instagramController.text = widget.coAndChallengerModel.instagramUrl ?? "";
    tweeterController.text = widget.coAndChallengerModel.tweeterUrl ?? "";
  }

  void getData() async {
    await Future.delayed(Duration(milliseconds: 100));
    AppLoadingDialog.show();
    UserRepository()
        .getCurrentChallenger(AppDataListener(onSuccess: (response) {
      AppLoadingDialog.hide();
      emailController.text = response['email'];
      usernameController.text = response['username'];
      firstNameController.text = response['firstname'] ?? "";
      lastNameController.text = response['lastname'] ?? "";
      phoneNumberController.text = response['mobile'] ?? "";
      _profileUrl = response["profileUrl"];
      setState(() {});
    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: widget.fromRegister ?  "Update Your Profile" : "Profile",
        body: _profileUrl == null
            ? Container()
            : Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      AppText(
                        "Your photo",
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: const Color(0xff101828),
                      ),
                      AppText.body(
                        "This will be displayed on your profile.",
                        fontSize: 14,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Column(
                          children: [
                            _profileUrl!.startsWith("http")
                                ? CircleAvatar(
                                    radius: 45,
                                    backgroundImage: NetworkImage(
                                      _profileUrl!,
                                    ))
                                : CircleAvatar(
                                    radius: 45,
                                    backgroundImage:
                                        FileImage(File(_profileUrl!))),
                            AppButton.flat(
                                text: "Update profile",
                                width: 170,
                                fontSize: 14,
                                onPressed: () {
                                  AppMediaPicker()
                                      .pickImageFromGallery((pickedFilePath) {
                                    setState(() {
                                      _profileUrl = pickedFilePath;
                                    });
                                  });
                                })
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      AppText(
                        "Personal info",
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: const Color(0xff101828),
                      ),
                      AppText.body(
                        "Update your personal details.",
                        fontSize: 14,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            child: AppTextField.round(
                              label: "First name",
                              textEditingController: firstNameController,
                            ),
                          )),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Container(
                              child: AppTextField.round(
                                width: 200,
                                label: "Last name",
                                textEditingController: lastNameController,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      AppTextField.round(
                        label: "Username",
                        denySpace: true,
                        textEditingController: usernameController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      // AppTextField.round(
                      //   label: "Nik name",
                      //   textEditingController: nikNameController,
                      // ),
                      // const SizedBox(
                      //   height: 10,
                      // ),
                      AppTextField.round(
                        label: "Email",
                        textEditingController: emailController,
                        enabled: false,
                        icon: "ic_email.png",
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      AppTextField.round(
                        label: "Phone Number",
                        textEditingController: phoneNumberController,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: AppText.body(
                          "Please record your intro and select your profile picture ",
                          fontSize: 12,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      AppBorderContainer(
                        height: 180,
                        width: double.maxFinite,
                        borderRadius: 24,
                        padding: const EdgeInsets.all(1),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(24),
                          child: Container(
                            color: Colors.black,
                            child: Center(
                                child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (_videoPath == null) {
                                      Get.to(VideoPlayerPage
                                          .playChallengerIntroVideo(
                                              widget.coAndChallengerModel));
                                    } else {
                                      Get.to(VideoPlayerPage.playFromFile(
                                          _videoPath!));
                                    }
                                  },
                                  child: const Icon(
                                    Icons.play_circle,
                                    color: Colors.white60,
                                    size: 40,
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                AppButton(
                                    width: 130,
                                    fontSize: 12,
                                    height: 30,
                                    text: "Upload new video",
                                    onPressed: () {
                                      AppMediaPicker().pickVideoFromGallery(
                                          (pickedFile, thumbnail) {
                                        setState(() {
                                          _videoPath = pickedFile;
                                        });
                                      });
                                    }),
                              ],
                            )),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: AppBorderContainer(
                          height: 175,
                          width: 175,
                          borderRadius: 250,
                          padding: const EdgeInsets.all(1),
                          child: InkWell(
                            onTap: () {
                              AppMediaPicker()
                                  .pickImageFromGallery((pickedFile) {
                                setState(() {
                                  _imagePath = pickedFile;
                                });
                              });
                            },
                            child: _imagePath == null
                                ? CircleAvatar(
                                    backgroundImage: NetworkImage(widget
                                        .coAndChallengerModel
                                        .introVideoCoverUrl!),
                                  )
                                : CircleAvatar(
                                    backgroundImage:
                                        FileImage(File(_imagePath!)),
                                  ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: AppText(
                          "Select your intro video cover",
                          color: const Color(0xff484848),
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Divider(
                        height: 2,
                        color: AppColors.borderColor,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          AppText(
                            "Your Social Media",
                            fontSize: 12,
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "facebook";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem("ic_facebook.png",
                                  selectedSocial == "facebook"),
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "youtube";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem("ic_youtube.png",
                                  selectedSocial == "youtube"),
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "tweeter";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem("ic_tweeter.png",
                                  selectedSocial == "tweeter"),
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "hand";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem(
                                  "ic_hand.png", selectedSocial == "hand"),
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "instagram";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem("ic_instagram.png",
                                  selectedSocial == "instagram"),
                            ),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selectedSocial = "linkedin";
                              });
                            },
                            child: SizedBox(
                              height: 24,
                              width: 24,
                              child: _socialItem("ic_linkedin.png",
                                  selectedSocial == "linkedin"),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      if (selectedSocial == "facebook")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: facebookController,
                        ),
                      if (selectedSocial == "youtube")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: youtubeController,
                        ),
                      if (selectedSocial == "tweeter")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: tweeterController,
                        ),
                      if (selectedSocial == "hand")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: handController,
                        ),
                      if (selectedSocial == "instagram")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: instagramController,
                        ),
                      if (selectedSocial == "linkedin")
                        AppTextField(
                          hintText: "Enter the link ...",
                          textEditingController: linkedinController,
                        ),
                      const SizedBox(
                        height: 15,
                      ),
                      AppTextField(
                        hintText: "Write a summery about yourself ...",
                        borderRadius: 24,
                        textEditingController: summeryController,
                        keyboardType: TextInputType.multiline,
                        height: 104,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          const Spacer(),
                          if (!widget.fromRegister)
                          AppButton.unFocused(
                              height: 48,
                              width: 100,
                              text: "Cancel",
                              onPressed: () {
                                Get.back();
                              }),
                          const SizedBox(
                            width: 10,
                          ),
                          _action()
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                )));
  }

  Widget _socialItem(String image, bool isOriginal) {
    if (isOriginal) {
      return AppImage(image);
    }
    return ColorFiltered(
      colorFilter: const ColorFilter.matrix(<double>[
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0.2126,
        0.7152,
        0.0722,
        0,
        0,
        0,
        0,
        0,
        1,
        0,
      ]),
      child: AppImage(image),
    );
  }
}
