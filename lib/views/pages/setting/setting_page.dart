import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/user_reporitory.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/setting/change_password/change_password_page.dart';
import 'package:challenger/views/pages/splash/splash_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field_with_icon.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:challenger/views/widgets/common/login_action_bar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {

  Widget _action() {
    return AppMagicButton<String>(
      text: "Log In",
      postUrl: "https://google.com",
      isFake: true,
      getData: () {
        return RequestBodyBuilder()
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) {
        Get.to(() => ExplorerPage());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
      title: "Settings",
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: [
            // const SizedBox(height: 10,),
            // _clickable(_iconTextButton("Change Password", "ic_lock.png"), () {
            //   Get.to(ChangePasswordPage());
            // }),
            const SizedBox(
              height: 10,
            ),
            _clickable(_iconTextButton("Log out", "ic_logout.png"), () async {
              await Amplify.DataStore.clear();
              final FirebaseAuth auth = FirebaseAuth.instance;
              await auth.signOut();
              AppStorage.getInstance().setUserLogout();
              Get.offAll(SplashPage());
            }),
            _clickable(_iconTextButton("Delete account", "ic_user.png"), () async {
              AppDialog.showAlertDialog(
                  title: "Delete Account",
                  description:
                  "By deleting the account, all your information will be deleted" + "\n" +  "Are you sure to delete your account?",
                  onAction: () {
                    AppLoadingDialog.show();
                    UserRepository().deleteAccount(
                        AppDataListener(onSuccess: (message) async {
                          await Amplify.DataStore.clear();
                          final FirebaseAuth auth = FirebaseAuth.instance;
                          await auth.signOut();
                          AppStorage.getInstance().setUserLogout();
                          AppLoadingDialog.hide();
                          Get.offAll(SplashPage());
                        }, onFailure: (message) {
                          AppLoadingDialog.hide();
                          AppMessage.showDialogMessage(
                              message);
                        }));
                  });
            }),
          ],
        ),
      ),
    );
  }

  Widget _iconTextButton(String text, String icon, {double size = 20}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          AppImage(
            icon,
            width: size,
            height: size,
          ),
          const SizedBox(
            width: 15,
          ),
          AppText(text)
        ],
      ),
    );
  }

  Widget _clickable(Widget child, Function onTap) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: child,
    );
  }
}
