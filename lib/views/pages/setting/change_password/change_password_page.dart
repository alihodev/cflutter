import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final currentPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final newPasswordConfirmController = TextEditingController();

  Widget _action() {
    return AppMagicButton<String>(
      text: "Update Password",
      postUrl: "https://google.com",
      isFake: true,
      width: 142,
      fontSize: 14,
      borderRadius: 8,
      getData: () {
        return RequestBodyBuilder()
            .addField("username", currentPasswordController.text)
            .addField("password", newPasswordController.text)
            .addField("password", newPasswordConfirmController.text)
            .build();
      },
      onDone: (message) {},
      onDoneWithData: (response) {
        Get.to(() => ExplorerPage());
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    currentPasswordController.dispose();
    newPasswordController.dispose();
    newPasswordConfirmController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
      title: "Change Password",
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: SingleChildScrollView(child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),

            AppText(
              "Password",
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: const Color(0xff101828),
            ),
            AppText.body("Please enter your current password to change your password.", fontSize: 14,),
            const SizedBox(height: 20,),
            const Divider(height: 2,),
            const SizedBox(height: 20,),
            AppTextField.round(label: "Current password",textEditingController: currentPasswordController, obscureText: true,),
            const SizedBox(height: 20,),
            const Divider(height: 2,),
            const SizedBox(height: 20,),
            AppTextField.round(label: "New password",textEditingController: newPasswordController, obscureText: true,),
            const SizedBox(height: 5,),
            AppText.body("Your new password must be more than 8 characters.", fontSize: 14,),
            const SizedBox(height: 20,),
            const Divider(height: 2,),
            const SizedBox(height: 20,),
            AppTextField.round(label: "Confirm password",textEditingController: newPasswordConfirmController, obscureText: true,),
            const SizedBox(height: 20,),
            const Divider(height: 2,),
            const SizedBox(height: 20,),
            Row(children: [
              const Spacer(),
              AppButton.unFocused(height: 48, width : 100, text: "Cancel", onPressed: (){
                Get.back();
              }),
              const SizedBox(width: 10,),
              _action()
            ],)
          ],
        ),)
      ),
    );
  }
}
