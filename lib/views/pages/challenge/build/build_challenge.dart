import 'package:challenger/views/pages/challenge/build/views/step1.dart';
import 'package:challenger/views/pages/challenge/build/views/step2.dart';
import 'package:challenger/views/pages/challenge/build/views/step3.dart';
import 'package:challenger/views/pages/challenge/build/views/step4.dart';
import 'package:challenger/views/pages/challenge/build/views/step5.dart';
import 'package:challenger/views/pages/challenge/build/views/step6.dart';
import 'package:challenger/views/pages/challenge/build/views/step7.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class BuildChallengePage extends StatefulWidget {
  const BuildChallengePage({Key? key}) : super(key: key);

  @override
  State<BuildChallengePage> createState() => _BuildChallengePageState();
}

class _BuildChallengePageState extends State<BuildChallengePage> {
  int _step = 1;

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(title: "Build Challenge",
        logoWithTitle: true, body: _getView());
  }

  void next() {
    setState(() {
      _step += 1;
    });
  }

  void previous() {
    setState(() {
      _step -= 1;
    });
  }

  Widget _getView() {
    switch (_step) {
      case 1:
        return Step1View(
          onNext: next,
        );
      case 2:
        return Step2View(onNext: next, onPrevious: previous);
      case 3:
        return Step3View(onNext: next, onPrevious: previous);
      case 4:
        return Step4View(onNext: next, onPrevious: previous);
      case 5:
        return Step5View(onNext: next, onPrevious: previous);
      case 6:
        return Step6View(onNext: next, onPrevious: previous);
      case 7:
        return Step7View(onNext: next, onPrevious: previous);
      default:
        return Step1View(
          onNext: next,
        );
    }
  }
}
