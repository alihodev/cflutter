import 'dart:io';

import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';

class Step3View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step3View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step3View> createState() => _Step3ViewState();
}

class _Step3ViewState extends State<Step3View> {
  String? _disadvantage;
  String? _advantage;
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _disadvantage = model.disadvantage;
    _advantage = model.advantage;
    setState(() {
    });
  }

  void next() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    model.disadvantage = _disadvantage;
    model.advantage = _advantage;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model);
    widget.onNext();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 3),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(title: "Bottom edge (disadvantage)", hintText: "The first fact of problem is that...", label: "What are the problems before your challenge\n(individual or social)?",value: _disadvantage, onValueChange: (value){
              _disadvantage = value;
            }),
            AppBuildChallengeTextField(title: "Upper edge (advantage)",hintText: "The first fact of problem is that...", label: "What are the benefits after your challenge\n(individual or social)?", value: _advantage, onValueChange: (value){
              _advantage = value;
            }),
            const SizedBox(height: 30,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
                const SizedBox(
                  width: 10,
                ),
                AppButton.focused(
                    text: "Next",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isNextButton: true,
                    onPressed: () {
                      next();
                    }),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
