import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Step7View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step7View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step7View> createState() => _Step7ViewState();
}

class _Step7ViewState extends State<Step7View> {
  String? _endAchievement;
  String? _midAchievement;
  String? _firstAchievement;

  CreateChallengeRequestModel? model;

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    model = await AppStorage.getInstance().getBuildChallengeRequestModel();
  }

  void uploadCover() {
    if (model!.coverLocal == null || model!.introLocal == null) {
      AppMessage.showDialogMessage("Please add video and cover for your challenge");
      return;
    }
    if (model!.firstStageCount == 0 || model!.secondStageCount == 0 || model!.endStageCount == 0) {
      AppMessage.showDialogMessage("Please complete all fields");
      return;
    }
    if (model!.cover == null) {
    AppLoadingDialog.show();
      uploadFile("/file/image", model!.coverLocal!,
          appDataListener: AppDataListener(onSuccess: (response) {
            model?.cover = response['data']['id'];
            AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
            AppLoadingDialog.hide();
            uploadIntroVideo();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      uploadIntroVideo();
    }
  }

  void uploadIntroVideo() {
    if (model!.introVideo == null) {
    AppLoadingDialog.show();
      uploadFile("/file/video", model!.introLocal!,
          appDataListener: AppDataListener(onSuccess: (response) {
            model?.introVideo = response['data']['id'];
            AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
            AppLoadingDialog.hide();
            upload();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      upload();
    }
  }

  void upload() async {
    AppLoadingDialog.show();
    model?.endStageAchievement = _endAchievement;
    model?.secondStageAchievement = _midAchievement;
    model?.firstStageAchievement = _firstAchievement;

    HttpService().post("/challenge/", body: model!.toJson(),
        onSuccess: (response) {
      AppStorage.getInstance().removeBuildChallengeRequestModel();
      AppLoadingDialog.hide();
      AppMessage.showDialogMessage(response['message'], onOkAction: () {
        Get.off(ChallengePage(
          initialTabIndex: 0,
          id: response['data']['id'],
          title: response['data']['title'],
        ));
      });
    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    }, onForbidden: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    });
  }

  Widget _action() {
    return AppButton(
        text: "Save",
        width: 311,
        onPressed: () {
          uploadCover();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 7),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(
                title: "Three Steps (back casting)",
                hintText: "First...",
                label:
                    "1. the End:\nWhat will you achive on the last day of the challenge?",
                value: _endAchievement,
                onValueChange: (value) {
                  _endAchievement = value;
                }),
            AppText("How many days does this stage take?"),
            AppTextField(borderRadius: 4,keyboardType: TextInputType.number, onChange: (value){
              model!.endStageCount = int.tryParse(value) ?? 0;
            },),
            const SizedBox(height: 20,),
            AppBuildChallengeTextField(
                title: "",
                hintText: "First...",
                label:
                    "2. Second Step:\nWhat are your achievements at the beginning of the second stage?",
                value: _midAchievement,
                onValueChange: (value) {
                  _midAchievement = value;
                }),
            AppText("How many days does this stage take?"),
            AppTextField(borderRadius: 4,keyboardType: TextInputType.number, onChange: (value){
              model!.secondStageCount = int.tryParse(value) ?? 0;
            },),
            const SizedBox(height: 20,),
            AppBuildChallengeTextField(
                title: "",
                hintText:
                    "I have to do ...(action) on first day to reach...(goal).",
                label:
                    "First Step:\nFrom today until reaching the second stage, what goals should you achieve every day (give a daily plan ) ?\n\nFirst day:",
                onValueChange: (value) {
                  _firstAchievement = value;
                }),
            AppText("How many days does this stage take?"),
            AppTextField(borderRadius: 4,keyboardType: TextInputType.number, onChange: (value){
              model!.firstStageCount = int.tryParse(value) ?? 0;
            },),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            _action(),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
