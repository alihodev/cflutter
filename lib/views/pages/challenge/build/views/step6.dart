import 'dart:io';

import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';

class Step6View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step6View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step6View> createState() => _Step6ViewState();
}

class _Step6ViewState extends State<Step6View> {
  String? _realIdentity;
  String? _idealIdentity;
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _realIdentity = model.realIdentity;
    _idealIdentity = model.idealIdentity;
    setState(() {
    });
  }

  void next() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    model.realIdentity = _realIdentity;
    model.idealIdentity = _idealIdentity;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model);
    widget.onNext();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 6),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(title: "Real Identity (before your challenge)",hintText: "A person who...", label: "Others knows you now  as (before challenge) ?",value: _realIdentity, onValueChange: (value){
              _realIdentity = value;
            }),
            AppBuildChallengeTextField(title: "Ideal Identity (After your challenge)",hintText: "A person who...", label: "Others knows you in future  as (After challenge) ?",value: _idealIdentity, onValueChange: (value){
              _idealIdentity = value;
            }),
            const SizedBox(height: 30,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
                const SizedBox(
                  width: 10,
                ),
                AppButton.focused(
                    text: "Next",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isNextButton: true,
                    onPressed: () {
                      next();
                    }),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
