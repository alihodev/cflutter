import 'dart:io';

import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';

class Step4View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step4View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step4View> createState() => _Step4ViewState();
}

class _Step4ViewState extends State<Step4View> {
  String? _influence;
  String? _necessities;
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _influence = model.influence;
    _necessities = model.necessities;
    setState(() {
    });
  }

  void next() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    model.influence = _influence;
    model.necessities = _necessities;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model);
    widget.onNext();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 4),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(title: "Under the influence",hintText: "the first ones who...", label: "Who is your challenge useful for?",value: _influence, onValueChange: (value){
              _influence = value;
            }),
            AppBuildChallengeTextField(title: "Necessities",hintText: "the first ones is...", label: "What are the most essential things you need for this challenge ?",value: _necessities, onValueChange: (value){
              _necessities = value;
            }),
            const SizedBox(height: 30,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
                const SizedBox(
                  width: 10,
                ),
                AppButton.focused(
                    text: "Next",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isNextButton: true,
                    onPressed: () {
                      next();
                    }),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
