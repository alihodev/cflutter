import 'dart:io';

import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:get/get.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';

class Step2View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step2View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step2View> createState() => _Step2ViewState();
}

class _Step2ViewState extends State<Step2View> {
  String? _videoPath;
  Image? _thumbnailImage;

  String? _initiator;
  @override
  void initState() {
    super.initState();
    initData();
  }
  CreateChallengeRequestModel? model;
  void initData() async {
    model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _videoPath = model?.introLocal;
    setState(() {
    _initiator = model?.initiator ?? "";
    });
  }

  void next() async {
    model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    model?.initiator = _initiator;
    model?.introLocal = _videoPath;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
    widget.onNext();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 2),
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: AppText(
                "Intro",
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: const Color(0xff1D1D1D),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            AppBorderContainer(
              height: 180,
              width: double.maxFinite,
              borderRadius: 24,
              padding: const EdgeInsets.all(1),
              child: InkWell(
                onTap: () {
                  AppMediaPicker().pickVideoFromGallery((pickedFile, thumbnail) {
                    setState(() {
                      _videoPath = pickedFile;
                      _thumbnailImage = thumbnail;
                      model!.introVideo = null;
                    });
                  });
                },
                child: _videoPath == null
                    ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const SizedBox(
                          height: 35,
                        ),
                        AppImage(
                          "ic_video.png",
                          width: 52,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        AppText(
                          "Select your challenge cover",
                          color: const Color(0xff484848),
                          fontWeight: FontWeight.w400,
                        ),
                      ],
                    )
                    : ClipRRect(
                  borderRadius: BorderRadius.circular(24),
                  child: Container(
                    color: Colors.black,
                    child: Stack(children: [
                      Container(
                        width: double.maxFinite,
                        child:
                        _thumbnailImage ?? Container(),
                      ),
                      InkWell(onTap: (){
                        Get.to(VideoPlayerPage(path: _videoPath,));
                      }, child: const Center(child: Icon(Icons.play_circle, color: Colors.white,size: 60,)),)


                    ],),

                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(title: "Initiator", label: "What made this challenge important to you?", value: _initiator, onValueChange: (value){
              _initiator = value;
            }),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
                const SizedBox(
                  width: 10,
                ),
                AppButton.focused(
                    text: "Next",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isNextButton: true,
                    onPressed: () {
                      next();
                    }),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
