import 'dart:io';

import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';

class Step5View extends StatefulWidget {
  Function onNext;
  Function onPrevious;

  Step5View({Key? key, required this.onNext, required this.onPrevious})
      : super(key: key);

  @override
  State<Step5View> createState() => _Step5ViewState();
}

class _Step5ViewState extends State<Step5View> {
  String? _coChallengers;
  String? _honor;
  String? _reasons;
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _coChallengers = model.contributors;
    _honor = model.honor;
    _reasons = model.whyYou;
    setState(() {
    });
  }

  void next() async {
    CreateChallengeRequestModel model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    model.honor = _honor;
    model.whyYou = _reasons;
    model.contributors=  _coChallengers;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model);
    widget.onNext();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            StepRow(stepIndex: 5),
            const SizedBox(
              height: 20,
            ),
            AppBuildChallengeTextField(title: "Contributors",hintText: "the first of all...", label: "Who are the most important people who can help you with this challenge ?",value: _coChallengers, onValueChange: (value){
              _coChallengers = value;
            }),
            AppBuildChallengeTextField(title: "Honor",hintText: "I present the honor of my challenge to ... because ...", label: "",value: _honor, onValueChange: (value){
              _honor = value;
            }),
            AppBuildChallengeTextField(title: "Why you",hintText: "first...", label: "Why are you the best person for this challenge ?",value: _reasons, onValueChange: (value){
              _reasons = value;
            }),
            const SizedBox(height: 30,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppButton.unFocused(
                    text: "Previous",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isPreviousButton: true,
                    onPressed: () {
                      widget.onPrevious();
                    }),
                const SizedBox(
                  width: 10,
                ),
                AppButton.focused(
                    text: "Next",
                    height: 44,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    isNextButton: true,
                    onPressed: () {
                      next();
                    }),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
