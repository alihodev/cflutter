import 'dart:io';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/data/repository/file_reporitory.dart';
import 'package:challenger/models/category_model.dart';
import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/build/views/step2.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/prize/build/attach_users.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/step_row.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Step1View extends StatefulWidget {
  Function onNext;

  Step1View({Key? key, required this.onNext}) : super(key: key);

  @override
  State<Step1View> createState() => _Step1ViewState();
}

class _Step1ViewState extends State<Step1View> {
  List<CategoryModel> _list = [];
  int _selectedId = -1;
  String category = "";
  String categoryId = "";
  String _selectedValue = "everyOne";
  List<UserModel> userList = [];
  String? _imagePath;
  String? _videoPath;
  Image? _thumbnailImage;

  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    initData();
  }

  CreateChallengeRequestModel? model;

  void initData() async {
    model = await AppStorage.getInstance().getBuildChallengeRequestModel();
    _nameController.text = model!.title ?? "";
    _descriptionController.text = model?.description ?? "";
    _imagePath = model?.coverLocal;
    setState(() {});
  }

  void next() async {
    if (categoryId.isEmpty) {
      AppMessage.showDialogMessage(
          "You should select a category for your challenge");
      return;
    }
    model?.title = _nameController.text;
    model?.description = _descriptionController.text;
    model?.coverLocal = _imagePath;
    model?.category = categoryId;
    model?.type = 0;
    AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
    widget.onNext();
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _descriptionController.dispose();
  }

  void uploadCover() {
    if (categoryId.isEmpty) {
      AppMessage.showDialogMessage(
          "You should select a category for your challenge");
      return;
    }

    if (model!.coverLocal == null || model!.introLocal == null) {
      AppMessage.showDialogMessage(
          "Please add video and cover for your challenge");
      return;
    }
    if (model!.firstStageCount == 0 ||
        model!.secondStageCount == 0 ||
        model!.endStageCount == 0) {
      AppMessage.showDialogMessage("Please complete all fields");
      return;
    }
    if (model!.cover == null) {
      AppLoadingDialog.show();
      FileRepository().uploadImage(
          model!.coverLocal!,
          AppDataListener(onSuccess: (response) {
            model?.cover = response['id'];
            AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
            AppLoadingDialog.hide();
            uploadIntroVideo();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      uploadIntroVideo();
    }
  }

  void uploadIntroVideo() {
    if (model!.introVideo == null) {
      AppLoadingDialog.show();
      FileRepository().uploadVideo(
          model!.introLocal!,
          AppDataListener(onSuccess: (response) {
            model?.introVideo = response['id'];
            AppStorage.getInstance().updateBuildChallengeRequestModel(model!);
            AppLoadingDialog.hide();
            upload();
          }, onFailure: (message) {
            AppLoadingDialog.hide();
            AppMessage.showDialogMessage(message);
          }));
    } else {
      upload();
    }
  }

  void upload() async {
    AppLoadingDialog.show();
    HttpService().post("/challenge/", body: model!.toJson(),
        onSuccess: (response) {
      AppStorage.getInstance().removeBuildChallengeRequestModel();
      AppLoadingDialog.hide();
      AppMessage.showDialogMessage(response['message'], onOkAction: () {
        Get.off(ChallengePage(
          initialTabIndex: 0,
          id: response['data']['id'],
          title: response['data']['title'],
        ));
      });
    }, onFailure: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    }, onForbidden: (message) {
      AppLoadingDialog.hide();
      AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
        upload();
      });
    });
  }

  Widget _action() {
    return AppButton(
        text: "Save",
        width: 311,
        onPressed: () {
          model!.description = _descriptionController.text;
          model!.title = _nameController.text;
          model?.category = categoryId;
          model?.type = 0;
          uploadCover();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            // StepRow(stepIndex: 1),
            const SizedBox(
              height: 20,
            ),
            AppTextField(
              hintText: "Challenge Name",
              textEditingController: _nameController,
            ),
            const SizedBox(
              height: 15,
            ),
            AppTextField(
              hintText: "Challenge Description",
              height: 104,
              borderRadius: 24,
              textEditingController: _descriptionController,
              keyboardType: TextInputType.multiline,
            ),
            const SizedBox(
              height: 20,
            ),

            AppBorderContainer(
              height: 175,
              width: 175,
              borderRadius: 250,
              padding: const EdgeInsets.all(1),
              child: InkWell(
                onTap: () {
                  AppMediaPicker().pickImageFromGallery((pickedFile) {
                    setState(() {
                      _imagePath = pickedFile;
                      model!.coverLocal = _imagePath;
                      model!.cover = null;
                    });
                  });
                },
                child: _imagePath == null
                    ? Container(
                        padding: const EdgeInsets.all(50),
                        child: AppImage(
                          "ic_photo.png",
                        ),
                      )
                    : CircleAvatar(
                        backgroundImage: FileImage(File(_imagePath!)),
                      ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            AppText(
              "Select your challenge cover",
              color: const Color(0xff484848),
              fontWeight: FontWeight.w400,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: AppText(
                "Intro",
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: const Color(0xff1D1D1D),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            AppBorderContainer(
              height: 180,
              width: double.maxFinite,
              borderRadius: 24,
              padding: const EdgeInsets.all(1),
              child: InkWell(
                onTap: () {
                  AppMediaPicker()
                      .pickVideoFromGallery((pickedFile, thumbnail) {
                    setState(() {
                      _videoPath = pickedFile;
                      _thumbnailImage = thumbnail;
                      model!.introLocal = _videoPath;
                      model!.introVideo = null;
                    });
                  });
                },
                child: _videoPath == null
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(
                            height: 35,
                          ),
                          AppImage(
                            "ic_video.png",
                            width: 52,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          AppText(
                            "Select your challenge cover",
                            color: const Color(0xff484848),
                            fontWeight: FontWeight.w400,
                          ),
                        ],
                      )
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(24),
                        child: Container(
                          color: Colors.black,
                          child: Stack(
                            children: [
                              Container(
                                width: double.maxFinite,
                                child: _thumbnailImage ?? Container(),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(VideoPlayerPage(
                                    path: _videoPath,
                                  ));
                                },
                                child: const Center(
                                    child: Icon(
                                  Icons.play_circle,
                                  color: Colors.white,
                                  size: 60,
                                )),
                              )
                            ],
                          ),
                        ),
                      ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),
            AppText("How many days does first stage take?"),
            AppTextField(
              borderRadius: 4,
              keyboardType: TextInputType.number,
              onChange: (value) {
                model!.firstStageCount = int.tryParse(value) ?? 0;
              },
            ),
            const SizedBox(
              height: 20,
            ),
            AppText("How many days does second stage take?"),
            AppTextField(
              borderRadius: 4,
              keyboardType: TextInputType.number,
              onChange: (value) {
                model!.secondStageCount = int.tryParse(value) ?? 0;
              },
            ),
            const SizedBox(
              height: 20,
            ),
            AppText("How many days does end stage take?"),
            AppTextField(
              borderRadius: 4,
              keyboardType: TextInputType.number,
              onChange: (value) {
                model!.endStageCount = int.tryParse(value) ?? 0;
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: AppText(
                "Category",
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: const Color(0xff1D1D1D),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            AppSearchTextField(
                borderRadius: 30,
                horizontalMargin: 0,
                onChaneValue: (value) {
                  setState(() {
                    category = value;
                  });
                }),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: double.infinity,
              child: AppMagicFutureBuilder(
                  url: "/category/?query=$category",
                  isFake: false,
                  onSuccess: (result) {
                    _list.clear();
                    for (var item in result) {
                      _list.add(CategoryModel(item["id"], item["title"]));
                    }
                    return Wrap(
                      crossAxisAlignment: WrapCrossAlignment.start,
                      alignment: WrapAlignment.start,
                      spacing: 5,
                      runSpacing: 5,
                      children: [
                        ..._list.map((e) => _categoryItem(e.id, e.value))
                      ],
                    );
                  }),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            // Container(
            //   alignment: Alignment.centerLeft,
            //   child: AppText(
            //     "Type",
            //     fontSize: 18,
            //     fontWeight: FontWeight.w600,
            //     color: const Color(0xff1D1D1D),
            //   ),
            // ),
            // Container(
            //   alignment: Alignment.centerLeft,
            //   child: AppText.body("Who can get this challenge?"),
            // ),
            // const SizedBox(
            //   height: 5,
            // ),
            // RadioListTile(
            //     title: AppText.body("Every One"),
            //     value: "everyOne",
            //     groupValue: _selectedValue,
            //     activeColor: AppColors.primary,
            //     dense: true,
            //     onChanged: (value) {
            //       setState(() {
            //         userList.clear();
            //         _selectedValue = "everyOne";
            //       });
            //     }),
            // Row(
            //   children: [
            //     SizedBox(
            //       width: 250,
            //       child: RadioListTile(
            //           title: AppText.body(
            //               "People i choose${_selectedValue == "peopleIChoose" ? "   (${userList.length})" : ""}"),
            //           value: "peopleIChoose",
            //           groupValue: _selectedValue,
            //           dense: true,
            //           activeColor: AppColors.primary,
            //           onChanged: (value) {
            //             setState(() {
            //               _selectedValue = "peopleIChoose";
            //             });
            //           }),
            //     ),
            //     if (_selectedValue == "peopleIChoose")
            //       AppButton.flat(
            //           width: 60,
            //           text: "+ Add",
            //           onPressed: () {
            //             AppDialog(
            //                 body: AttachUsers(
            //               users: userList,
            //               onSelectUsers: (users) {
            //                 Get.back();
            //                 setState(() {
            //                   userList = users;
            //                 });
            //               },
            //             ));
            //           })
            //   ],
            // ),
            // RadioListTile(
            //     title: AppText.body("Followers"),
            //     value: "followers",
            //     groupValue: _selectedValue,
            //     activeColor: AppColors.primary,
            //     dense: true,
            //     onChanged: (value) {
            //       setState(() {
            //         userList.clear();
            //         _selectedValue = "followers";
            //       });
            //     }),
            const SizedBox(
              height: 20,
            ),
            _action(),
            // AppButton.focused(text: "Next", height: 44, fontSize:16, fontWeight: FontWeight.w500 ,isNextButton: true, onPressed: (){
            //   next();
            // }),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget _categoryItem(int id, String value) {
    return InkWell(
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        margin: const EdgeInsets.symmetric(horizontal: 3),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(
                color: id == (_selectedId == -1 ? 0 : _selectedId)
                    ? AppColors.primary
                    : AppColors.unSelectedCategory,
                width: 1)),
        child: AppText(value,
            fontSize: 14,
            color: id == (_selectedId == -1 ? 0 : _selectedId)
                ? AppColors.primary
                : AppColors.unSelectedCategory),
      ),
      onTap: () {
        setState(() {
          _selectedId = id;
          categoryId = id.toString();
        });
        // widget.onSelectCategory(value);
      },
    );
  }
}
