import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WishListPage extends StatefulWidget {
  int id;

  WishListPage({Key? key, required this.id}) : super(key: key);

  @override
  State<WishListPage> createState() => _BuildDebateState();
}

class _BuildDebateState extends State<WishListPage> {
  DateTime startDateTime = DateTime.now();
  DateTime endDateTime = DateTime.now();
  String _selectedValue = "everyOne";

  List<UserModel> userList = [];
  String _title = "";
  String _description = "";

  Widget _action() {
    return AppMagicButton(
      text: "Save",
      postUrl: "/wish/",
      getData: () {
        return RequestBodyBuilder()
            .addField("title", _title)
            .addField("description", _description)
            .addField("challengeId", widget.id)
            .build();
      },
      onDone: (response) {
        AppMessage.showDialogMessage(response, cancelable: false,
            onOkAction: () {
          Get.back();
        });
      },
      width: double.maxFinite,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "Wishlist",
        logoWithTitle: true,
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                AppText.body(
                  "Please Write what you will need to complete your challenge",
                  fontSize: 12,
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Wish item",
                  onChange: (value) {
                    _title = value;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Wish Description",
                  keyboardType: TextInputType.multiline,
                  onChange: (value) {
                    _description = value;
                  },
                  height: 104,
                  borderRadius: 24,
                ),
                const SizedBox(
                  height: 30,
                ),
                _action(),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ));
  }
}
