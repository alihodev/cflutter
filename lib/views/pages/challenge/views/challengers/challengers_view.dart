import 'dart:math';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/pages/contributor/build/build_countributor_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:get/get.dart';
import 'package:challenger/views/pages/explorer/views/co_and_challengers/co_and_challenger_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class ChallengersView extends StatefulWidget {
  int id;

  ChallengersView({Key? key, required this.id}) : super(key: key);

  @override
  State<ChallengersView> createState() => _ChallengersViewState();
}

class _ChallengersViewState extends State<ChallengersView> {
  List<CoAndChallengerModel> challenges = [];
  String query = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.centerLeft,
          child: AppText.h1(
            "Main Challenger",
            fontSize: 18,
          ),
        ),
        Container(
          child: AppMagicFutureBuilder(
            url: "/challenge/${widget.id}/main-challenger",
            onSuccess: (response) {
              return CoAndChallengerItem(userModel: CoAndChallengerModel.fromJson(response));
            },
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.centerLeft,
          child: AppText.h1(
            "Co Challengers",
            fontSize: 18,
          ),
        ),
        Expanded(
            child: AppPaginatedListView<CoAndChallengerModel>(
          url: "/challenge/${widget.id}/co-challengers",
          query: query,
          onFetchData: (response) {
            return response
                .map<CoAndChallengerModel>((e) => CoAndChallengerModel.fromJson(e))
                .toList();
          },
          renderItem: (item) => CoAndChallengerItem(userModel: item),
        )),
      ],
    );
  }

  Widget _tabItem({
    required String text,
  }) {
    List<Color> textColors = [
      Color(0xff6941C6),
      Color(0xff175CD3),
      Color(0xff3538CD),
      Color(0xffC11574)
    ];
    List<Color> backgroundColors = [
      Color(0xffF9F5FF),
      Color(0xffEFF8FF),
      Color(0xffEEF4FF),
      Color(0xffFDF2FA)
    ];
    int randId = Random().nextInt(3);
    return Container(
      margin: const EdgeInsets.only(right: 5),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
          color: backgroundColors[randId],
          borderRadius: BorderRadius.circular(20)),
      child: AppText(
        text,
        fontSize: 12,
        color: textColors[randId],
        fontWeight: FontWeight.w700,
      ),
    );
  }
}
