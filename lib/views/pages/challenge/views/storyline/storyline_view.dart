import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/challenge/stage/stage_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StorylineView extends StatefulWidget {
  int id;

  StorylineView({Key? key, required this.id}) : super(key: key);

  @override
  State<StorylineView> createState() => _StorylineViewState();
}

class _StorylineViewState extends State<StorylineView> {
  int _selectedIndex = 3;
  List<String> steps = [
    // "Building",
    // "Opening",
    "First Stage",
    "Second Stage",
    "End Stage"
  ];
  int index = 1;

  int _getSelectedIndex(ChallengeModel model) {
    switch (model.stage) {
      // case "OPENING":
      //   return 1;
      case "FIRST_STAGE":
        return 1;
      case "SECOND_STAGE":
        return 2;
      case "END_STAGE":
        return 3;
      default:
        return 0;
    }
  }

  void _openChallenge(ChallengeModel challengeModel) {
    AppLoadingDialog.show();
    ChallengeRepository().openChallenge(
        challengeModel.id!,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(response, cancelable: false,
              onOkAction: () {
                Get.back();
                Get.to(ChallengePage(
                    initialTabIndex: 2,
                    id: challengeModel.id!,
                    title: challengeModel.title!));
              });
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            _openChallenge(challengeModel);
          });
        }));
  }

  void _stopChallenge(ChallengeModel challengeModel) {
    AppLoadingDialog.show();
    ChallengeRepository().stopChallenge(
        challengeModel.id!,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(response, cancelable: false,
              onOkAction: () {
                Get.back();
                Get.to(ChallengePage(
                    initialTabIndex: 2,
                    id: challengeModel.id!,
                    title: challengeModel.title!));
              });
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            _openChallenge(challengeModel);
          });
        }));
  }

  void _setStartChallenge(ChallengeModel challengeModel) {
    AppLoadingDialog.show();
    ChallengeRepository().startChallenge(
        challengeModel.id!,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(response, cancelable: false,
              onOkAction: () {
            Get.back();
            Get.to(ChallengePage(
                initialTabIndex: 2,
                id: challengeModel.id!,
                title: challengeModel.title!));
          });
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            _openChallenge(challengeModel);
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return AppMagicFutureBuilder(
        url: "/challenge/${widget.id}",
        onSuccess: (response) {
          ChallengeModel model = ChallengeModel.fromJson(response);
          _selectedIndex = _getSelectedIndex(model);

          return Stack(
            children: [
              Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      RotatedBox(
                        quarterTurns: 3,
                        child: AppText(
                          "Development",
                          fontSize: 12,
                          color: const Color(0xff7b8794),
                        ),
                      ),
                      Expanded(
                        child: StorylineChart(model),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      RotatedBox(
                        quarterTurns: 3,
                        child: AppText(
                          "Challenge Timeline",
                          fontSize: 12,
                          color: const Color(0xff7b8794),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Column(
                        children: [
                          ...steps.map((e) => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _step(
                                      index: index++,
                                      text: e,
                                      challenge: model),
                                  if (index <= steps.length)
                                    Container(
                                      margin: const EdgeInsets.only(left: 7),
                                      color: AppColors.primary,
                                      height: 20,
                                      width: 2,
                                    ),
                                ],
                              ))
                        ],
                      )),
                    ],
                  ),
                ],
              ),
              if (_getSelectedIndex(model) == 0)
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: AppButton(
                      width: 100,
                      height: 40,
                      fontSize: 12,
                      text: "Start",
                      onPressed: () {
                        _openDialog(model);
                      },
                    ),
                  ),
                ),
              if (model.status == "CANCELED")
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: AppText(
                      "This challenge is stopped", color: Colors.red,
                    ),
                  ),
                ),
            ],
          );
        });
  }

  Widget StorylineChart(ChallengeModel challengeModel) {
    return Column(
      children: [
        Container(
          alignment: Alignment.bottomCenter,
          margin: const EdgeInsets.only(top: 20, right: 0),
          height: 180,
          width: 290,
          child: Stack(
            children: [
              Container(
                child: AppImage.asset(
                  "chart_background.png",
                  width: 290,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(top: 10),
                width: 310,
                child: AppImage.asset(
                  "chart_gradient_background.png",
                  width: 290,
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(bottom: 9),
                width: 310,
                child: AppImage.asset(
                  "chart_line.png",
                  width: 290,
                ),
              ),
              // Container(
              //   width: 11,
              //   height: 11,
              //   margin: const EdgeInsets.only(top: 155),
              //   decoration: BoxDecoration(
              //       color: challengeModel.stage == "BUILD"
              //           ? const Color(0xff2259C9)
              //           : Colors.white,
              //       borderRadius: BorderRadius.circular(20),
              //       border: Border.all(width: 2, color: AppColors.primary)),
              // ),

              Container(
                width: 11,
                height: 11,
                margin: const EdgeInsets.only(top: 155, left: 70),
                decoration: BoxDecoration(
                    color: challengeModel.stage == "OPENING" ||
                            challengeModel.stage == "BUILD"
                        ? const Color(0xff2259C9)
                        : Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 2, color: AppColors.primary)),
              ),
              InkWell(
                onTap: () {
                  if (index > 1) {
                    Get.to(StagePage(
                      challengeModel: challengeModel,
                      stage: "FIRST_STAGE",
                    ));
                  }
                },
                child: Container(
                  width: 11,
                  height: 11,
                  margin: const EdgeInsets.only(top: 143, left: 108),
                  decoration: BoxDecoration(
                      color: challengeModel.stage == "FIRST_STAGE"
                          ? const Color(0xff2259C9)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(width: 2, color: AppColors.primary)),
                ),
              ),
              InkWell(
                onTap: () {
                  if (index > 1) {
                    Get.to(StagePage(
                      challengeModel: challengeModel,
                      stage: "SECOND_STAGE",
                    ));
                  }
                },
                child: Container(
                  width: 11,
                  height: 11,
                  margin: const EdgeInsets.only(top: 98, left: 170),
                  decoration: BoxDecoration(
                      color: challengeModel.stage == "SECOND_STAGE"
                          ? const Color(0xff2259C9)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(width: 2, color: AppColors.primary)),
                ),
              ),
              InkWell(
                onTap: () {
                  if (index > 1) {
                    Get.to(StagePage(
                      challengeModel: challengeModel,
                      stage: "END_STAGE",
                    ));
                  }
                },
                child: Container(
                  width: 11,
                  height: 11,
                  margin: const EdgeInsets.only(top: 7, left: 250),
                  decoration: BoxDecoration(
                      color: challengeModel.stage == "END_STAGE"
                          ? const Color(0xff2259C9)
                          : Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(width: 2, color: AppColors.primary)),
                ),
              ),
              if (challengeModel.challenger?.id ==
                  AppStorage.getInstance().getStaticUser().id  && challengeModel.status == "AVAILABLE" && _getSelectedIndex(challengeModel) != 0)
                InkWell(
                  onTap: () {
                    _stopDialog(challengeModel);
                  },
                  child:Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Row(
                    children: [
                      const Icon(Icons.stop_rounded, size: 15, color: Colors.red,),
                      const SizedBox(
                        width: 2,
                      ),
                      AppText("Stop Challenge",color: Colors.red, fontSize: 10,)
                    ],
                  ),),
                )
              ,
              if (challengeModel.status == "DONE")
                AppImage("finished.png", color: AppColors.primary, width: 200,)
            ],
          ),
        ),
        Container(
          width: 290,
          margin: const EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppText(
                "1D",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "7D",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "1M",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "3M",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "6M",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "9M",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "12M",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
              AppText(
                "Time",
                fontSize: 12,
                color: const Color(0xff7B8794),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _step(
      {required int index,
      required String text,
      required ChallengeModel challenge}) {
    return InkWell(
      onTap: () {
        if (index >= 1) {
          String stage = "FIRST_STAGE";
          if (index == 2) {
            stage = "SECOND_STAGE";
          } else if (index == 3) {
            stage = "END_STAGE";
          }
          Get.to(StagePage(
            challengeModel: challenge,
            stage: stage,
          ));
        }
      },
      child: Row(
        children: [
          Container(
            width: 16,
            height: 16,
            decoration: BoxDecoration(
                color:
                    index == _selectedIndex ? AppColors.primary : Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 3, color: AppColors.primary)),
          ),
          const SizedBox(
            width: 10,
          ),
          Container(
            alignment: Alignment.center,
            width: 15,
            child: AppText(
              "$index",
              fontSize: 12,
              color: index == _selectedIndex
                  ? AppColors.primary
                  : const Color(0xff7b8794),
            ),
          ),
          const SizedBox(
            width: 50,
          ),
          AppText(
            "$index-$text",
            fontWeight:
                index == _selectedIndex ? FontWeight.w600 : FontWeight.w500,
            color: index == _selectedIndex
                ? AppColors.primary
                : const Color(0xff7b8794),
          ),
          // if (index == _selectedIndex)
          //   Expanded(
          //       child: Row(
          //     children: [
          //       const Spacer(),
          //       if (index <= 1 && !(challenge.coChallenge ?? false))
          //         AppButton.flat(
          //             fontSize: 12,
          //             width: 120,
          //             height: 35,
          //             text: _getButtonText(index),
          //             onPressed: () {
          //               if (index == 0) {
          //                 _openDialog(challenge);
          //               } else if (index == 1) {
          //                 _setDailyTargets(challenge);
          //               }
          //             }),
          //       const Spacer(),
          //     ],
          //   ))
        ],
      ),
    );
  }

  void _setDailyTargets(ChallengeModel challengeModel) {
    _setStartChallenge(challengeModel);
  }

  void _openDialog(ChallengeModel challenge) {
    AppDialog(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            AppText.h1(
              "Start Challenge",
              fontSize: 16,
            ),
            AppText.body("Do you want to start this challenge?"),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Spacer(),
                AppButton.flat(
                    width: 130,
                    fontSize: 14,
                    text: "Start Challenge",
                    onPressed: () {
                      Get.back();
                      _openChallenge(challenge);
                    }),
                AppButton.flat(
                    width: 80,
                    fontSize: 14,
                    text: "Cancel",
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ],
        ));
  }

  void _stopDialog(ChallengeModel challenge) {
    AppDialog(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            AppText.h1(
              "Stop Challenge",
              fontSize: 16,
            ),
            AppText.body("Do you want to stop this challenge?"),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Spacer(),
                AppButton.flat(
                    width: 130,
                    fontSize: 14,
                    text: "Stop Challenge",
                    onPressed: () {
                      Get.back();
                      _stopChallenge(challenge);
                    }),
                AppButton.flat(
                    width: 80,
                    fontSize: 14,
                    text: "Cancel",
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ],
        ));
  }

  String _getButtonText(int index) {
    switch (index) {
      case 0:
        return "Open Challenge";
      case 1:
        return "Set Daily Targets";
      case 2:
        return "Open Challenge";
      default:
        return "";
    }
  }
}
