import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class StorylineChart extends StatefulWidget {
  const StorylineChart({Key? key}) : super(key: key);

  @override
  State<StorylineChart> createState() => _StorylineChartState();
}

class _StorylineChartState extends State<StorylineChart> {
  List<Color> gradientColors = [
    Color(0xd30f2d69),
    Color(0xd30f2d69),
    Color(0x1fffffff),
  ];

  bool showAvg = false;
  double _selectedStep = 3;
  double _selectedStepValue = 180;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.70,
          child: Padding(
            padding: const EdgeInsets.only(
              right: 18,
              left: 12,
              top: 24,
              bottom: 12,
            ),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 1,
                      color: AppColors.borderColor,
                    ),
                    Container(
                      height: 1,
                      color: AppColors.borderColor,
                    ),
                    Container(
                      height: 1,
                      color: AppColors.borderColor,
                    ),
                    Container(
                      height: 1,
                      color: AppColors.borderColor,
                    ),
                    Container(
                      height: 1,
                      color: AppColors.borderColor,
                    ),
                    const SizedBox(
                      height: 30,
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                    Container(
                      width: 1,
                      margin: const EdgeInsets.only(bottom: 30),
                      color: AppColors.borderColor,
                    ),
                  ],
                ),
                ClipRRect(child: LineChart(
                  mainData(),
                ),),
                Container(
                  alignment: Alignment.topCenter,
                  margin: const EdgeInsets.only(top: 10),
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.primary,
                        borderRadius: BorderRadius.circular(8)),
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                    child: AppText(
                      Utils.convertDateToMonthAndYear(_selectedStepValue),
                      fontSize: 12,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          width: 60,
          height: 34,
          child: TextButton(
            onPressed: () {
              setState(() {
                showAvg = !showAvg;
              });
            },
            child: Text(
              'avg',
              style: TextStyle(
                fontSize: 12,
                color: showAvg ? Colors.white.withOpacity(0.5) : Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
        fontWeight: FontWeight.w400, fontSize: 12, color: Color(0xff7b8794));
    Widget text;
    switch (value.toInt()) {
      case 0:
        text = const Text('1D', style: style);
        break;
      case 1:
        text = const Text('1M', style: style);
        break;
      case 2:
        text = const Text('3M', style: style);
        break;
      case 3:
        text = const Text('6M', style: style);
        break;
      case 4:
        text = const Text('12M', style: style);
        break;
      default:
        if (value ~/ 12 > 0) {}
        text = const Text('1D', style: style);
        break;
    }

    return SideTitleWidget(
      axisSide: meta.axisSide,
      child: text,
    );
  }

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );
    String text;
    switch (value.toInt()) {
      case 1:
        text = '10K';
        break;
      case 3:
        text = '30k';
        break;
      case 5:
        text = '50k';
        break;
      default:
        return Container();
    }

    return Text(text, style: style, textAlign: TextAlign.left);
  }

  LineChartData mainData() {
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
      ),
      showingTooltipIndicators: [],
      gridData: FlGridData(
        show: false,
        drawVerticalLine: true,
        horizontalInterval: 10,
        verticalInterval: 10,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: AppColors.borderColor,
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: AppColors.borderColor,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 30,
            interval: 1,
            getTitlesWidget: bottomTitleWidgets,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: false,
            interval: 1,
            getTitlesWidget: leftTitleWidgets,
            reservedSize: 42,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border.all(color: AppColors.borderColor),
      ),
      minX: 0,
      minY: -50,
      maxX: 4.5,
      maxY: 400,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 1),
            FlSpot(1, 3),
            FlSpot(2, 50),
            FlSpot(3, 300),
            FlSpot(4, 365),
            FlSpot(5, 365),
          ],
          isCurved: true,
          gradient: const LinearGradient(
            colors: [AppColors.primary, AppColors.primary],
          ),
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: true,
            checkToShowDot: (value, v) {
              return true;
              return value.x == 3;
            },
            getDotPainter: (p0, p1, p2, p3) {
              return FlDotCirclePainter(
                  color: p3 == _selectedStep ? AppColors.primary : Colors.white,
                  radius: p3 == _selectedStep ? 7 : 4);
            },
          ),
          belowBarData: BarAreaData(
            show: true,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: gradientColors
                  .map((color) => color.withOpacity(0.7))
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }

  LineChartData avgData() {
    return LineChartData(
      lineTouchData: LineTouchData(enabled: false),
      gridData: FlGridData(
        show: true,
        drawHorizontalLine: true,
        verticalInterval: 1,
        horizontalInterval: 1,
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 30,
            getTitlesWidget: bottomTitleWidgets,
            interval: 1,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            getTitlesWidget: leftTitleWidgets,
            reservedSize: 42,
            interval: 1,
          ),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border.all(color: const Color(0xff37434d)),
      ),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 3.44),
            FlSpot(2.6, 3.44),
            FlSpot(4.9, 3.44),
            FlSpot(6.8, 3.44),
            FlSpot(8, 3.44),
            FlSpot(9.5, 3.44),
            FlSpot(11, 3.44),
          ],
          isCurved: true,
          gradient: LinearGradient(
            colors: [
              ColorTween(begin: gradientColors[0], end: gradientColors[1])
                  .lerp(0.2)!,
              ColorTween(begin: gradientColors[0], end: gradientColors[1])
                  .lerp(0.2)!,
            ],
          ),
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradient: LinearGradient(
              colors: [
                ColorTween(begin: gradientColors[0], end: gradientColors[1])
                    .lerp(0.2)!
                    .withOpacity(0.1),
                ColorTween(begin: gradientColors[0], end: gradientColors[1])
                    .lerp(0.2)!
                    .withOpacity(0.1),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
