import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/challenge/edit/challenge_edit.dart';
import 'package:challenger/views/pages/chat/chat_page.dart';
import 'package:challenger/views/pages/contributor/co_challengers/co_challengers_page.dart';
import 'package:challenger/views/pages/contributor/followers/followers_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/app_follow_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_toggle_widget.dart';
import 'package:challenger/views/widgets/common/app_like_widget.dart';
import 'package:challenger/views/widgets/common/app_toast_save.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SummaryView extends StatefulWidget {
  int id;

  SummaryView({Key? key, required this.id}) : super(key: key);

  @override
  State<SummaryView> createState() => _SummeryState();
}

class _SummeryState extends State<SummaryView> {
  bool _showSaveToast = false;
  bool _showLikeWidget = false;
  bool showMore = false;

  void coChallenge(ChallengeModel challengeModel) {
    AppLoadingDialog.show();
    ChallengeRepository().coChallenge(
        challengeModel.id!,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          Get.back();
          Get.to(ChallengePage(
              initialTabIndex: 0,
              id: response['id'],
              title: response['title']));
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            coChallenge(challengeModel);
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return AppMagicFutureBuilder(
        url: "/challenge/${widget.id}",
        onSuccess: (response) {
          ChallengeModel model = ChallengeModel.fromJson(response);
          return Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(
                      children: [
                        InkWell(
                          onTap: () {
                            Get.to(VideoPlayerPage.playChallengeVideo(
                              model,
                            ));
                          },
                          child: Image.network(
                            model.coverUrl!,
                            height: 229,
                            width: double.maxFinite,
                            fit: BoxFit.cover,
                          ),
                        ),
                        if (model.challenger?.id ==
                            AppStorage.getInstance().getStaticUser().id)
                          Container(
                            height: 220,
                            alignment: Alignment.bottomCenter,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  AppButton(width: 100,height: 35, text: "Edit", onPressed: (){Get.to(
                                      ChallengeEditPage(challengeModel: model));},)
                                ],
                              ),
                          )
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              FollowWidget(challengeModel: model),
                              const Spacer(),
                              AppText("Progress: ${model.progress ?? 0}%",
                                  fontSize: 12, fontWeight: FontWeight.w400),
                              const Spacer(),
                              InkWell(
                                onTap: () {
                                  Get.to(CoChallengersPage.challenge(
                                      id: model.id!));
                                },
                                child: Row(children: [
                                  AppImage(
                                    "ic_co_challenge.png",
                                    height: 17,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  AppText(
                                      "${model.coChallengers ?? 0} Co Challenger${(model.coChallengers ?? 0) > 1 ? "s" : ""}",
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                ]),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          const Divider(
                            color: AppColors.borderColor,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              _actionItem(
                                  icon: "ic_send.png",
                                  text: "Send",
                                  onClick: () {
                                    AppShare.shareChallenge(model);
                                  }),
                              _actionItem(
                                  icon: "ic_message.png",
                                  text: "Message",
                                  onClick: () {
                                    Get.to(ChatPage(
                                      coAndChallengerModel:
                                          CoAndChallengerModel.fromJson(
                                              model.challenger!.toJson()),
                                    ));
                                  }),
                              _actionItem(
                                  icon: "ic_co_challenge.png",
                                  text: "Co Challenge",
                                  onClick: () {
                                    AppDialog.showAlertDialog(
                                        description:
                                            "Do you want to be co challenger with this challenge?",
                                        onAction: () {
                                          coChallenge(model);
                                        });
                                  }),
                              AppLikeToggle.challenge(model),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const Divider(
                      color: AppColors.borderColor,
                      height: 1,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                child: AppText(
                                  model.title!,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: AppText(
                              model.description!,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: AppButton.flat(
                                text:
                                    "${showMore ? "Hide" : "Show"} More Details",
                                fontSize: 14,
                                onPressed: () {
                                  setState(() {
                                    showMore = !showMore;
                                  });
                                }),
                          ),
                          if (showMore)
                            if (model.challenger?.id ==
                                AppStorage.getInstance().getStaticUser().id)
                              _challengeDetails(model),
                          if (showMore)
                            if (model.challenger?.id !=
                                AppStorage.getInstance().getStaticUser().id)
                              Column(
                                children: [
                                  _titleDescription(
                                      title: "Initiator",
                                      subtitle:
                                          "What made this challenge important to you?",
                                      description:
                                          model.challengeDetail?.initiator ??
                                              "-"),
                                  _titleDescription(
                                      title: "Bottom edge (disadvantage)",
                                      subtitle:
                                          "What are the problems before your challenge (individual or social)?",
                                      description:
                                          model.challengeDetail?.disadvantage ??
                                              "-"),
                                  _titleDescription(
                                      title: "Upper edge (advantage)",
                                      subtitle:
                                          "What are the benefits after your challenge (individual or social)?",
                                      description:
                                          model.challengeDetail?.advantage ??
                                              "-"),
                                  _titleDescription(
                                      title: "Under the influence",
                                      subtitle:
                                          "Who is your challenge useful for?",
                                      description:
                                          model.challengeDetail?.influence ??
                                              "-"),
                                  _titleDescription(
                                      title: "Necessities",
                                      subtitle:
                                          "What are the most essential things you need for this challenge ?",
                                      description:
                                          model.challengeDetail?.necessities ??
                                              "-"),
                                  _titleDescription(
                                      title: "Contributors",
                                      subtitle:
                                          "Who are the most important people who can help you with this challenge ?",
                                      description:
                                          model.challengeDetail?.contributors ??
                                              "-"),
                                  _titleDescription(
                                      title: "Honor",
                                      description:
                                          model.challengeDetail?.honor ?? "-"),
                                  _titleDescription(
                                      title: "Why you",
                                      subtitle:
                                          "Why are you the best person for this challenge ?",
                                      description:
                                          model.challengeDetail?.whyYou ?? "-"),
                                  _titleDescription(
                                      title:
                                          "Real Identity (before your challenge)",
                                      subtitle:
                                          "Others knows you now  as (before challenge) ?",
                                      description:
                                          model.challengeDetail?.realIdentity ??
                                              "-"),
                                  _titleDescription(
                                      title:
                                          "Ideal Identity (After your challenge)",
                                      subtitle:
                                          "Others knows you in future  as (After challenge) ?",
                                      description: model
                                              .challengeDetail?.idealIdentity ??
                                          "-"),
                                  _titleDescription(
                                      title: "Three Steps (back casting)",
                                      subtitle:
                                          "1. the End:\nWhat will you achive on the last day of the challenge?",
                                      description: model.challengeDetail
                                              ?.endStageAchievement ??
                                          "-"),
                                  _titleDescription(
                                      subtitle:
                                          "2. Second Step:\nWhat are your achievements at the beginning of the second stage?",
                                      description: model.challengeDetail
                                              ?.secondStageAchievement ??
                                          "-"),
                                  _titleDescription(
                                      subtitle:
                                          "3. First Step:\nFrom today until reaching the second stage, what goals should you achieve every day (give a daily plan ) ?",
                                      description: model.challengeDetail
                                              ?.firstStageAchievement ??
                                          "-")
                                ],
                              ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              AppToastSave(
                show: _showSaveToast,
                onHide: () {
                  setState(() {
                    _showSaveToast = false;
                  });
                },
              ),
              AppLikeWidget(
                show: _showLikeWidget,
                onHide: () {
                  setState(() {
                    _showLikeWidget = false;
                  });
                },
              ),
            ],
          );
        });
  }

  Widget _titleDescription(
      {String? title, String? subtitle, required String description}) {
    if (description == "-") {
      return Container();
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (title != null)
          Container(
            alignment: Alignment.centerLeft,
            child: AppText(
              title,
              fontSize: 16,
            ),
          ),
        if (subtitle != null)
          const SizedBox(
            height: 5,
          ),
        if (subtitle != null)
          Container(
            alignment: Alignment.centerLeft,
            child: AppText(
              subtitle,
            ),
          ),
        const SizedBox(
          height: 5,
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: AppText(
            description,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget _actionItem(
      {required String icon, required String text, required Function onClick}) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Column(
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: AppImage(icon),
          ),
          const SizedBox(
            height: 4,
          ),
          AppText(
            text,
            fontSize: 9,
          ),
        ],
      ),
    );
  }

  Widget _challengeDetails(ChallengeModel model) {
    return Column(
      children: [
        _titleDescriptionEditable(
            title: "Initiator",
            subtitle: "What made this challenge important to you?",
            description: model.challengeDetail?.initiator,
            onChange: (value) {
              model.challengeDetail?.initiator = value;
            }),
        _titleDescriptionEditable(
            title: "Bottom edge (disadvantage)",
            subtitle:
                "What are the problems before your challenge (individual or social)?",
            description: model.challengeDetail?.disadvantage,
            onChange: (value) {
              model.challengeDetail?.disadvantage = value;
            }),
        _titleDescriptionEditable(
            title: "Upper edge (advantage)",
            subtitle:
                "What are the benefits after your challenge (individual or social)?",
            description: model.challengeDetail?.advantage,
            onChange: (value) {
              model.challengeDetail?.advantage = value;
            }),
        _titleDescriptionEditable(
            title: "Under the influence",
            subtitle: "Who is your challenge useful for?",
            description: model.challengeDetail?.influence,
            onChange: (value) {
              model.challengeDetail?.influence = value;
            }),
        _titleDescriptionEditable(
            title: "Necessities",
            subtitle:
                "What are the most essential things you need for this challenge ?",
            description: model.challengeDetail?.necessities,
            onChange: (value) {
              model.challengeDetail?.necessities = value;
            }),
        _titleDescriptionEditable(
            title: "Contributors",
            subtitle:
                "Who are the most important people who can help you with this challenge ?",
            description: model.challengeDetail?.contributors,
            onChange: (value) {
              model.challengeDetail?.contributors = value;
            }),
        _titleDescriptionEditable(
            title: "Honor",
            description: model.challengeDetail?.honor,
            onChange: (value) {
              model.challengeDetail?.honor = value;
            }),
        _titleDescriptionEditable(
            title: "Why you",
            subtitle: "Why are you the best person for this challenge ?",
            description: model.challengeDetail?.whyYou,
            onChange: (value) {
              model.challengeDetail?.whyYou = value;
            }),
        _titleDescriptionEditable(
            title: "Real Identity (before your challenge)",
            subtitle: "Others knows you now  as (before challenge) ?",
            description: model.challengeDetail?.realIdentity,
            onChange: (value) {
              model.challengeDetail?.realIdentity = value;
            }),
        _titleDescriptionEditable(
            title: "Ideal Identity (After your challenge)",
            subtitle: "Others knows you in future  as (After challenge) ?",
            description: model.challengeDetail?.idealIdentity,
            onChange: (value) {
              model.challengeDetail?.idealIdentity = value;
            }),
        _titleDescriptionEditable(
            title: "Three Steps (back casting)",
            subtitle:
                "1. the End:\nWhat will you achive on the last day of the challenge?",
            description: model.challengeDetail?.endStageAchievement,
            onChange: (value) {
              model.challengeDetail?.endStageAchievement = value;
            }),
        _titleDescriptionEditable(
            subtitle:
                "2. Second Step:\nWhat are your achievements at the beginning of the second stage?",
            description: model.challengeDetail?.secondStageAchievement,
            onChange: (value) {
              model.challengeDetail?.secondStageAchievement = value;
            }),
        _titleDescriptionEditable(
            subtitle:
                "3. First Step:\nFrom today until reaching the second stage, what goals should you achieve every day (give a daily plan ) ?",
            description: model.challengeDetail?.firstStageAchievement,
            onChange: (value) {
              model.challengeDetail?.firstStageAchievement = value;
            }),
        AppButton(
            text: "Update",
            onPressed: () {
              _updateChallengeDetails(model);
            }),
      ],
    );
  }

  Widget _titleDescriptionEditable(
      {String? title,
      String? subtitle,
      required String? description,
      required Function(String value) onChange}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          height: 5,
        ),
        AppBuildChallengeTextField(
            title: title ?? "",
            label: subtitle ?? "",
            value: description,
            onValueChange: (value) {
              onChange(value);
            }),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  void _updateChallengeDetails(ChallengeModel model) {
    AppLoadingDialog.show();
    ChallengeRepository().updateChallengeDetails(
        model,
        AppDataListener(onSuccess: (data) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(
              "Challenge Details successfully updated");
        }, onFailure: (message) {
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            AppLoadingDialog.hide();
            _updateChallengeDetails(model);
          });
        }));
  }
}

class FollowWidget extends StatefulWidget {
  ChallengeModel challengeModel;

  FollowWidget({Key? key, required this.challengeModel}) : super(key: key);

  @override
  State<FollowWidget> createState() => _FollowWidgetState();
}

class _FollowWidgetState extends State<FollowWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(children: [
      InkWell(
        onTap: () {
          widget.challengeModel.follow =
              !(widget.challengeModel.follow ?? false);
          if (widget.challengeModel.follow == true) {
            widget.challengeModel.followers =
                (widget.challengeModel.followers ?? 0) + 1;
          } else {
            widget.challengeModel.followers =
                (widget.challengeModel.followers ?? 0) - 1;
          }
          setState(() {});
          ChallengeRepository().challengeFollowToggle(
              widget.challengeModel.id ?? 0,
              AppDataListener(
                  onSuccess: (response) {}, onFailure: (message) {}));
        },
        child: Row(children: [
          AppImage(
            (widget.challengeModel.follow ?? false)
                ? "ic_follow_1.png"
                : "ic_unfollow_1.png",
            width: 15,
          ),
          const SizedBox(
            width: 10,
          ),
        ]),
      ),
      InkWell(
        onTap: () {
          Get.to(FollowersPage.challenge(id: widget.challengeModel.id!));
        },
        child: AppText(
          "${widget.challengeModel.followers ?? 0} Follower${(widget.challengeModel.followers ?? 0) > 1 ? "s" : ""}",
          fontSize: 10,
          fontWeight: FontWeight.w400,
        ),
      ),
    ]);
  }
}
