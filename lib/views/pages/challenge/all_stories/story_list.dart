import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/story_model.dart';
import 'package:challenger/views/pages/challenge/story/story_item.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:flutter/material.dart';

class StoryList extends StatefulWidget {
  String stage;
  ChallengeModel challengeModel;

  StoryList({Key? key, required this.stage, required this.challengeModel})
      : super(key: key);

  @override
  State<StoryList> createState() => _StoryListState();
}

class _StoryListState extends State<StoryList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Expanded(
            child: AppPaginatedListView<DailyReportModel>(
          requestParams: "type=done",
          url:
              "/challenge/${widget.challengeModel.id}/${widget.stage.toLowerCase().replaceAll("_", "-")}/daily-reports",
          onFetchData: (response) {
            return response.map<DailyReportModel>((e) => DailyReportModel.fromJson(e)).toList();
          },
          renderItem: (item) => Container(
            margin: const EdgeInsets.symmetric(vertical: 15),
            child: StoryItem(dailyReportModel: item),
          ),
        )),
      ],
    );
  }
}
