import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/views/pages/challenge/all_stories/story_grid.dart';
import 'package:challenger/views/pages/challenge/all_stories/story_list.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class AllStories extends StatefulWidget {
  String stage;
  ChallengeModel challengeModel;

  AllStories({Key? key, required this.stage, required this.challengeModel})
      : super(key: key);

  @override
  State<AllStories> createState() => _AllStoriesState();
}

class _AllStoriesState extends State<AllStories> {
  bool _isList = true;

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "${widget.challengeModel.title} Challenge",
        logoWithTitle: true,
        body: Stack(
          children: [
            _isList
                ? StoryList(
                    stage: widget.stage,
                    challengeModel: widget.challengeModel,
                  )
                : StoryGrid(
                    stage: widget.stage,
                    challengeModel: widget.challengeModel,
                  ),
            Container(
              margin: const EdgeInsets.all(16),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: 120,
                  height: 45,
                  decoration: BoxDecoration(
                      color: const Color(0xff9b9b9e),
                      borderRadius: BorderRadius.circular(100)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            _isList = true;
                          });
                        },
                        child: AppImage(
                          "ic_video2.png",
                          color: _isList ? AppColors.primary : Colors.black45,
                          width: 20,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _isList = false;
                          });
                        },
                        child: AppImage(
                          "ic_grid.png",
                          color: !_isList ? AppColors.primary : Colors.black45,
                          width: 22,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
