import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/story_model.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/daily_demo/daily_demo_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StoryGrid extends StatefulWidget {
  String stage;
  ChallengeModel challengeModel;
  StoryGrid({Key? key, required this.stage, required this.challengeModel}) : super(key: key);

  @override
  State<StoryGrid> createState() => _StoryGridState();
}

class _StoryGridState extends State<StoryGrid> {
  List<DailyReportModel> stories = [];

  void updateView() async {
    await Future.delayed(const Duration(milliseconds: 100));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        AppMagicFutureBuilder(
            url: "/challenge/${widget.challengeModel.id}/${widget.stage.toLowerCase().replaceAll("_", "-")}/daily-reports?type=done",
            onSuccess: (response) {
              print(response);
              if (stories.isEmpty) {
                for (var i = 0; i < response.length; i++) {
                  stories.add(DailyReportModel.fromJson(response[i]));
                }
                updateView();
              }
              return Container();
            }),
        Expanded(
            child: GridView.count(
          mainAxisSpacing: 5,
          crossAxisCount: 3,
          crossAxisSpacing: 15,
          childAspectRatio: 2 / 2,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          children: List.generate(stories.length, (index) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                AppText(Utils.formatCalendarDatePickerForDailyReport(DateTime.parse(stories[index].date!)), color: _stateColor(stories[index]), fontSize: 10,),
                InkWell(onTap: (){
                  Get.to(DailyDemoPage(dailyReportModel: stories[index]));
                  // Get.to(VideoPlayerPage.playDailyReportVideo(stories[index]));
                },child: Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(6), border: Border.all(color: _stateColor(stories[index],),width: 2)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: Image.network(
                      stories[index].dailyVideoCoverUrl!,
                      height: 90,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),),
              ],
            );
          }),
        )),
      ],
    );
  }

  Color _stateColor(DailyReportModel dailyReportModel) {
    switch (dailyReportModel.result) {
      case "NOT_REACHED":
        return const Color(0xffE66969);
      case "REACHED":
        return const Color(0xff78db76);
      case "REACHED_BUT_WITH_PROBLEM":
        return const Color(0xffDEC557);
      default:
        return const Color(0xffE66969);
    }
  }

  List<Color> _gradientColors(StoryModel storyModel) {
    switch (storyModel.state) {
      case StoryState.red:
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
      case StoryState.green:
        return [const Color(0xff9bd49a), const Color(0xffc5f6c6)];
      case StoryState.yellow:
        return [const Color(0xffdec557), const Color(0xfff6efc5)];
      default:
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
    }
  }
}
