import 'dart:io';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/daily_report_reporitory.dart';
import 'package:challenger/data/repository/file_reporitory.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_error_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/stage/stage_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_build_challenge_text_field.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class BuildStoryPage extends StatefulWidget {
  DailyReportModel dailyReportModel;
  ChallengeModel challengeModel;
  bool newVideo = true;
  bool edit = false;

  BuildStoryPage(
      {Key? key, required this.dailyReportModel, required this.challengeModel})
      : super(key: key);

  BuildStoryPage.edit(
      {required this.dailyReportModel, required this.challengeModel}) {
    edit = true;
    newVideo = false;
  }

  @override
  State<BuildStoryPage> createState() => _BuildStoryPageState();
}

class _BuildStoryPageState extends State<BuildStoryPage> {
  bool _newVideo = true;
  String? _videoPath;
  int? videoId;
  String? _result;
  String _selectedCondition = "Select todays condition";
  bool _selectReason = false;
  Image? _thumbnailImage;
  String successText = "";
  String failureText = "";

  @override
  void initState() {
    super.initState();
    _initEdit();
  }

  void _initEdit() async {
    if (widget.edit) {
      successText = widget.dailyReportModel.successText ?? "";
      failureText = widget.dailyReportModel.failureText ?? "";
      _selectReason = true;
      _result = widget.dailyReportModel.result ?? "";
      if (_result == "REACHED") {
        _selectedCondition = "I had no problem achiving my goals today";
      } else if (_result == "NOT_REACHED") {
        _selectedCondition =
            "I didn’t achieve my goals today and i had the problem of...";
      } else if (_result == "REACHED_BUT_WITH_PROBLEM") {
        _selectedCondition =
            "I reached my goal a little bit and i had some problems...";
      }

      _newVideo = widget.newVideo;

      setState(() {});
    }
  }

  Widget _action() {
    return AppButton(
      width: double.maxFinite,
      text: widget.edit ? "Edit" : "Save",
      onPressed: () {
        uploadVideo();
      },
    );
  }

  void uploadVideo() {
    if (_videoPath == null && !widget.edit) {
      AppMessage.showDialogMessage(
          "You should send a video for your daily report");
      return;
    }

    if (_result == null && !widget.edit) {
      AppMessage.showDialogMessage(
          "You should select condition for your daily report");
      return;
    }
    uploadVideoFile();
  }

  void uploadVideoFile() {
    if (_videoPath == null && widget.edit) {
      editDailyReport();
      return;
    }
    AppLoadingDialog.show();
    FileRepository().uploadVideo(
        _videoPath ?? "",
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          videoId = response['id'];
          if (widget.edit) {
            editDailyReport();
          } else {
            uploadDailyReport();
          }
        }, onFailure: (message) {
          AppLoadingDialog.hide();
        }));
  }

  void uploadDailyReport() {
    AppLoadingDialog.show();
    DailyReportRepository().setDailyReport(
        widget.dailyReportModel.id!,
        widget.dailyReportModel.challengeId!,
        videoId!,
        widget.dailyReportModel.stage!,
        _result!,
        successText,
        failureText,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(response, cancelable: false,
              onOkAction: () {
                Get.back();
                Get.back();
                Get.to(StagePage(
                  challengeModel: widget.challengeModel,
                  stage: widget.dailyReportModel.stage!,
                ));
              });
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            uploadDailyReport();
          });
        }));
  }

  void editDailyReport() {
    AppLoadingDialog.show();
    DailyReportRepository().editAfterDoneDailyReport(
        widget.dailyReportModel.id!,
        widget.dailyReportModel.challengeId!,
        videoId,
        widget.dailyReportModel.stage!,
        _result!,
        successText,
        failureText,
        AppDataListener(onSuccess: (response) {
          AppLoadingDialog.hide();
          AppMessage.showDialogMessage(response, cancelable: false,
              onOkAction: () {
                Get.back();
                Get.back();
                Get.to(StagePage(
                  challengeModel: widget.challengeModel,
                  stage: widget.dailyReportModel.stage!,
                ));
              });
        }, onFailure: (message) {
          AppLoadingDialog.hide();
          AppErrorDialog.showErrorDialogWithRetry(message, onRetry: () {
            uploadDailyReport();
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title:
            "${Utils.formatCalendarDatePickerForDailyReport(DateTime.parse(widget.dailyReportModel.date!))} Daily Report",
        logoWithTitle: true,
        titleFontSize: 16,
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: AppText(
                    "Daily report video",
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: const Color(0xff1D1D1D),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: AppText(
                    "I must do action 1 to reach goal 1and then action 2 to reach...",
                    fontSize: 12,
                    color: const Color(0xff1D1D1D),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                if (widget.edit && !_newVideo)
                  AppBorderContainer(
                    height: 180,
                    width: double.maxFinite,
                    borderRadius: 24,
                    padding: const EdgeInsets.all(1),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(24),
                      child: Container(
                        color: Colors.black,
                        child: Stack(
                          children: [
                            Container(
                              width: double.maxFinite,
                              child: Image.network(widget.dailyReportModel.dailyVideoCoverUrl ?? ""),
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(VideoPlayerPage.playDailyReportVideo(widget.dailyReportModel));
                              },
                              child: const Center(
                                  child: Icon(
                                    Icons.play_circle,
                                    color: Colors.white,
                                    size: 60,
                                  )),
                            ),
                            Container(
                                  alignment: Alignment.bottomCenter,
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: AppButton(width: 150,height: 35, text: "Edit video",onPressed: (){
                                    setState(() {
                                      _newVideo = true;
                                    });
                                  },),
                            )
                          ],
                        ),
                      ),
                    ),

                  ),
                if (_newVideo)
                  AppBorderContainer(
                    height: 180,
                    width: double.maxFinite,
                    borderRadius: 24,
                    padding: const EdgeInsets.all(1),
                    child: InkWell(
                      onTap: () {
                        AppMediaPicker()
                            .pickVideoFromGallery((pickedFile, thumbnail) {
                          setState(() {
                            _videoPath = pickedFile;
                            _thumbnailImage = thumbnail;
                          });
                        });
                      },
                      child: _videoPath == null
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(
                                  height: 35,
                                ),
                                AppImage(
                                  "ic_video.png",
                                  width: 52,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppText(
                                  "Select your daily video",
                                  color: const Color(0xff484848),
                                  fontWeight: FontWeight.w400,
                                ),
                              ],
                            )
                          : ClipRRect(
                              borderRadius: BorderRadius.circular(24),
                              child: Container(
                                color: Colors.black,
                                child: Stack(
                                  children: [
                                    Container(
                                      width: double.maxFinite,
                                      child: _thumbnailImage!,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Get.to(VideoPlayerPage(
                                          path: _videoPath,
                                        ));
                                      },
                                      child: const Center(
                                          child: Icon(
                                        Icons.play_circle,
                                        color: Colors.white,
                                        size: 60,
                                      )),
                                    )
                                  ],
                                ),
                              ),
                            ),
                    ),
                  ),
                const SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    AppDialog(
                        body: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: AppColors.borderColor)),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    _selectedCondition =
                                        "I had no problem achiving my goals today";
                                    _selectReason = true;
                                    _result = "REACHED";
                                  });
                                  Get.back();
                                },
                                child: Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  child: AppText(
                                      "I had no problem achiving my goals today"),
                                ),
                              ),
                              Container(
                                height: 1,
                                color: AppColors.borderColor,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    _selectedCondition =
                                        "I didn’t achieve my goals today and i had the problem of...";
                                    _selectReason = true;
                                    _result = "NOT_REACHED";
                                  });
                                  Get.back();
                                },
                                child: Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  child: AppText(
                                      "I didn’t achieve my goals today and i had the problem of..."),
                                ),
                              ),
                              Container(
                                height: 1,
                                color: AppColors.borderColor,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    _selectedCondition =
                                        "I reached my goal a little bit and i had some problems...";
                                    _selectReason = true;
                                    _result = "REACHED_BUT_WITH_PROBLEM";
                                  });
                                  Get.back();
                                },
                                child: Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  child: AppText(
                                      "I reached my goal a little bit and i had some problems..."),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ));
                  },
                  child: AppBorderContainer(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: Row(
                        children: [
                          Expanded(
                            child: AppText(
                              _selectedCondition,
                              maxLines: 1,
                              overFlow: TextOverflow.ellipsis,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          const Icon(
                            Icons.arrow_drop_down,
                            color: Colors.black,
                          )
                        ],
                      )),
                ),
                const SizedBox(
                  height: 10,
                ),
                if (_selectReason)
                  AppBuildChallengeTextField(
                      title: "",
                      label: "What goals did you reach ? ",
                      hintText: "I reached ...",
                      value: successText,
                      onValueChange: (value) {
                        successText = value;
                      }),
                if (_selectReason)
                  AppBuildChallengeTextField(
                      title: "",
                      label: "What was your problems?  ",
                      hintText: "The problem is that ...",
                      value: failureText,
                      onValueChange: (value) {
                        failureText = value;
                      }),
                const SizedBox(
                  height: 20,
                ),
                _action(),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ));
  }
}
