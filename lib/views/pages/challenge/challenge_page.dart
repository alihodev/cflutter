import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/repository/challgenge_reporitory.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_loading_dialog.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/pages/challenge/views/challengers/challengers_view.dart';
import 'package:challenger/views/pages/challenge/views/storyline/storyline_view.dart';
import 'package:challenger/views/pages/challenge/views/summary/summary_view.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChallengePage extends StatefulWidget {
  int initialTabIndex;
  int id;
  String title;
  ChallengePage({Key? key, required this.initialTabIndex, required this.id, required this.title}) : super(key: key);

  @override
  State<ChallengePage> createState() => _ChallengePageState();
}

class _ChallengePageState extends State<ChallengePage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: 3, vsync: this, initialIndex: widget.initialTabIndex);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.actionBarColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 8,
                    offset: const Offset(0, 4),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(
                        left: 7, right: 7, top: 10, bottom: 10),
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Builder(builder: (BuildContext context) {
                          return InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              width: 50,
                              height: 50,
                              child: AppImage(
                                "ic_arrow_left.png",
                                color: AppColors.h1TextColor,
                              ),
                            ),
                          );
                        }),
                        Column(
                          children: [
                            AppImage(
                              "logo.png",
                              height: 20,
                            ),
                            FittedBox(
                              fit: BoxFit.fill,
                              child: AppText("${widget.title} Challenge", fontSize: 11, fontWeight: FontWeight.bold,),
                            ),
                          ],
                        ),
                        InkWell(
                            onTap: () {
                              AppDialog.showReportDialog(
                                  title: "Report Challenge",
                                  description: "Why do you want to report this challenge?", onAction: (String description){
                                AppLoadingDialog.show();
                                print(description);
                                ChallengeRepository().reportChallenge(widget.id,description, AppDataListener(onSuccess: (message){
                                  AppLoadingDialog.hide();
                                  AppMessage.showDialogMessage(message);
                                }, onFailure: (message){
                                  AppLoadingDialog.hide();
                                  AppMessage.showDialogMessage(message);
                                }));
                              });
                            },
                            child: SizedBox(
                              width: 50,
                              child: Icon(Icons.report_gmailerrorred),
                            )),
                        // Container(
                        //   padding: const EdgeInsets.all(10),
                        //   width: 50,
                        //   height: 50,
                        // ),
                      ],
                    ),
                  ),
                  PreferredSize(
                    preferredSize: const Size.fromHeight(kToolbarHeight),
                    child: TabBar(
                      controller: _tabController,
                      labelColor: AppColors.primary,
                      labelStyle: const TextStyle(
                        fontSize: 16,
                      ),
                      unselectedLabelColor: AppColors.unSelectedTabColor,
                      indicatorColor: AppColors.primary,
                      indicatorSize: TabBarIndicatorSize.label,
                      tabs: const [
                        Tab(text: 'Summary'),
                        Tab(text: 'Challengers'),
                        Tab(text: 'Storyline'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  SummaryView(id: widget.id,),
                  ChallengersView(id: widget.id,),
                  StorylineView(id: widget.id,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
