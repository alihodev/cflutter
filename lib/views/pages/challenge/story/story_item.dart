import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_share.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/daily_demo/daily_demo_page.dart';
import 'package:challenger/views/pages/video_player/video_player_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StoryItem extends StatefulWidget {
  DailyReportModel dailyReportModel;

  StoryItem({Key? key, required this.dailyReportModel}) : super(key: key);

  @override
  State<StoryItem> createState() => _StoryItemState();
}

class _StoryItemState extends State<StoryItem> {
  final PageController _controller = PageController(
    viewportFraction: 0.8,
    initialPage: 1
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AppText(
          Utils.formatCalendarDatePickerForDailyReport(DateTime.parse(widget.dailyReportModel.date!)),
          fontSize: 18,
          fontWeight: FontWeight.w700,
          color: _stateColor(),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          padding: const EdgeInsets.all(0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(26),
              border: Border.all(color: _stateColor(), width: 2)),
          child: Stack(
            children: [
              InkWell(
                onTap: () {
                  Get.to(DailyDemoPage(dailyReportModel: widget.dailyReportModel));
                  // Get.to(VideoPlayerPage.playDailyReportVideo(widget.dailyReportModel));
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(24),
                  child: Image.network(
                    widget.dailyReportModel.dailyVideoCoverUrl ?? "",
                    height: 213,
                    width: double.maxFinite,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: 213,
                padding: EdgeInsets.only(bottom: 20),
                alignment: Alignment.bottomCenter,
                child:
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppButton(
                  text: "Share",
                  width: 128,
                  height: 42,
                  icon: AppImage(
                    "ic_send.png",
                    width: 20,
                    height: 20,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    AppShare.shareDailyReport(widget.dailyReportModel);
                  }),
                // if (AppStorage.getInstance().getStaticUser().id == widget.dailyReportModel.challenger?.id)
                //   Container(
                //     margin: EdgeInsets.only(left: 10),
                //     child: AppButton(
                //       text: "Edit",
                //       width: 128,
                //       height: 42,
                //       icon: AppImage(
                //         "ic_edit.png",
                //         width: 20,
                //         height: 20,
                //         color: Colors.white,
                //       ),
                //       onPressed: () {
                //
                //       }),)
              ],),)
            ],
          ),
        ),
        const SizedBox(height: 15,),
        Container(
            height: 160,
            child: PageView.builder(
              controller: _controller,
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index){
              return Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(12),gradient: LinearGradient(colors: _gradientColors())),
                  child:  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    AppText(_getTitle(index), fontWeight: FontWeight.w700,),
                    AppText(_getDescription(index, widget.dailyReportModel),fontSize: 12, fontWeight: FontWeight.w400,),
                      const SizedBox(height: 10,),
                      const Spacer(),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //   AppText("🖥️ 50+ videos", fontSize: 10,),
                      //   AppText("⏰️ 12 hours", fontSize: 10,),
                      //   AppText("👨🏻‍‍️ 509+ students", fontSize: 10,),
                      // ],)
                  ],));
            }))
      ],
    );
  }

  String _getTitle(int index) {
    switch (index) {
      case 1: return "Day Report";
      case 2: return "Problems";
      default : return "Day Target";
    }
  }

  String _getDescription(int index, DailyReportModel dailyReportModel) {
    switch (index) {
      case 1: return dailyReportModel.successText!;
      case 2: return dailyReportModel.failureText!;
      default : return dailyReportModel.aim!;
    }
  }

  Color _stateColor() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return const Color(0xffE66969);
      case "REACHED":
        return const Color(0xff78db76);
      case "REACHED_BUT_WITH_PROBLEM":
        return const Color(0xffDEC557);
      default:
        return const Color(0xffE66969);
    }
  }

  List<Color> _gradientColors() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
      case "REACHED":
        return [const Color(0xff9bd49a), const Color(0xffc5f6c6)];
      case "REACHED_BUT_WITH_PROBLEM":
        return [const Color(0xffdec557), const Color(0xfff6efc5)];
      default:
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
    }
  }
}
