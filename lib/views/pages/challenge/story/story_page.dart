import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/story_model.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/challenge/story/story_item.dart';
import 'package:challenger/views/widgets/app_magic_future_builder.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/cupertino.dart';

class StoryPage extends StatefulWidget {
  DailyReportModel dailyReportModel;
  StoryPage({Key? key, required this.dailyReportModel}) : super(key: key);

  @override
  State<StoryPage> createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> {
  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: Utils.formatCalendarDatePickerForDailyReport(DateTime.parse(widget.dailyReportModel.date!)),
        logoWithTitle: true,
        titleFontSize: 24,
        body: AppMagicFutureBuilder(
          url: "/daily-report/${widget.dailyReportModel.id}/",
          onSuccess: (response) {
            return Center(
              child: StoryItem(
                dailyReportModel: DailyReportModel.fromJson(response),
              ),
            );
          },
        ));
  }
}
