import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/data/utils/request_body_builder.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/challenge/stage/stage_page.dart';
import 'package:get/get.dart';
import 'package:challenger/views/dialogs/app_message.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class ActiveStoryPage extends StatefulWidget {
  ChallengeModel challengeModel;
  DailyReportModel dailyReportModel;
  bool edit = false;

  ActiveStoryPage(
      {Key? key, required this.challengeModel, required this.dailyReportModel})
      : super(key: key);
  ActiveStoryPage.edit({Key? key, required this.challengeModel, required this.dailyReportModel}) {
    edit = true;
  }

  @override
  State<ActiveStoryPage> createState() => _ActiveStoryPageState();
}

class _ActiveStoryPageState extends State<ActiveStoryPage> {
  final _descriptionTextEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.edit) {
      ;
      _descriptionTextEditingController.text =widget.dailyReportModel.aim ?? "";
    }
  }
  Widget _action() {
    return AppMagicButton(
      text: widget.edit ? "Edit" : "Active",
      postUrl: "/daily-report/${widget.dailyReportModel.id}/${widget.edit ? "edit" : "enable"}",
      getData: () {
        return RequestBodyBuilder()
            .addField("challengeId", widget.challengeModel.id)
            .addField("aim", _descriptionTextEditingController.text)
            .addField("stage", widget.challengeModel.stage)
            .build();
      },
      onDone: (response) {
        AppMessage.showDialogMessage(response, cancelable: false, onOkAction: (){
          Get.back();
          Get.back();
          Get.to(StagePage(challengeModel: widget.challengeModel, stage: widget.dailyReportModel.stage!,));
        });
      },
      width: double.maxFinite,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "Active Story",
        logoWithTitle: true,
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                AppText.body("Insert story target for this day"),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Story description",
                  keyboardType: TextInputType.multiline,
                  height: 104,
                  textEditingController: _descriptionTextEditingController,
                  borderRadius: 24,
                ),
                const SizedBox(
                  height: 20,
                ),
                _action(),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ));
  }
}
