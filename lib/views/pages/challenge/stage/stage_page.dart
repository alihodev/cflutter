import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/views/pages/challenge/all_stories/all_stories.dart';
import 'package:challenger/views/pages/challenge/stage/daily_report_item.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StagePage extends StatefulWidget {
  ChallengeModel challengeModel;
  String stage;

  StagePage({Key? key, required this.challengeModel, required this.stage})
      : super(key: key);

  @override
  State<StagePage> createState() => _StagePageState();
}

class _StagePageState extends State<StagePage> {
  int _stage = 0;
  List<int> _stageList = [];

  @override
  void initState() {
    super.initState();
    _stage = _getCurrentStage(widget.challengeModel);
    for (int i = 0; i < _stage; i++) {
      _stageList.add(i);
    }
    _stageList = _stageList.reversed.toList();
  }

  int _getCurrentStage(ChallengeModel challengeModel) {
    switch (challengeModel.stage) {
      case "OPENING":
        return 1;
      case "FIRST_STAGE":
        return 2;
      case "SECOND_STAGE":
        return 3;
      case "END_STAGE":
        return 4;
      default:
        return 0;
    }
  }

  String _getCurrentStageName() {
    switch (widget.stage) {
      case "OPENING":
        return "Opening";
      case "FIRST_STAGE":
        return "First Stage";
      case "SECOND_STAGE":
        return "Second Stage";
      case "END_STAGE":
        return "End Stage";
      default:
        return "Build";
    }
  }

  String _getCurrentStageId() {
    switch (widget.stage) {
      case "OPENING":
        return "0";
      case "FIRST_STAGE":
        return "1";
      case "SECOND_STAGE":
        return "2";
      case "END_STAGE":
        return "3";
      default:
        return "0";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "${widget.challengeModel.title} Challenge",
        logoWithTitle: true,
        body: Stack(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  // Container(
                  //   margin: EdgeInsets.only(bottom: _stage > 0 ? 10 : 0),
                  //   child: Row(
                  //     children: [
                  //       ..._stageList.map(
                  //         (e) => Container(
                  //           width: 24,
                  //           height: 24,
                  //           margin: EdgeInsets.only(top: 6, right: 10),
                  //           alignment: Alignment.center,
                  //           decoration: BoxDecoration(
                  //               color: AppColors.primary,
                  //               borderRadius: BorderRadius.circular(5)),
                  //           child: AppText(
                  //             "${e}",
                  //             fontSize: 10,
                  //             color: Colors.white,
                  //             fontWeight: FontWeight.w600,
                  //           ),
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 24,
                        height: 24,
                        margin: EdgeInsets.only(top: 6),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: AppColors.primary,
                            borderRadius: BorderRadius.circular(5)),
                        child: AppText(
                          _getCurrentStageId(),
                          fontSize: 10,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          AppText(
                            _getCurrentStageName(),
                            color: AppColors.primary,
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                          // AppText(
                          //   "When an unknown printer took a galley of type and scrambled it to make a t specimen",
                          //   fontSize: 12,
                          // )
                        ],
                      )),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Expanded(
                      child: AppPaginatedListView<DailyReportModel>(
                    url:
                        "/challenge/${widget.challengeModel.id}/${widget.stage.toLowerCase().replaceAll("_", "-")}/daily-reports",
                    onFetchData: (response) {
                      return response
                          .map<DailyReportModel>(
                              (e) => DailyReportModel.fromJson(e))
                          .toList();
                    },
                    renderItem: (item) {
                      return DailyReportItem(
                        dailyReportModel: item,
                        challengeModel: widget.challengeModel,
                      );
                    },
                  ))
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: const EdgeInsets.only(bottom: 16),
                child: AppButton(
                  width: 120,
                  height: 45,
                  fontSize: 15,
                  text: "See All",
                  onPressed: () {
                    Get.to(AllStories(
                      challengeModel: widget.challengeModel,
                      stage: widget.stage,
                    ));
                  },
                ),
              ),
            )
          ],
        ));
  }
}
