import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/pages/challenge/active_story/active_story_page.dart';
import 'package:challenger/views/pages/challenge/build_story/build_story_page.dart';
import 'package:challenger/views/pages/challenge/story/story_page.dart';
import 'package:challenger/views/widgets/app_image.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DailyReportItem extends StatefulWidget {
  DailyReportModel dailyReportModel;
  ChallengeModel challengeModel;

  DailyReportItem(
      {Key? key, required this.dailyReportModel, required this.challengeModel})
      : super(key: key);

  @override
  State<DailyReportItem> createState() => _DailyReportItemState();
}

class _DailyReportItemState extends State<DailyReportItem> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _index(),
        const SizedBox(
          width: 10,
        ),
        _getState()
      ],
    );
  }

  Widget _getState() {
    print(widget.dailyReportModel.status);
    if (widget.dailyReportModel.status != "NOT_SET") {
      return _state(
          title: Utils.formatCalendarDatePickerForDailyReport(
              DateTime.parse(widget.dailyReportModel.date!)),
          description: widget.dailyReportModel.aim ?? "");
    }
    return _inactiveItem();
  }

  Widget _inactiveItem() {
    return Expanded(
      child: InkWell(
        onTap: () {
          if (AppStorage.getInstance().getStaticUser().id ==
              widget.challengeModel.challenger?.id)
            Get.to(ActiveStoryPage(
              challengeModel: widget.challengeModel,
              dailyReportModel: widget.dailyReportModel,
            ));
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: const Color(0xfff4f4f4),
          ),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                    color: const Color(0xffaaaaaa),
                    borderRadius: BorderRadius.circular(8)),
                child: AppImage(
                  "ic_poker.png",
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (AppStorage.getInstance().getStaticUser().id ==
                        widget.challengeModel.challenger?.id)
                      AppText(
                        (widget.challengeModel.coChallenge ?? true)
                            ? "Wait for main challenger to setup daily targets or set your personal targets"
                            : "Click to active this daily report",
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      ),
                    if (AppStorage.getInstance().getStaticUser().id !=
                        widget.challengeModel.challenger?.id)
                      AppText(
                        "Challenger has not yet set any targets for this day",
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      ),
                  ],
                ),
              ),
              const Icon(
                Icons.arrow_forward_ios_rounded,
                size: 15,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _index() {
    if (widget.dailyReportModel.status == "DONE") {
      return Container(
        width: 32,
        height: 32,
        margin: const EdgeInsets.only(bottom: 15, right: 8, left: 8),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: const Color(0xff2e384f),
            border: Border.all(color: AppColors.primary, width: 2),
            borderRadius: BorderRadius.circular(50)),
        child: AppText(
          "${widget.dailyReportModel.index! + 1}",
          color: Colors.white,
        ),
      );
    } else if (widget.dailyReportModel.status == "DOING") {
      return Container(
        width: 48,
        height: 48,
        margin: const EdgeInsets.only(bottom: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.primary, width: 2),
            borderRadius: BorderRadius.circular(50)),
        child: AppText(
          "${widget.dailyReportModel.index! + 1}",
          color: AppColors.primary,
        ),
      );
    } else {
      return Container(
        width: 32,
        height: 32,
        margin: const EdgeInsets.only(bottom: 15, right: 8, left: 8),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(color: const Color(0xffaaaaaa), width: 2),
            borderRadius: BorderRadius.circular(50)),
        child: AppText(
          "${widget.dailyReportModel.index! + 1}",
          color: Colors.black,
        ),
      );
    }
  }

  Widget _state({required String title, required String description}) {
    if (widget.dailyReportModel.status == "EXPIRED") {
      return Expanded(
        child: Container(
          margin: const EdgeInsets.only(bottom: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: const Color(0xfff4f4f4),
          ),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                    color: const Color(0xffaaaaaa),
                    borderRadius: BorderRadius.circular(8)),
                child: AppImage(
                  "ic_poker.png",
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText(
                      title,
                      fontWeight: FontWeight.w700,
                      fontSize: 11,
                    ),
                    AppText(
                      description,
                      maxLines: 2,
                      overFlow: TextOverflow.ellipsis,
                      fontSize: 10,
                    ),
                  ],
                ),
              ),
              AppText(
                "Expired!",
                fontSize: 12,
                color: Colors.red,
              ),
            ],
          ),
        ),
      );
    } else if (widget.dailyReportModel.status == "TODO") {
      return Expanded(
        child: Container(
          margin: const EdgeInsets.only(bottom: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: const Color(0xfff4f4f4),
          ),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                    color: const Color(0xffaaaaaa),
                    borderRadius: BorderRadius.circular(8)),
                child: AppImage(
                  "ic_poker.png",
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText(
                      title,
                      fontWeight: FontWeight.w700,
                      fontSize: 11,
                    ),
                    AppText(
                      description,
                      maxLines: 2,
                      overFlow: TextOverflow.ellipsis,
                      fontSize: 10,
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Get.to(ActiveStoryPage.edit(
                    challengeModel: widget.challengeModel,
                    dailyReportModel: widget.dailyReportModel,
                  ));
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)),
                  child: const Icon(
                    Icons.edit,
                    size: 15,
                    color: AppColors.primary,
                  ),
                ),
              ),
              // const Icon(
              //   Icons.arrow_forward_ios_rounded,
              //   size: 15,
              // )
            ],
          ),
        ),
      );
    } else if (widget.dailyReportModel.status == "DOING") {
      return Expanded(
        child: Container(
          margin: const EdgeInsets.only(bottom: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              boxShadow: const [
                BoxShadow(color: Colors.black12, offset: Offset(0, 4))
              ],
              color: const Color(0xdbf3f4ff)),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                    color: AppColors.primary,
                    borderRadius: BorderRadius.circular(8)),
                child: AppImage(
                  "ic_poker.png",
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText(
                        title,
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      ),
                      AppText(
                        description,
                        maxLines: 2,
                        overFlow: TextOverflow.ellipsis,
                        fontSize: 10,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {
                  Get.to(ActiveStoryPage.edit(
                    challengeModel: widget.challengeModel,
                    dailyReportModel: widget.dailyReportModel,
                  ));
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)),
                  child: const Icon(
                    Icons.edit,
                    size: 15,
                    color: AppColors.primary,
                  ),
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              InkWell(
                onTap: () {
                  Get.to(BuildStoryPage(
                      dailyReportModel: widget.dailyReportModel,
                      challengeModel: widget.challengeModel));
                },
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(50)),
                  child: const Icon(
                    Icons.add,
                    size: 15,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Expanded(
      child: InkWell(
        onTap: () {
          Get.to(StoryPage(
            dailyReportModel: widget.dailyReportModel,
          ));
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: _gradientColors())),
          child: Row(
            children: [
              Container(
                width: 48,
                height: 48,
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                    color: _stateColor(),
                    borderRadius: BorderRadius.circular(8)),
                child: AppImage(
                  _stateImage(),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppText(
                        title,
                        fontWeight: FontWeight.w700,
                        fontSize: 11,
                      ),
                      AppText(
                        description,
                        maxLines: 2,
                        overFlow: TextOverflow.ellipsis,
                        fontSize: 10,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              if (widget.dailyReportModel.editable ?? false)
                InkWell(
                  onTap: () {
                    Get.to(BuildStoryPage.edit(
                      challengeModel: widget.challengeModel,
                      dailyReportModel: widget.dailyReportModel,
                    ));
                  },
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                    child: const Icon(
                      Icons.edit,
                      size: 15,
                      color: AppColors.primary,
                    ),
                  ),
                ),
              const SizedBox(
                width: 5,
              ),
              const Icon(
                Icons.arrow_forward_ios_rounded,
                size: 15,
              )
            ],
          ),
        ),
      ),
    );
  }

  String _stateImage() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return "ic_sad.png";
      case "REACHED":
        return "ic_happy.png";
      case "REACHED_BUT_WITH_PROBLEM":
        return "ic_alert.png";
      default:
        return "ic_sad.png";
    }
  }

  Color _stateColor() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return const Color(0xffE66969);
      case "REACHED":
        return const Color(0xff78db76);
      case "REACHED_BUT_WITH_PROBLEM":
        return const Color(0xffDEC557);
      default:
        return const Color(0xffE66969);
    }
  }

  List<Color> _gradientColors() {
    switch (widget.dailyReportModel.result) {
      case "NOT_REACHED":
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
      case "REACHED":
        return [const Color(0xff9bd49a), const Color(0xffc5f6c6)];
      case "REACHED_BUT_WITH_PROBLEM":
        return [const Color(0xffdec557), const Color(0xfff6efc5)];
      default:
        return [const Color(0xffe17474), const Color(0xfff6c5c5)];
    }
  }
}
