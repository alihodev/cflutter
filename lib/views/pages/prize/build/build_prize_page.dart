import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/user_model.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/dialogs/app_multi_date_picker.dart';
import 'package:challenger/views/pages/prize/build/attach_users.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BuildPrizePage extends StatefulWidget {
  BuildPrizePage({Key? key}) : super(key: key);

  @override
  State<BuildPrizePage> createState() => _BuildDebateState();
}

class _BuildDebateState extends State<BuildPrizePage> {
  DateTime startDateTime = DateTime.now();
  DateTime endDateTime = DateTime.now();
  String _selectedValue = "everyOne";
  List<UserModel> userList = [];

  Widget _action() {
    return AppMagicButton(
      text: "Build",
      postUrl: "/google.com",
      getData: () {
        return "";
      },
      onDone: (response) {},
      width: double.maxFinite,
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "Build Prize",
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                AppText.body(
                    "The author, vice chairman of Ogilvy, shares why what’s irrational often works better than what’s The author, vice chairman of Ogilvy, shares why what’s irrational often works better than what’s "),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Prize title",
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Prize description",
                  keyboardType: TextInputType.multiline,
                  height: 104,
                  borderRadius: 24,
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Prize amount (USD)",
                ),
                const SizedBox(
                  height: 5,
                ),
                const SizedBox(
                  height: 20,
                ),
                // AppMultiDatePicker(),
                AppBorderContainer(
                  child: InkWell(
                      onTap: () {
                        AppMultiDatePickerDialog(onValueChange: (start, end) {
                          setState(() {
                            startDateTime = start;
                            endDateTime = end;
                          });
                        });
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.calendar_today_outlined,
                            color: AppColors.bodyTextColor,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          AppText(
                              "${Utils.formatCalendarDatePicker(startDateTime)} - ${Utils.formatCalendarDatePicker(endDateTime)}"),
                        ],
                      )),
                ),
                const SizedBox(
                  height: 20,
                ),
                AppText.body("Who can win this prize?"),
                const SizedBox(
                  height: 5,
                ),
                RadioListTile(
                    title: AppText.body("Every One"),
                    value: "everyOne",
                    groupValue: _selectedValue,
                    activeColor: AppColors.primary,
                    dense: true,
                    onChanged: (value) {
                      setState(() {
                        userList.clear();
                        _selectedValue = "everyOne";
                      });
                    }),
                Row(
                  children: [
                    SizedBox(
                      width: 250,
                      child: RadioListTile(
                          title: AppText.body(
                              "People i choose${_selectedValue == "peopleIChoose" ? "   (${userList.length})" : ""}"),
                          value: "peopleIChoose",
                          groupValue: _selectedValue,
                          dense: true,
                          activeColor: AppColors.primary,
                          onChanged: (value) {
                            setState(() {
                              _selectedValue = "peopleIChoose";
                            });
                          }),
                    ),
                    if (_selectedValue == "peopleIChoose")
                      AppButton.flat(
                          width: 60,
                          text: "+ Add",
                          onPressed: () {
                            AppDialog(
                                body: AttachUsers(
                              users: userList,
                              onSelectUsers: (users) {
                                Get.back();
                                setState(() {
                                  userList = users;
                                });
                              },
                            ));
                          })
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                _action(),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ));
  }
}
