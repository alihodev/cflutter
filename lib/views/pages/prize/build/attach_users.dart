import 'package:challenger/models/user_model.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:get/get.dart';
import 'package:challenger/views/widgets/app_paginated_list_view.dart';
import 'package:challenger/views/widgets/app_search_input.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';

class AttachUsers extends StatefulWidget {
  Function(List<UserModel> users) onSelectUsers;
  List<UserModel> users;

  AttachUsers({Key? key, required this.onSelectUsers, required this.users}) : super(key: key);

  @override
  State<AttachUsers> createState() => _AttachUsersState();
}

class _AttachUsersState extends State<AttachUsers> {
  String query = "";

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 470,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [...widget.users.map((e) => _clipItem(e))],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          AppSearchTextField(onChaneValue: (value) {
            setState(() {
              query = value;
            });
          }),
          Expanded(
              child: AppPaginatedListView<UserModel>(
            url: "/google.com",
            query: query,
            isFake: true,
            onFetchData: (response) {
              return response.map<UserModel>((e) => UserModel()).toList();
            },
            renderItem: (item) {
              return ListTile(
                onTap: () {
                  setState(() {
                    widget.users.add(UserModel());
                  });
                },
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: Image.network(
                    "",
                    height: 48,
                    width: 48,
                  ),
                ),
                title: AppText.body("Ali Baker"),
                subtitle: AppText.body("@phoenix"),
              );
            },
          )),
          const Divider(),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppButton.unFocused(
                width: 110,
                  text: "Cancel",
                  onPressed: () {
                    Get.back();
                  }),
              const SizedBox(
                width: 10,
              ),
              AppButton.focused(width: 110, text: "Apply", onPressed: () {
                widget.onSelectUsers(widget.users);
              }),
            ],
          )
        ],
      ),
    );
  }

  Widget _clipItem(UserModel user) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      child: AppBorderContainer(
        height: 32,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppText(
              "Reza",
              fontSize: 12,
            ),
            const SizedBox(
              width: 5,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  widget.users.remove(user);
                });
              },
              child: const Icon(
                Icons.close,
                size: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
