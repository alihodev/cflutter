import 'dart:async';

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:challenger/amplifyconfiguration.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/firebase_options.dart';
import 'package:challenger/models/ModelProvider.dart';
import 'package:challenger/services/amplify_user_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/pages/login/intro/intro_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/widgets/app_scaffold.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:uni_links/uni_links.dart';

bool _initialUriIsHandled = false;

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  Uri? _initialUri;
  Uri? _latestUri;
  Object? _err;
  StreamSubscription? _sub;

  bool _openUrl = false;

  @override
  void initState() {
    super.initState();
    _init();
    _handleIncomingLinks();
    _handleInitialUri();
  }

  void _init() async {
      var result = await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );
      print("initialize done");

    Future.delayed(const Duration(milliseconds: 500)).then((value) async {
      if (_openUrl) return;
      var isLogin = await AppStorage.getInstance().isUserLogin();
      Get.off(isLogin ? MainPage() : const IntroPage());
    });
  }

  void _checkUrl(Uri? uri) async {
    var isLogin = await AppStorage.getInstance().isUserLogin();

    if (uri == null) {
      Get.off(isLogin ? MainPage() : const IntroPage());
      return;
    }
    String type = uri.path.split("/")[1];
    String code = uri.path.split("/")[2];
    print(type);
    print(code);
    if (type == "challenge") {
      Get.off(isLogin
          ? ChallengePage(
              initialTabIndex: 0, id: int.tryParse(code) ?? 0, title: "")
          : const IntroPage());
    } else if (type == "challenger") {
      Get.off(isLogin
          ? ProfilePage(id: int.tryParse(code) ?? 0)
          : const IntroPage());
    } else {
      Get.off(isLogin ? MainPage() : const IntroPage());
      return;
    }
  }

  void _handleIncomingLinks() {
    if (!kIsWeb) {
      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _sub = uriLinkStream.listen((Uri? uri) {
        if (!mounted) return;
        print('got uri: $uri');
        setState(() {
          _openUrl = true;
          _checkUrl(uri);
          _latestUri = uri;
          _err = null;
        });
      }, onError: (Object err) {
        if (!mounted) return;
        print('got err: $err');
        setState(() {
          _latestUri = null;
          if (err is FormatException) {
            _err = err;
          } else {
            _err = null;
          }
        });
      });
    }
  }

  Future<void> _handleInitialUri() async {
    // In this example app this is an almost useless guard, but it is here to
    // show we are not going to call getInitialUri multiple times, even if this
    // was a weidget that will be disposed of (ex. a navigation route change).
    if (!_initialUriIsHandled) {
      _initialUriIsHandled = true;
      print('_handleInitialUri called');
      try {
        final uri = await getInitialUri();
        if (uri == null) {
          print('no initial uri');
        } else {
          print('got initial uri: $uri');
        }
        if (!mounted) return;
        setState(() => _initialUri = uri);
      } on PlatformException {
        // Platform messages may fail but we ignore the exception
        print('falied to get initial uri');
      } on FormatException catch (err) {
        if (!mounted) return;
        print('malformed initial uri');
        setState(() => _err = err);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      body: Center(),
    );
  }
}
