import 'package:calendar_date_picker2/calendar_date_picker2.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/utils/utils.dart';
import 'package:challenger/views/dialogs/app_multi_date_picker.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_magic_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:challenger/views/widgets/common/app_border_container.dart';
import 'package:challenger/views/widgets/common/app_titled_scaffold.dart';
import 'package:flutter/material.dart';

class BuildDebatePage extends StatefulWidget {
  BuildDebatePage({Key? key}) : super(key: key);

  @override
  State<BuildDebatePage> createState() => _BuildDebateState();
}

class _BuildDebateState extends State<BuildDebatePage> {
  bool _notification = true;
  DateTime startDateTime = DateTime.now();
  DateTime endDateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return AppTitledScaffold(
        title: "Build Debate",
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Debate title",
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Debate title",
                  keyboardType: TextInputType.multiline,
                  height: 104,
                  borderRadius: 24,
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  hintText: "Link (optional)",
                ),
                const SizedBox(
                  height: 5,
                ),
                AppText.body(
                  "You can attach the of  challenge or the subject that you want to talk about ",
                  fontSize: 14,
                ),
                const SizedBox(
                  height: 20,
                ),
                // AppMultiDatePicker(),
                AppBorderContainer(
                  child: InkWell(
                      onTap: () {
                        AppMultiDatePickerDialog(onValueChange: (start, end) {
                          setState(() {
                            startDateTime = start;
                            endDateTime = end;
                          });
                        });
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.calendar_today_outlined,
                            color: AppColors.bodyTextColor,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          AppText("${Utils.formatCalendarDatePicker(startDateTime)} - ${Utils.formatCalendarDatePicker(endDateTime)}"),
                        ],
                      )),
                ),
                const SizedBox(
                  height: 20,
                ),
                AppBorderContainer(
                    child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppText("Send Notification To Followers"),
                    const Spacer(),
                    Switch.adaptive(
                        activeColor: AppColors.primary,
                        value: _notification,
                        onChanged: (value) {
                          setState(() {
                            _notification = value;
                          });
                        }),
                  ],
                )),
                const SizedBox(
                  height: 30,
                ),
                AppMagicButton(
                  text: "Build",
                  postUrl: "/google.com",
                  getData: () {
                    return "";
                  },
                  onDone: (response) {},
                  width: double.maxFinite,
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ));
  }
}
