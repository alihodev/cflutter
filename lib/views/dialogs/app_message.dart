import 'dart:ui';

import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppMessage {
  AppMessage.showDialogMessage(String message,
      {String title = "",
        String positiveButtonText = "سعی مجدد",
        String negativeButtonText = "لغو",
        Function? onOkAction,
        bool cancelable = true}) {
    showDialog(
      context: Get.overlayContext!,
      barrierColor: Colors.transparent,
      builder: (context) {
        return WillPopScope(
          onWillPop: () {
            return Future.value(cancelable);
          },
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
            child: Dialog(
              backgroundColor: Colors.white,
              elevation: 1,
              shadowColor: Colors.white.withOpacity(0.1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(68)),
              //this right here
              child: Container(
                width: 350,
                padding: const EdgeInsets.all(
                  15.0,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(24.0),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 7.0,
                      offset: Offset(0.0, 10.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (title.isNotEmpty)
                      Container(
                          margin: const EdgeInsets.symmetric(horizontal: 10),
                          alignment: Alignment.centerRight,
                          child: Text(
                            title,
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                                fontFamily: 'Vazir',
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          )),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        alignment: Alignment.centerLeft,
                        child: AppText.body(
                          message,
                          fontSize: 17,
                        )),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        AppButton.flat(
                            text: "Ok",
                            fontSize: 15,
                            width: 70,
                            onPressed: () {
                              Navigator.pop(context);
                              if (onOkAction != null) {
                                onOkAction();
                              }
                            }),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
