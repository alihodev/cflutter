import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppLoadingDialog {
  AppLoadingDialog.show() {
    showDialog(
        barrierDismissible: false,
        context: Get.overlayContext!,
        barrierColor: Colors.transparent,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: AppColors.secondary,
                  ),
                  child: Container(
                    height: 100,
                    width: 100,
                    child: const Center(
                      child: CircularProgressIndicator(
                        color: AppColors.primary,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  static hide() {
    Navigator.of(Get.overlayContext!).pop();
  }
}
