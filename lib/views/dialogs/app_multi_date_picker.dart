import 'package:calendar_date_picker2/calendar_date_picker2.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/dialogs/app_dialog.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppMultiDatePickerDialog {
  Function(DateTime start, DateTime end) onValueChange;

  AppMultiDatePickerDialog({required this.onValueChange}) {
    show(onValueChange);
  }

  show(Function(DateTime start, DateTime end) onValueChange) async {
    AppDialog(body: const SizedBox(width: 0,height: 0,));
    var result = await showCalendarDatePicker2Dialog(
      context: Get.overlayContext!,
      barrierColor: Colors.transparent,
      borderRadius: BorderRadius.circular(24),
      config: CalendarDatePicker2WithActionButtonsConfig(
        calendarType: CalendarDatePicker2Type.range,
        buttonPadding: const EdgeInsets.only(bottom: 13, right: 20),
        dayTextStyle: const TextStyle(color: Color(0xff344054)),
        selectedRangeHighlightColor: const Color(0xffF9F5FF),
        selectedDayHighlightColor: AppColors.primary,
        controlsTextStyle:
            const TextStyle(fontSize: 16, color: Color(0xff344054)),
        weekdayLabelTextStyle:
            const TextStyle(fontSize: 14, color: Color(0xff344054)),
        cancelButton: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: AppColors.borderColor, width: 1),
          ),
          alignment: Alignment.center,
          width: 130,
          height: 40,
          child: AppText.body(
            "Cancel",
            fontSize: 14,
            color: const Color(0xff344054),
          ),
        ),
        okButton: Container(
          decoration: BoxDecoration(
            color: AppColors.primary,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: AppColors.borderColor, width: 1),
          ),
          alignment: Alignment.center,
          width: 130,
          height: 40,
          child: AppText.body(
            "Apply",
            fontSize: 14,
            color: Colors.white,
          ),
        ),
        customModePickerIcon: const SizedBox(),
      ),
      dialogSize: const Size(325, 430),
    );
    Get.back();
    if (result != null) {
      if (result.length == 2) {
        return onValueChange(result[0]!, result[1]!);
      }
    }
  }
}
