import 'dart:ui';

import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_text.dart';
import 'package:challenger/views/widgets/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppDialog {
  AppDialog({required Widget body, bool cancelable = true}) {
    showDialog(
      context: Get.overlayContext!,
      barrierColor: Colors.transparent,
      builder: (context) {
        return WillPopScope(
          onWillPop: () {
            return Future.value(cancelable);
          },
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
            child: Dialog(
              backgroundColor: Colors.white,
              elevation: 1,
              shadowColor: Colors.black45.withOpacity(0.8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              //this right here
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: body,
              ),
            ),
          ),
        );
      },
    );
  }

  AppDialog.showAlertDialog(
      {String title = "",
        required String description,
        required Function onAction,
        String positiveButtonText = "Yes",
        double positiveButtonWidth = 70,
        String negativeButtonText = "Cancel"}) {
    AppDialog(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            if (title.isNotEmpty)
              AppText.h1(
                title,
                fontSize: 16,
              ),
            AppText.body(description),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Spacer(),
                AppButton.flat(
                    width: positiveButtonWidth,
                    fontSize: 14,
                    text: positiveButtonText,
                    onPressed: () {
                      Get.back();
                      onAction();
                    }),
                AppButton.flat(
                    width: 80,
                    fontSize: 14,
                    text: negativeButtonText,
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ],
        ));
  }

  AppDialog.showReportDialog(
      {String title = "",
        required String description,
        required Function(String description) onAction,
        String positiveButtonText = "Report",
        double positiveButtonWidth = 70,
        String negativeButtonText = "Cancel"}) {
    showDialog(
      context: Get.overlayContext!,
      barrierColor: Colors.transparent,
      builder: (context) {
      String _selectedValue = "Harassment or Bullying";
      TextEditingController controller = TextEditingController();
        return WillPopScope(
          onWillPop: () {
            return Future.value(true);
          },
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
            child: AlertDialog(
              backgroundColor: Colors.white,
              elevation: 1,
              shadowColor: Colors.black45.withOpacity(0.8),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              //this right here
              content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return Padding(
                    padding: const EdgeInsets.all(0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        if (title.isNotEmpty)
                          AppText.h1(
                            title,
                            fontSize: 16,
                          ),
                        AppText.body(description),
                        const SizedBox(height: 10,),
                        RadioListTile(
                            title: AppText.body("Harassment or Bullying", fontSize: 12,),
                            value: "Harassment or Bullying",
                            groupValue: _selectedValue,
                            activeColor: AppColors.primary,
                            contentPadding: EdgeInsets.all(0),
                            onChanged: (value) {
                              setState((){
                                _selectedValue = "Harassment or Bullying";
                              });
                            }),
                        RadioListTile(
                            title: AppText.body("Hate Speech or Discrimination", fontSize: 12,),
                            value: "Hate Speech or Discrimination",
                            groupValue: _selectedValue,
                            activeColor: AppColors.primary,
                            contentPadding: EdgeInsets.all(0),
                            onChanged: (value) {
                              setState((){
                                _selectedValue = "Hate Speech or Discrimination";
                              });
                            }),
                        RadioListTile(
                            title: AppText.body("Inappropriate Content", fontSize: 12,),
                            value: "Inappropriate Content",
                            groupValue: _selectedValue,
                            activeColor: AppColors.primary,
                            contentPadding: EdgeInsets.all(0),
                            onChanged: (value) {
                              setState((){
                                _selectedValue = "Inappropriate Content";
                              });
                            }),
                        RadioListTile(
                            title: AppText.body("Spam or Misleading Content", fontSize: 12,),
                            value: "Spam or Misleading Content",
                            groupValue: _selectedValue,
                            activeColor: AppColors.primary,
                            contentPadding: EdgeInsets.all(0),
                            dense: true,
                            onChanged: (value) {
                              setState((){
                                _selectedValue = "Spam or Misleading Content";
                              });
                            }),
                        RadioListTile(
                            title: AppText.body("Other", fontSize: 12,),
                            value: "Other",
                            groupValue: _selectedValue,
                            contentPadding: EdgeInsets.all(0),
                            activeColor: AppColors.primary,
                            dense: true,
                            onChanged: (value) {
                              setState((){
                                _selectedValue = "Other";
                              });
                            }),
                        AppTextField.round(textEditingController: controller, hintText: "Description ...",),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            const Spacer(),
                            AppButton.flat(
                                width: positiveButtonWidth,
                                fontSize: 14,
                                text: positiveButtonText,
                                onPressed: () {
                                  Get.back();
                                  onAction(_selectedValue + ": " + controller.text);
                                }),
                            AppButton.flat(
                                width: 80,
                                fontSize: 14,
                                text: negativeButtonText,
                                onPressed: () {
                                  Get.back();
                                }),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}
