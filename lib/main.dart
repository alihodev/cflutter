import 'dart:io';

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'package:challenger/amplifyconfiguration.dart';
import 'package:challenger/constants/app_colors.dart';
import 'package:challenger/models/ModelProvider.dart';
import 'package:challenger/services/app_media_picker.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:challenger/views/pages/challenge/build/build_challenge.dart';
import 'package:challenger/views/pages/challenge/challenge_page.dart';
import 'package:challenger/views/pages/contributor/profile_page.dart';
import 'package:challenger/views/pages/debate/build/build_debate_page.dart';
import 'package:challenger/views/pages/home/home_page.dart';
import 'package:challenger/views/pages/login/forget_password/forget_password_page.dart';
import 'package:challenger/views/pages/login/sign_up/sign_up_page.dart';
import 'package:challenger/views/pages/explorer/expolorer_page.dart';
import 'package:challenger/views/pages/main/main_page.dart';
import 'package:challenger/views/pages/prize/build/build_prize_page.dart';
import 'package:challenger/views/pages/splash/splash_page.dart';
import 'package:challenger/views/widgets/app_button.dart';
import 'package:challenger/views/widgets/app_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(const MyApp());
  await _configureAmplify();

  AppStorage.getInstance().init();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Challenger',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        fontFamily: 'Overpass',
      ),
      home: SplashPage(),
    );
  }
}
Future<void> _configureAmplify() async {
  final api = AmplifyAPI(modelProvider: ModelProvider.instance);
  final datastorePlugin =
  AmplifyDataStore(modelProvider: ModelProvider.instance);
  await Amplify.addPlugins([datastorePlugin, api]);


  try {
    await Amplify.configure(amplifyconfig);
  } on Exception catch (e) {
    safePrint('An error occurred configuring Amplify: $e');
  }
  safePrint("Amplify successfully init");
  await Amplify.DataStore.clear();
  await Future.delayed(const Duration(seconds: 1));
  await Amplify.DataStore.start();
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }
  String? _path;
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        child: _path == null ? AppButton(text: "select", onPressed: (){
          AppMediaPicker().pickVideoFromGallery((pickedFilePath, thumbnail) {
            setState(() {
              _path = pickedFilePath;
            });
          });
        },) : AppVideoPlayer.playFile(file: File(_path!)),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
