class RequestBodyBuilder {
  final Map<String, dynamic> _body = {};

  RequestBodyBuilder addField(String key, dynamic value) {
    _body[key] = value;
    return this;
  }

  Map<String, dynamic> build() {
    return _body;
  }
}