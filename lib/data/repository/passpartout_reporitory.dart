import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/services/app_storage.dart';

class PasspartoutRepository {
  HttpService _httpService = HttpService();

  void sendMessage(String message, AppDataListener appDataListener) async  {
    Map<String, dynamic> body = {};
    body["text"] = message;
    body["userId"] = AppStorage.getInstance().getStaticUser().id.toString();
    _httpService.post("http://gpt.metachallengers.ca/send",
        body: body,
        onSuccess: (response) {
          appDataListener.success(response["messages"][0]['content'][0]['text']['value']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void getMessages(AppDataListener appDataListener) {
    _httpService.get("/passpartout/",
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }


}
