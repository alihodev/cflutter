import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';

class DailyReportRepository {
  HttpService _httpService = HttpService();

  void setDailyReport(
      int dailyReportId,
      int challengeId,
      int videoId,
      String stage,
      String result,
      String successText,
      String failureText,
      AppDataListener appDataListener) {
    _httpService.post("/daily-report/$dailyReportId/",
        body: {
          "challengeId": challengeId,
          "successText": successText,
          "failureText": failureText,
          "videoId": videoId,
          "stage": stage,
          "result": result
        },
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void editAfterDoneDailyReport(
      int dailyReportId,
      int challengeId,
      int? videoId,
      String stage,
      String result,
      String successText,
      String failureText,
      AppDataListener appDataListener) {
    _httpService.post("/daily-report/$dailyReportId/edit-after-done",
        body: {
          "challengeId": challengeId,
          "successText": successText,
          "failureText": failureText,
          "videoId": videoId,
          "stage": stage,
          "result": result
        },
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
  void reportDailyReport(int id,String description, AppDataListener appDataListener) {
    _httpService.post("/report/daily-report",
        body: {"targetId":id, "description": description},
        onSuccess: (response) {
          appDataListener.success(response["message"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
}
