import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/models/co_and_challenger_model.dart';

class UserRepository {
  HttpService _httpService = HttpService();

  void getCurrentChallenger( AppDataListener appDataListener) {
    _httpService.get("/account/",
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void updateProfile(Map<String,dynamic> body, AppDataListener appDataListener) {
    _httpService.post("/account/",
        body: body,
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void challengerLikeToggle(int id, AppDataListener appDataListener) {
    _httpService.post("/challenger/$id/toggle/like",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void challengerFollowToggle(int id, AppDataListener appDataListener) {
    _httpService.post("/challenger/$id/toggle/follow",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void dailyReportLikeToggle(int id, AppDataListener appDataListener) {
    _httpService.post("/daily-report/$id/toggle/like",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void getUsersById(List<String> ids, AppDataListener appDataListener) {
    _httpService.get("/account/list/${ids.join(",")}",
        onSuccess: (response) {
          List<CoAndChallengerModel> users = [];
          for (var value in response['data']) {
            users.add(CoAndChallengerModel.fromJson(value));
          }
          appDataListener.success(users);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void googleConfirm(String email, String idToken, AppDataListener appDataListener) {
    _httpService.post("/account/google-confirm",
        body: {"email":email, "idToken": idToken},
        onSuccess: (response) {
          appDataListener.success(response);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void reportUser(int id,String description, AppDataListener appDataListener) {
    _httpService.post("/report/user",
        body: {"targetId":id, "description": description},
        onSuccess: (response) {
          appDataListener.success(response["message"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void blockUser(int id, AppDataListener appDataListener) {
    _httpService.post("/account/block",
        body: {"userId":id, "description": ""},
        onSuccess: (response) {
          appDataListener.success(response["message"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void deleteAccount(AppDataListener appDataListener) {
    _httpService.post("/account/delete-account",
        onSuccess: (response) {
          appDataListener.success(response["message"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void checkBlock(int id, AppDataListener appDataListener) {
    _httpService.get("/challenger/$id/check-block",
        onSuccess: (response) {
          appDataListener.success(response["data"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
}
