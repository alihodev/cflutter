import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/models/challenge_model.dart';

class ChallengeRepository {
  HttpService _httpService = HttpService();

  void openChallenge(int challengeId, AppDataListener appDataListener) {
    _httpService.post("/challenge/$challengeId/open",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void updateChallengeDetails(ChallengeModel challengeModel, AppDataListener appDataListener) {
    _httpService.post("/challenge/${challengeModel.id}/update",
        body: challengeModel.challengeDetail?.toJson(),
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void stopChallenge(int challengeId, AppDataListener appDataListener) {
    _httpService.post("/challenge/$challengeId/cancel",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void getChallengeById(int challengeId, AppDataListener appDataListener) {
    _httpService.get("/challenge/$challengeId",
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void startChallenge(int challengeId, AppDataListener appDataListener) {
    _httpService.post("/challenge/$challengeId/start-set-daily-reports",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void coChallenge(int challengeId, AppDataListener appDataListener) {
    _httpService.post("/challenge/$challengeId/co-challenge",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onForbidden: (message) {
          appDataListener.failure(message);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void challengeFollowToggle(int id, AppDataListener appDataListener) {
    _httpService.post("/challenge/$id/toggle/follow",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
  void challengeLikeToggle(int id, AppDataListener appDataListener) {
    _httpService.post("/challenge/$id/toggle/like",
        body: {},
        onSuccess: (response) {
          appDataListener.success(response['message']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void currentChallenges(AppDataListener appDataListener) {
    _httpService.get("/challenger/current-challenges",
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void reportChallenge(int id, String description, AppDataListener appDataListener) {
    _httpService.post("/report/challenge",
        body: {"targetId":id, "description": description},
        onSuccess: (response) {
          appDataListener.success(response["message"]);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
}
