import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';

class ChatRepository {
  HttpService _httpService = HttpService();

  void sendMessage(int userId, String message, AppDataListener appDataListener) {
    Map<String, dynamic> body = {};
    body["receiverId"] = userId;
    body["message"] = message;
    _httpService.post("/chat/",
        body: body,
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void getMessages(int userId, AppDataListener appDataListener) {
    _httpService.get("/chat/$userId",
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {
        },
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }


}
