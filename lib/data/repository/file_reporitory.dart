import 'dart:io';

import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:mime/mime.dart';

class FileRepository {
  HttpService _httpService = HttpService();

  void uploadVideo(String videoPath, AppDataListener appDataListener) {
    getVideoPreSignedUrl(
        videoPath,
        AppDataListener(onSuccess: (response) {
          uploadFileToS3(response['uploadUrl'], videoPath,
              appDataListener: AppDataListener(onSuccess: (data) {
                appDataListener.success(response['file']);
              }, onFailure: (message) {
                appDataListener.failure(message);
              }));
        }, onFailure: (message) {
          appDataListener.onFailure(message);
        }));
  }

  void uploadImage(String imagePath, AppDataListener appDataListener) {
    getImagePreSignedUrl(
        imagePath,
        AppDataListener(onSuccess: (response) {
          uploadFileToS3(response['uploadUrl'], imagePath,
              appDataListener: AppDataListener(onSuccess: (data) {
                appDataListener.success(response['file']);
              }, onFailure: (message) {
                appDataListener.failure(message);
              }));
        }, onFailure: (message) {
          appDataListener.onFailure(message);
        }));
  }

  void getImagePreSignedUrl(String imagePath, AppDataListener appDataListener) {
    File file = File(imagePath);
    Map<String, dynamic> body = {};
    body["size"] = file.lengthSync();
    body["extension"] = imagePath.substring(imagePath.lastIndexOf(".") + 1);
    body["contentType"] = lookupMimeType(file.path);

    _httpService.post("/file/image/upload_url",
        body: body,
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }

  void getVideoPreSignedUrl(String videoPath, AppDataListener appDataListener) {
    File file = File(videoPath);
    Map<String, dynamic> body = {};
    body["size"] = file.lengthSync();
    body["extension"] = videoPath.substring(videoPath.lastIndexOf(".") + 1);
    body["contentType"] = lookupMimeType(file.path);

    _httpService.post("/file/video/upload_url",
        body: body,
        onSuccess: (response) {
          appDataListener.success(response['data']);
        },
        onUnAuthorized: () {},
        onFailure: (message) {
          appDataListener.failure(message);
        });
  }
}
