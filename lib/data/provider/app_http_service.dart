import 'dart:convert';

import './app_http.dart' as appHttp;

class HttpService {
  Future<appHttp.AppResponse> get(String url,
      {Function? onSuccess,
      Function(String message)? onFailure,
      Function? onUnAuthorized}) async {
    var response = await appHttp.get(url);
    if (response.isSuccessful) {
      if (onSuccess != null) onSuccess(jsonDecode(response.body));
    } else if (response.unAuthorized) {
      if (onUnAuthorized != null) onUnAuthorized.call();
    } else {
      if (onFailure != null) onFailure(getError(response.body));
    }
    return response;
  }

  Future<appHttp.AppResponse> post(
    String url, {
    Map<String, dynamic>? body,
    Function(Map<String, dynamic> response)? onSuccess,
    Function(String message)? onFailure,
    Function? onUnAuthorized,
    Function(String message)? onForbidden,
  }) async {
    var response = await appHttp.post(url, body: body);
    if (response.isSuccessful) {
      if (onSuccess != null) onSuccess(jsonDecode(response.body));
    } else if (response.unAuthorized) {
      if (onUnAuthorized != null) onUnAuthorized.call();
    } else if (response.forbidden) {
      if (onForbidden != null) onForbidden.call(getError(response.body));
    } else {
      if (onFailure != null) onFailure(getError(response.body));
    }
    return response;
  }

  Future<appHttp.AppResponse> put(
    String url, {
    Map<String, dynamic>? body,
    String? token,
    Function(Map<String, dynamic> response)? onSuccess,
    Function(String message)? onFailure,
    Function? onUnAuthorized,
    Function(String message)? onForbidden,
  }) async {
    var response = await appHttp.put(url, token: token, body: body);
    if (response.isSuccessful) {
      if (onSuccess != null) onSuccess(jsonDecode(response.body));
    } else if (response.unAuthorized) {
      if (onUnAuthorized != null) onUnAuthorized.call();
    } else if (response.forbidden) {
      if (onForbidden != null) {
        onForbidden.call(getError(response.body));
      } else if (onFailure != null) onFailure(getError(response.body));
    } else {
      if (onFailure != null) onFailure(getError(response.body));
    }
    return response;
  }

  Future<appHttp.AppResponse> patch(String url,
      {Map<String, dynamic>? body,
      String? token,
      Function(Map<String, dynamic> response)? onSuccess,
      Function(String message)? onFailure,
      Function? onUnAuthorized}) async {
    var response = await appHttp.patch(url, token: token, body: body);
    if (response.isSuccessful) {
      if (onSuccess != null) onSuccess(jsonDecode(response.body));
    } else if (response.unAuthorized) {
      if (onUnAuthorized != null) onUnAuthorized.call();
    } else {
      if (onFailure != null) onFailure(getError(response.body));
    }
    return response;
  }

  Future<appHttp.AppResponse> delete(String url,
      {Map<String, dynamic>? body,
      String? token,
      Function(Map<String, dynamic> response)? onSuccess,
      Function(String message)? onFailure,
      Function? onUnAuthorized}) async {
    var response = await appHttp.delete(url, token: token, body: body);
    if (response.isSuccessful) {
      if (onSuccess != null) onSuccess(jsonDecode(response.body));
    } else if (response.unAuthorized) {
      if (onUnAuthorized != null) onUnAuthorized.call();
    } else {
      if (onFailure != null) onFailure(getError(response.body));
    }
    return response;
  }

  String getError(String body) {
    try {
      var response = jsonDecode(body);
      String message = "";
      Iterable iterable = response['errors'];
      for (var value in iterable) {
        message += (value["field"] != null ? value["field"] + " " : "") +  value["message"] + '\n';
      }
      return message.trim();
    } catch (e) {
      return "Failed, something wrong!";
    }
  }
}
