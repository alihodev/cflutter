import 'dart:convert';
import 'dart:io';

import 'package:challenger/data/provider/app_data_listener.dart';
import 'package:challenger/data/provider/app_http_service.dart';
import 'package:challenger/services/app_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:mime/mime.dart';

// const _baseUrl = "http://localhost:8080";
const _baseUrl = "https://appv2.metachallengers.ca";
// const _baseUrl = "https://app.metachallengers.ca";
// const _baseUrl = "http://62.106.95.103:3000/api/v1";
// const _baseUrl = "http://217.197.97.78:8000/v1";

String getBaseUrl() {
  return _baseUrl.replaceAll("/api/v1", "");
}

Future<AppResponse> get(String url) async {
  Map<String, String>? headers = null;

  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json; charset=utf-8',
      'x-platform': platform,
      'Accept-Encoding': 'gzip'
    };
    String token = await AppStorage.getInstance().getUserToken();
    if (token != null) {
      headers['Authorization'] = 'Bearer $token';
      // headers['Authorization'] = 'Bearer $token';
    }
  }
  print("[APP_HTTP] HTTP --> ${_baseUrl + url}");
  try {
    http.Response response;
    response = await http.get(Uri.parse(_baseUrl + url), headers: headers);
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${_baseUrl + url}");
    debugPrint("[APP_HTTP] HTTP ${response.body}", wrapWidth: 3024);
    return AppResponse.fromResponse(response);
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return AppResponse(
        "{\"errors\": [{\"message\":\"Failed, check your connection!\"}]}",
        500);
  }
}

Future<AppResponse> post(String url, {Map<String, dynamic>? body}) async {
  Map<String, String>? headers = null;
  String token = await AppStorage.getInstance().getUserToken();
  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json',
      'x-platform': platform,
      'accept': 'application/json'
    };
    if (token != null && token.isNotEmpty) {
      headers['Authorization'] = 'Bearer $token';
    }
  }
  print("[APP_HTTP] HTTP --> ${(url.startsWith("http") ? "" : _baseUrl) + url}");
  print("[APP_HTTP] HTTP ${body}");
  try {
    var response = await http.post(Uri.parse((url.startsWith("http") ? "" : _baseUrl) + url),
        headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${(url.startsWith("http") ? "" : _baseUrl) + url}");
    print("[APP_HTTP] HTTP ${response.body}");
    return AppResponse.fromResponse(response);
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return AppResponse(
        "{\"errors\": [{\"message\":\"Failed, check your connection!\"}]}",
        500);
  }
}

Future<Map<String, dynamic>> uploadFile(String url, String filePath,
    {required AppDataListener appDataListener}) async {

  if (url.contains("image")) {

  } else {

  }

  Map<String, String>? headers = null;
  String token = await AppStorage.getInstance().getUserToken();
  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json',
      'x-platform': platform,
      'accept': 'application/json'
    };
    if (token != null && token.isNotEmpty) {
      headers['Authorization'] = 'Bearer $token';
    }
  }
  print("[APP_HTTP] HTTP --> ${_baseUrl + url}");
  try {
    var request = http.MultipartRequest("POST", Uri.parse(_baseUrl + url));
    request.headers.addAll(headers);
    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath('file', filePath);

    request.files.add(multipartFile);

    http.StreamedResponse response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    var d = jsonDecode(responseString);
    // var response = await http.post(Uri.parse(_baseUrl + url),
    //     headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${_baseUrl + url}");
    print(responseString);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      appDataListener.onSuccess(d);
    } else {
      appDataListener.onFailure(HttpService().getError(responseString));
    }
    return d;
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return {};
  }
}

Future<Map<String, dynamic>> uploadFileToS3(String url, String filePath,
    { required AppDataListener appDataListener}) async {
  print("[APP_HTTP] HTTP --> ${url}");

  try {
    File file = File(filePath);
    var response = await http.put(Uri.parse(url),body: await file.readAsBytes(), headers: {"Content-Type": lookupMimeType(filePath) ?? ""});
    // var request = http.MultipartRequest("PUT", Uri.parse(url));
    // http.MultipartFile multipartFile = http.MultipartFile.fromBytes(
    //     "file",await file.readAsBytes(), filename: basename(filePath), contentType: MediaType("image","png"));
    //
    // request.files.add(multipartFile);
    // request.headers.addAll({
    //   'content-type': "image/png",
    // });
    // request.headers["content-type"] = "image/png";
    // print(request.headers);
    //
    // final response = await request.send();
    // print(request.headers);

    // var response = await http.post(Uri.parse(_baseUrl + url),
    //     headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${url}");
    if (response.statusCode >= 200 && response.statusCode < 300) {
      appDataListener.onSuccess("d");
    } else {
      appDataListener.onFailure("error");
    }
    return {};
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return {};
  }
}

Future<AppResponse> put(String url,
    {Map<String, String>? headers,
    Map<String, dynamic>? body,
    String? token}) async {
  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json',
      'x-platform': platform,
      'accept': 'application/json'
    };
    if (token != null) {
      headers['Authorization'] = 'Bearer $token';
    }
  }
  print("[APP_HTTP] HTTP --> ${_baseUrl + url}");
  try {
    var response = await http.put(Uri.parse(_baseUrl + url),
        headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${_baseUrl + url}");
    print("[APP_HTTP] HTTP ${response.body}");
    return AppResponse.fromResponse(response);
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return AppResponse(
        "{\"errors\": [{\"message\":\"Failed, check your connection!\"}]}",
        500);
  }
}

Future<AppResponse> patch(String url,
    {Map<String, String>? headers,
    Map<String, dynamic>? body,
    String? token}) async {
  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json',
      'x-platform': platform,
      'accept': 'application/json'
    };
    if (token != null) {
      headers['Authorization'] = 'Bearer $token';
    }
  }
  print("[APP_HTTP] HTTP --> ${_baseUrl + url}");
  try {
    var response = await http.patch(Uri.parse(_baseUrl + url),
        headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${_baseUrl + url}");
    print("[APP_HTTP] HTTP ${response.body}");
    return AppResponse.fromResponse(response);
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");
    return AppResponse(
        "{\"errors\": [{\"message\":\"Failed, check your connection!\"}]}",
        500);
  }
}

Future<AppResponse> delete(String url,
    {Map<String, String>? headers,
    Map<String, dynamic>? body,
    String? token}) async {
  if (headers == null) {
    String platform = "";
    if (kIsWeb)
      platform = "pwa";
    else {
      if (Platform.isAndroid)
        platform = "android";
      else if (Platform.isIOS) platform = "ios";
    }
    headers = {
      'content-type': 'application/json',
      'x-platform': platform,
      'accept': 'application/json'
    };
    if (token != null) {
      headers['Authorization'] = 'Bearer $token';
    }
  }
  try {
    print("[APP_HTTP] HTTP --> ${_baseUrl + url}");
    var response = await http.delete(Uri.parse(_baseUrl + url),
        headers: headers, body: jsonEncode(body));
    print("[APP_HTTP] HTTP [${response.statusCode}] <-- ${_baseUrl + url}");
    print("[APP_HTTP] HTTP ${response.body}");
    return AppResponse.fromResponse(response);
  } catch (e) {
    print("[APP_HTTP] HTTP ${e}");

    return AppResponse(
        "{\"errors\": [{\"message\":\"Failed, check your connection!\"}]}",
        500);
  }
}

class AppResponse extends http.Response {
  AppResponse(String body, int statusCode,
      {BaseRequest? request,
      Map<String, String> headers = const {},
      bool isRedirect = false,
      bool persistentConnection = true,
      String? reasonPhrase})
      : super(body, statusCode,
            request: request,
            headers: headers,
            isRedirect: isRedirect,
            persistentConnection: persistentConnection,
            reasonPhrase: reasonPhrase);

  AppResponse.fromResponse(http.Response response)
      : super(response.body, response.statusCode,
            request: response.request,
            headers: response.headers,
            isRedirect: response.isRedirect,
            persistentConnection: response.persistentConnection,
            reasonPhrase: response.reasonPhrase);

  bool get isSuccessful {
    return statusCode >= 200 && statusCode < 300;
  }

  bool get unAuthorized {
    return statusCode == 401;
  }

  bool get forbidden {
    return statusCode == 403;
  }
}
