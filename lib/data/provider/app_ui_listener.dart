class AppUiListener {
  Function? onSuccess;
  Function(String message)? onMessage;
  Function? onShowLoading;
  Function? onHideLoading;
  Function? onForbidden;

  AppUiListener(
      {this.onSuccess, this.onMessage, this.onShowLoading, this.onHideLoading, this.onForbidden});

  void success({response}) {
    if (onSuccess != null) onSuccess!.call(response);
  }
  void forbidden({response}) {
    if (onForbidden != null) onForbidden!.call(response);
  }

  void showMessage(String message) {
    if (onMessage != null) onMessage!.call(message);
  }

  void showLoading() {
    if (onShowLoading != null) onShowLoading!.call();
  }

  void hideLoading() {
    if (onHideLoading != null) onHideLoading!.call();
  }
}
