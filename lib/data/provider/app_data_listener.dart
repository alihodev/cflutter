import 'package:flutter/cupertino.dart';

class AppDataListener<T> {
  Function(T item) onSuccess;
  Function(String message) onFailure;
  Function(String message)? onForbidden;

  AppDataListener({required this.onSuccess, required this.onFailure, this.onForbidden});

  void success(T item) {
    onSuccess(item);
  }

  void failure(String message) {
    onFailure(message);
  }

  void forbidden(String message) {
    onForbidden!(message);
  }
}
