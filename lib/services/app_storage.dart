import 'dart:convert';

import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/request/create_challenge_request_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppStorage {
  static AppStorage? _instance;

  AppStorage._internal();

  static AppStorage getInstance() {
    _instance ??= AppStorage._internal();
    return _instance!;
  }
  late UserModel _staticUser;
  late String? _amplifyUserId;

  void init () async {
    _staticUser = await getUser();
    String token = await getUserToken();
    _amplifyUserId = await getUserAmplifyId();
    print(token);
  }

  String? getStaticAmplifyUserId() {
    return _amplifyUserId;
  }

  UserModel getStaticUser() {
    return _staticUser;
  }

  void setUserToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  void setUser(UserModel userModel) async {
    _staticUser = userModel;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('user', jsonEncode(userModel.toJson()));
  }

  Future<UserModel> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return UserModel.fromJson(jsonDecode(prefs.getString('user') ?? "{}"));
  }

  void updateBuildChallengeRequestModel(CreateChallengeRequestModel model) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('create_challenge', jsonEncode(model.toJson()));
  }

  void removeBuildChallengeRequestModel() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('create_challenge', jsonEncode("{}"));
  }

  Future<CreateChallengeRequestModel> getBuildChallengeRequestModel() async {
    final prefs = await SharedPreferences.getInstance();
    String challenge = prefs.getString('create_challenge') ?? "";
    if (challenge.isEmpty || challenge == "\"{}\"") {
      return CreateChallengeRequestModel();
    }
    return CreateChallengeRequestModel.fromJson(jsonDecode(prefs.getString('create_challenge') ?? "{}"));
  }

  void setUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('login', true);
  }

  Future<bool> isUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    return Future.value(prefs.getBool('login') ?? false);
  }

  Future<String?> getUserAmplifyId() async {
    final prefs = await SharedPreferences.getInstance();
    return Future.value(prefs.getString('amplifyUserId'));
  }

  void setAmplifyUserId(String amplifyUserId) async {
    _amplifyUserId = amplifyUserId;
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('amplifyUserId', amplifyUserId);
  }

  Future<String> getUserToken() async {
    final prefs = await SharedPreferences.getInstance();
    return Future.value(prefs.getString('token') ?? "");
  }

  void setUserLogout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('login', false);
    prefs.setString('user', '');
    prefs.setString('token', '');
    prefs.setString('amplifyUserId', '');
  }
}
