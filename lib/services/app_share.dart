import 'package:challenger/data/provider/app_http.dart';
import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/daily_report_model.dart';
import 'package:challenger/models/user_model.dart';
import 'package:share_plus/share_plus.dart';

class AppShare {
  static share(String text) {
    Share.share(text);
  }

  static shareDailyReport(DailyReportModel dailyReportModel) {
    share("${dailyReportModel.challengeName}\n${dailyReportModel.challenger?.firstname} ${dailyReportModel.challenger?.lastname}\n\n${getBaseUrl()}/daily-report/${dailyReportModel.id}");
  }

  static shareChallenge(ChallengeModel challengeModel) {
    String value = "${challengeModel.title!}\n${challengeModel.challenger!.firstname!} ${challengeModel.challenger!.lastname!}\n${challengeModel.description!}\n\n${getBaseUrl()}/challenge/${challengeModel.id}";
    share(value);
  }

  static shareChallenger(CoAndChallengerModel userModel) {
    share("${userModel.firstname} ${userModel.lastname}\n\n${getBaseUrl()}/challenger/${userModel.id}");
  }
}
