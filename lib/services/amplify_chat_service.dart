
import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/models/Chat.dart';

Future<Chat?> getAmplifyChatId(String senderId, String receiverId) async {
  List<Chat?> chats = await queryListItems(senderId, receiverId);
  if (chats.isNotEmpty) {
    return chats[0]!;
  }
  Chat? chat = await createChat(senderId, receiverId);
  return chat;

}

Future<Chat?> createChat(String senderId, String receiverId, {bool senderHasNewMessage = false,bool receiverHasNewMessage = false, String lastMessage = ""
}) async {
  try {
    final model = Chat(
        senderId: senderId,
        receiverId: receiverId,
        senderHasNewMessage: senderHasNewMessage,
        receiverHasNewMessage: receiverHasNewMessage,
        lastMessage: lastMessage,
        lastMessageDate: TemporalDateTime(DateTime.now()));

    // try {
    //   await Amplify.DataStore.save(model);
    //   safePrint("Message successfully saved");
    // } on DataStoreException catch (e) {
    //   safePrint('Something went wrong saving model: ${e.message}');
    // }

    final request = ModelMutations.create(model);
    final response = await Amplify.API.mutate(request: request).response;

    final createdChat = response.data;
    if (createdChat == null) {
      safePrint('errors: ${response.errors}');
      return null;
    }
    safePrint('Mutation result: ${createdChat.id}');
    return createdChat;
  } on ApiException catch (e) {
    safePrint('Mutation failed: $e');
    return null;
  }
}

Future<Chat> updateChat(String currentUserId, Chat model, String message) async {
  final Chat updatedItem;
  try {
    updatedItem = model.copyWith(
        senderId: model.senderId,
        receiverId: model.receiverId,
        receiverHasNewMessage: currentUserId == model.receiverId ? false : true,
        senderHasNewMessage: currentUserId == model.senderId ? false : true,
        lastMessageDate: TemporalDateTime(DateTime.now()),
        lastMessage: message);
    await Amplify.DataStore.save(updatedItem);
      safePrint("Message successfully updated");
    return updatedItem;
    } on DataStoreException catch (e) {
      safePrint('Something went wrong saving model: ${e.message}');
      return model;
    }
}

Future<void> seenChat(String currentUser, Chat model) async {
  try {
    final updatedItem = model.copyWith(
        receiverHasNewMessage: currentUser == model.receiverId ? false : model.receiverHasNewMessage,
        senderHasNewMessage: currentUser == model.senderId ? false : model.receiverHasNewMessage,);
    await Amplify.DataStore.save(updatedItem);
    safePrint("Message successfully updated");
  } on DataStoreException catch (e) {
    safePrint('Something went wrong saving model: ${e.message}');
  }
}


Future<List<Chat?>> queryListItems(String senderId, String receiverId) async {
  try {
    final request = ModelQueries.list(Chat.classType, where:
    (Chat.SENDERID.eq(senderId).and( Chat.RECEIVERID.eq(receiverId))).or(
        (Chat.RECEIVERID.eq(senderId).and(Chat.SENDERID.eq(receiverId)))));
    final response = await Amplify.API.query(request: request).response;

    final items = response.data?.items;
    if (items == null) {
      print('errors: ${response.errors}');
      return <Chat?>[];
    }
    return items;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return <Chat?>[];
}

Future<bool> hasNewMessage(String currentUserId) async {
  try {
    final request = ModelQueries.list(Chat.classType, where:
    Chat.SENDERID.eq(currentUserId).or( Chat.RECEIVERID.eq(currentUserId)));
    final response = await Amplify.API.query(request: request).response;

    final items = response.data?.items;
    if (items == null) {
      print('errors: ${response.errors}');
      return false;
    }
    for (var item in items) {
      if (item!.senderId == currentUserId) {
        if (item.senderHasNewMessage == true) {
          return true;
        }
      } else {
        if (item.receiverHasNewMessage == true) {
          return true;
        }
      }
    }
    return false;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return false;
}