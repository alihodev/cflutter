import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/models/ModelProvider.dart';

Future<void> createMessage(
    {required String chatId,required String message,required String senderId,required String receiverId}) async {
  // try {
    final model = Message(
        chatId: chatId,
        message: message,
        createdAt: TemporalDateTime(DateTime.now()),
        senderId: senderId,
        receiverId: receiverId);

    try {
      await Amplify.DataStore.save(model);
      safePrint("Message successfully saved");
    } on DataStoreException catch (e) {
      safePrint('Something went wrong saving model: ${e.message}');
    }

  //   final request = ModelMutations.create(model);
  //   final response = await Amplify.API.mutate(request: request).response;
  //
  //   final createdMessage = response.data;
  //   if (createdMessage == null) {
  //     safePrint('errors: ${response.errors}');
  //     return;
  //   }
  //   safePrint('Mutation result: ${createdMessage.id}');
  // } on ApiException catch (e) {
  //   safePrint('Mutation failed: $e');
  // }
}