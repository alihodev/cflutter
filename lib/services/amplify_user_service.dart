import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:challenger/models/ModelProvider.dart';
import 'package:challenger/models/User.dart';


Future<String?> getAmplifyUserId(String userId) async {
  User? userExist = await isUserExist(userId);
  print(userExist != null);
  if (userExist != null) {
    return userExist.id;
  }
  String? newId = await createUser(userId);
  return newId;

}

Future<String?> createUser(String userId) async {
  try {
    final model = User(
        userId: userId);
    ModelProvider().getModelTypeByModelName("User");
    final request = ModelMutations.create(model);
    final response = await Amplify.API.mutate(request: request).response;

    final createdUser = response.data;
    if (createdUser == null) {
      safePrint('errors: ${response.errors}');
      return null;
    }
    safePrint('Mutation result: ${createdUser.id}');
    return createdUser.id;
  } on ApiException catch (e) {
    safePrint('Mutation failed: $e');
    return null;
  }
}

Future<User?> isUserExist(String userId) async {
  List<User?> users = await queryListItems(userId);
  if (users.isNotEmpty) {
    return users[0];
  }
  return null;
}

Future<List<User?>> queryListItems(String userId) async {
  try {
    final request = ModelQueries.list(User.classType,where: User.USERID.eq(userId));

    final response = await Amplify.API.query(request: request).response;

    final items = response.data?.items;
    if (items == null) {
      print('errors: ${response.errors}');
      return <User?>[];
    }
    return items;
  } on ApiException catch (e) {
    print('Query failed: $e');
  }
  return <User?>[];
}