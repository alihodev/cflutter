
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class AppMediaPicker {
  Future<void> pickImageFromGallery(
      Function(String pickedFilePath) onPickImage) async {
    final picker = ImagePicker();
    final picked = await picker.pickImage(source: ImageSource.gallery);
    if (picked != null) {
      File rotatedImage =
      await FlutterExifRotation.rotateImage(path: picked.path);
      onPickImage(rotatedImage.path);
    }
  }

  Future<void> pickVideoFromGallery(
      Function(String pickedFilePath, Image thumbnail) onPickVideo) async {
    final picker = ImagePicker();
    final picked = await picker.pickVideo(source: ImageSource.gallery);
    if (picked != null) {
      Image thumbnail = await getThumbnail(picked.path);
      onPickVideo(picked.path, thumbnail);
    }
  }

  Future<Image> getThumbnail(String videoPath) async {
    final uint8list = await VideoThumbnail.thumbnailData(
      video: videoPath,
      imageFormat: ImageFormat.JPEG,
    );
    return Image.memory(uint8list!, fit: BoxFit.cover,);
  }
}
