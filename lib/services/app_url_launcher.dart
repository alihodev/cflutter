import 'package:url_launcher/url_launcher.dart';

class AppUrlLauncher {
  AppUrlLauncher.loadUrl(String url) {
    _openUrl(url);
  }

  void _openUrl (String url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw Exception('Could not launch $url');
    }
  }
}