

import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xff0A3161);
  static const Color secondary = Color(0xffD9DCDE);
  static const Color bodyTextColor = Color(0xff484848);
  static const Color h1TextColor = Color(0xff1d1d1d);
  static const Color h2TextColor = Color(0xcb1d1d1d);
  static const Color inputFill = Color(0xfff4f4f4);
  static const Color unSelectedTabColor = Color(0xffAAAAAA);
  static const Color actionBarColor = Color(0xffF4f4f4);
  static const Color challengeCardBackground = Color(0xffF4f4f4);
  static const Color debateCardBackground = Color(0xffEBE9FE);
  static const Color coChallengerCardBackground = Color(0xffDFEAFF);
  static const Color challengerCardBackground = Color(0xffE7FBFF);
  static const Color unSelectedCategory = Color(0xff8d8d8d);
  static const Color borderColor = Color(0xffD0D5DD);
}