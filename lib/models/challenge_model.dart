class ChallengeModel {
  int? id;
  String? title;
  String? status;
  String? description;
  String? stage;
  String? type;
  Challenger? challenger;
  ChallengeDetail? challengeDetail;
  String? coverUrl;
  String? introVideoUrl;
  Category? category;
  int? firstStageCount;
  int? secondStageCount;
  int? endStageCount;
  String? firstStageStartDate;
  String? secondStageStartDate;
  String? endStageStartDate;
  bool? coChallenge;
  bool? follow;
  int? followers;
  int? coChallengers;
  int? progress;
  bool? like;
  int? likes;

  ChallengeModel(
      {this.id,
        this.title,
        this.description,
        this.stage,
        this.type,
        this.challenger,
        this.coverUrl,
        this.introVideoUrl,
        this.category,
        this.firstStageCount,
        this.secondStageCount,
        this.endStageCount,
        this.firstStageStartDate,
        this.secondStageStartDate,
        this.endStageStartDate,
        this.follow,
        this.followers,
        this.coChallengers,
        this.progress,
        this.like,
        this.challengeDetail,
        this.status,
        this.likes,
      this.coChallenge});

  ChallengeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    stage = json['stage'];
    type = json['type'];
    challenger = json['challenger'] != null
        ? Challenger.fromJson(json['challenger'])
        : null;
    challengeDetail = json['challengeDetail'] != null
        ? ChallengeDetail.fromJson(json['challengeDetail'])
        : null;
    coverUrl = json['coverUrl'];
    introVideoUrl = json['introVideoUrl'];
    category = json['category'] != null
        ? Category.fromJson(json['category'])
        : null;
    firstStageCount = json['firstStageCount'];
    secondStageCount = json['secondStageCount'];
    endStageCount = json['endStageCount'];
    firstStageStartDate = json['firstStageStartDate'];
    secondStageStartDate = json['secondStageStartDate'];
    endStageStartDate = json['endStageStartDate'];
    coChallenge = json['coChallenge'];
    follow = json['follow'];
    followers = json['followers'];
    coChallengers = json['coChallengers'];
    progress = json['progress'];
    like = json['like'];
    likes = json['likes'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['stage'] = stage;
    data['type'] = type;
    if (challenger != null) {
      data['challenger'] = challenger!.toJson();
    }
    if (challengeDetail != null) {
      data['challengeDetail'] = challengeDetail!.toJson();
    }
    data['coverUrl'] = coverUrl;
    data['introVideoUrl'] = introVideoUrl;
    if (category != null) {
      data['category'] = category!.toJson();
    }
    data['firstStageCount'] = firstStageCount;
    data['secondStageCount'] = secondStageCount;
    data['endStageCount'] = endStageCount;
    data['firstStageStartDate'] = firstStageStartDate;
    data['secondStageStartDate'] = secondStageStartDate;
    data['endStageStartDate'] = endStageStartDate;
    data['coChallenge'] = coChallenge;
    data['follow'] = follow;
    data['followers'] = followers;
    data['coChallengers'] = coChallengers;
    data['progress'] = progress;
    data['like'] = like;
    data['likes'] = likes;
    data['status'] = status;
    return data;
  }
}

class Challenger {
  int? id;
  String? username;
  String? email;
  String? phone;
  String? firstname;
  String? lastname;
  String? profileUrl;

  Challenger({this.id, this.username, this.email, this.phone, this.profileUrl,this.firstname,this.lastname});

  Challenger.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    profileUrl = json['profileUrl'];
    firstname = json['firstname'];
    lastname = json['lastname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['username'] = username;
    data['email'] = email;
    data['phone'] = phone;
    data['profileUrl'] = profileUrl;
    data['firstname'] = firstname;
    data['lastname'] = lastname;
    return data;
  }
}

class ChallengeDetail {
  int? id;
  String? initiator;
  String? disadvantage;
  String? advantage;
  String? influence;
  String? necessities;
  String? contributors;
  String? honor;
  String? whyYou;
  String? realIdentity;
  String? idealIdentity;
  String? firstStageAchievement;
  String? secondStageAchievement;
  String? endStageAchievement;

  ChallengeDetail(
      {this.id,
        this.initiator,
        this.disadvantage,
        this.advantage,
        this.influence,
        this.necessities,
        this.contributors,
        this.honor,
        this.whyYou,
        this.realIdentity,
        this.idealIdentity,
        this.firstStageAchievement,
        this.secondStageAchievement,
        this.endStageAchievement});

  ChallengeDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    initiator = json['initiator'];
    disadvantage = json['disadvantage'];
    advantage = json['advantage'];
    influence = json['influence'];
    necessities = json['necessities'];
    contributors = json['contributors'];
    honor = json['honor'];
    whyYou = json['whyYou'];
    realIdentity = json['realIdentity'];
    idealIdentity = json['idealIdentity'];
    firstStageAchievement = json['firstStageAchievement'];
    secondStageAchievement = json['secondStageAchievement'];
    endStageAchievement = json['endStageAchievement'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['initiator'] = this.initiator;
    data['disadvantage'] = this.disadvantage;
    data['advantage'] = this.advantage;
    data['influence'] = this.influence;
    data['necessities'] = this.necessities;
    data['contributors'] = this.contributors;
    data['honor'] = this.honor;
    data['whyYou'] = this.whyYou;
    data['realIdentity'] = this.realIdentity;
    data['idealIdentity'] = this.idealIdentity;
    data['firstStageAchievement'] = this.firstStageAchievement;
    data['secondStageAchievement'] = this.secondStageAchievement;
    data['endStageAchievement'] = this.endStageAchievement;
    return data;
  }
}


class Category {
  int? id;
  String? title;

  Category({this.id, this.title});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['title'] = title;
    return data;
  }
}
