import 'dart:math';

class StoryModel {
  StoryState state = StoryState.green;
  StoryModel() {
    state = StoryState.values[Random().nextInt(StoryState.values.length)];
  }
}

enum StoryState {
  red,
  green,
  yellow,
}