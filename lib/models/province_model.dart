class ProvinceModel {
  String? createdAt;
  String? updatedAt;
  int? id;
  String? name;

  ProvinceModel({this.createdAt, this.updatedAt, this.id, this.name});

  ProvinceModel.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
