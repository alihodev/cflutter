/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart' as amplify_core;


/** This is an auto generated class representing the Chat type in your schema. */
class Chat extends amplify_core.Model {
  static const classType = const _ChatModelType();
  final String id;
  final String? _senderId;
  final String? _receiverId;
  final bool? _receiverHasNewMessage;
  final bool? _senderHasNewMessage;
  final amplify_core.TemporalDateTime? _lastMessageDate;
  final String? _lastMessage;
  final amplify_core.TemporalDateTime? _createdAt;
  final amplify_core.TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;
  
  @Deprecated('[getId] is being deprecated in favor of custom primary key feature. Use getter [modelIdentifier] to get model identifier.')
  @override
  String getId() => id;
  
  ChatModelIdentifier get modelIdentifier {
      return ChatModelIdentifier(
        id: id
      );
  }
  
  String? get senderId {
    return _senderId;
  }
  
  String? get receiverId {
    return _receiverId;
  }
  
  bool? get receiverHasNewMessage {
    return _receiverHasNewMessage;
  }
  
  bool? get senderHasNewMessage {
    return _senderHasNewMessage;
  }
  
  amplify_core.TemporalDateTime? get lastMessageDate {
    return _lastMessageDate;
  }
  
  String? get lastMessage {
    return _lastMessage;
  }
  
  amplify_core.TemporalDateTime? get createdAt {
    return _createdAt;
  }
  
  amplify_core.TemporalDateTime? get updatedAt {
    return _updatedAt;
  }
  
  const Chat._internal({required this.id, senderId, receiverId, receiverHasNewMessage, senderHasNewMessage, lastMessageDate, lastMessage, createdAt, updatedAt}): _senderId = senderId, _receiverId = receiverId, _receiverHasNewMessage = receiverHasNewMessage, _senderHasNewMessage = senderHasNewMessage, _lastMessageDate = lastMessageDate, _lastMessage = lastMessage, _createdAt = createdAt, _updatedAt = updatedAt;
  
  factory Chat({String? id, String? senderId, String? receiverId, bool? receiverHasNewMessage, bool? senderHasNewMessage, amplify_core.TemporalDateTime? lastMessageDate, String? lastMessage}) {
    return Chat._internal(
      id: id == null ? amplify_core.UUID.getUUID() : id,
      senderId: senderId,
      receiverId: receiverId,
      receiverHasNewMessage: receiverHasNewMessage,
      senderHasNewMessage: senderHasNewMessage,
      lastMessageDate: lastMessageDate,
      lastMessage: lastMessage);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Chat &&
      id == other.id &&
      _senderId == other._senderId &&
      _receiverId == other._receiverId &&
      _receiverHasNewMessage == other._receiverHasNewMessage &&
      _senderHasNewMessage == other._senderHasNewMessage &&
      _lastMessageDate == other._lastMessageDate &&
      _lastMessage == other._lastMessage;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Chat {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("senderId=" + "$_senderId" + ", ");
    buffer.write("receiverId=" + "$_receiverId" + ", ");
    buffer.write("receiverHasNewMessage=" + (_receiverHasNewMessage != null ? _receiverHasNewMessage!.toString() : "null") + ", ");
    buffer.write("senderHasNewMessage=" + (_senderHasNewMessage != null ? _senderHasNewMessage!.toString() : "null") + ", ");
    buffer.write("lastMessageDate=" + (_lastMessageDate != null ? _lastMessageDate!.format() : "null") + ", ");
    buffer.write("lastMessage=" + "$_lastMessage" + ", ");
    buffer.write("createdAt=" + (_createdAt != null ? _createdAt!.format() : "null") + ", ");
    buffer.write("updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Chat copyWith({String? senderId, String? receiverId, bool? receiverHasNewMessage, bool? senderHasNewMessage, amplify_core.TemporalDateTime? lastMessageDate, String? lastMessage}) {
    return Chat._internal(
      id: id,
      senderId: senderId ?? this.senderId,
      receiverId: receiverId ?? this.receiverId,
      receiverHasNewMessage: receiverHasNewMessage ?? this.receiverHasNewMessage,
      senderHasNewMessage: senderHasNewMessage ?? this.senderHasNewMessage,
      lastMessageDate: lastMessageDate ?? this.lastMessageDate,
      lastMessage: lastMessage ?? this.lastMessage);
  }
  
  Chat copyWithModelFieldValues({
    ModelFieldValue<String?>? senderId,
    ModelFieldValue<String?>? receiverId,
    ModelFieldValue<bool?>? receiverHasNewMessage,
    ModelFieldValue<bool?>? senderHasNewMessage,
    ModelFieldValue<amplify_core.TemporalDateTime?>? lastMessageDate,
    ModelFieldValue<String?>? lastMessage
  }) {
    return Chat._internal(
      id: id,
      senderId: senderId == null ? this.senderId : senderId.value,
      receiverId: receiverId == null ? this.receiverId : receiverId.value,
      receiverHasNewMessage: receiverHasNewMessage == null ? this.receiverHasNewMessage : receiverHasNewMessage.value,
      senderHasNewMessage: senderHasNewMessage == null ? this.senderHasNewMessage : senderHasNewMessage.value,
      lastMessageDate: lastMessageDate == null ? this.lastMessageDate : lastMessageDate.value,
      lastMessage: lastMessage == null ? this.lastMessage : lastMessage.value
    );
  }
  
  Chat.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _senderId = json['senderId'],
      _receiverId = json['receiverId'],
      _receiverHasNewMessage = json['receiverHasNewMessage'],
      _senderHasNewMessage = json['senderHasNewMessage'],
      _lastMessageDate = json['lastMessageDate'] != null ? amplify_core.TemporalDateTime.fromString(json['lastMessageDate']) : null,
      _lastMessage = json['lastMessage'],
      _createdAt = json['createdAt'] != null ? amplify_core.TemporalDateTime.fromString(json['createdAt']) : null,
      _updatedAt = json['updatedAt'] != null ? amplify_core.TemporalDateTime.fromString(json['updatedAt']) : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'senderId': _senderId, 'receiverId': _receiverId, 'receiverHasNewMessage': _receiverHasNewMessage, 'senderHasNewMessage': _senderHasNewMessage, 'lastMessageDate': _lastMessageDate?.format(), 'lastMessage': _lastMessage, 'createdAt': _createdAt?.format(), 'updatedAt': _updatedAt?.format()
  };
  
  Map<String, Object?> toMap() => {
    'id': id,
    'senderId': _senderId,
    'receiverId': _receiverId,
    'receiverHasNewMessage': _receiverHasNewMessage,
    'senderHasNewMessage': _senderHasNewMessage,
    'lastMessageDate': _lastMessageDate,
    'lastMessage': _lastMessage,
    'createdAt': _createdAt,
    'updatedAt': _updatedAt
  };

  static final amplify_core.QueryModelIdentifier<ChatModelIdentifier> MODEL_IDENTIFIER = amplify_core.QueryModelIdentifier<ChatModelIdentifier>();
  static final ID = amplify_core.QueryField(fieldName: "id");
  static final SENDERID = amplify_core.QueryField(fieldName: "senderId");
  static final RECEIVERID = amplify_core.QueryField(fieldName: "receiverId");
  static final RECEIVERHASNEWMESSAGE = amplify_core.QueryField(fieldName: "receiverHasNewMessage");
  static final SENDERHASNEWMESSAGE = amplify_core.QueryField(fieldName: "senderHasNewMessage");
  static final LASTMESSAGEDATE = amplify_core.QueryField(fieldName: "lastMessageDate");
  static final LASTMESSAGE = amplify_core.QueryField(fieldName: "lastMessage");
  static var schema = amplify_core.Model.defineSchema(define: (amplify_core.ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Chat";
    modelSchemaDefinition.pluralName = "Chats";
    
    modelSchemaDefinition.authRules = [
      amplify_core.AuthRule(
        authStrategy: amplify_core.AuthStrategy.PUBLIC,
        operations: const [
          amplify_core.ModelOperation.CREATE,
          amplify_core.ModelOperation.UPDATE,
          amplify_core.ModelOperation.DELETE,
          amplify_core.ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.SENDERID,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.RECEIVERID,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.RECEIVERHASNEWMESSAGE,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.SENDERHASNEWMESSAGE,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.LASTMESSAGEDATE,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
      key: Chat.LASTMESSAGE,
      isRequired: false,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.nonQueryField(
      fieldName: 'createdAt',
      isRequired: false,
      isReadOnly: true,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.nonQueryField(
      fieldName: 'updatedAt',
      isRequired: false,
      isReadOnly: true,
      ofType: amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.dateTime)
    ));
  });
}

class _ChatModelType extends amplify_core.ModelType<Chat> {
  const _ChatModelType();
  
  @override
  Chat fromJson(Map<String, dynamic> jsonData) {
    return Chat.fromJson(jsonData);
  }
  
  @override
  String modelName() {
    return 'Chat';
  }
}

/**
 * This is an auto generated class representing the model identifier
 * of [Chat] in your schema.
 */
class ChatModelIdentifier implements amplify_core.ModelIdentifier<Chat> {
  final String id;

  /** Create an instance of ChatModelIdentifier using [id] the primary key. */
  const ChatModelIdentifier({
    required this.id});
  
  @override
  Map<String, dynamic> serializeAsMap() => (<String, dynamic>{
    'id': id
  });
  
  @override
  List<Map<String, dynamic>> serializeAsList() => serializeAsMap()
    .entries
    .map((entry) => (<String, dynamic>{ entry.key: entry.value }))
    .toList();
  
  @override
  String serializeAsString() => serializeAsMap().values.join('#');
  
  @override
  String toString() => 'ChatModelIdentifier(id: $id)';
  
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    
    return other is ChatModelIdentifier &&
      id == other.id;
  }
  
  @override
  int get hashCode =>
    id.hashCode;
}