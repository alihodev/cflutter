class CoAndChallengerModel {
  int? id;
  String? username;
  String? firstname;
  String? lastname;
  String? email;
  String? profileUrl;
  String? introVideoUrl;
  String? introVideoCoverUrl;
  String? description;
  bool? challenger;
  String? mobile;
  String? facebookUrl;
  String? youtubeUrl;
  String? tweeterUrl;
  String? clubhouseUrl;
  String? instagramUrl;
  String? linkedinUrl;
  bool? like;
  int? likes;
  bool? follow;
  int? followers;
  int? followings;
  int? coChallengers;

  CoAndChallengerModel(
      {this.id,
        this.username,
        this.firstname,
        this.lastname,
        this.email,
        this.profileUrl,
        this.introVideoUrl,
        this.introVideoCoverUrl,
        this.description,
        this.challenger,
        this.mobile,
        this.facebookUrl,
        this.youtubeUrl,
        this.tweeterUrl,
        this.clubhouseUrl,
        this.instagramUrl,
        this.linkedinUrl,
        this.follow,
        this.followers,
        this.followings,
        this.coChallengers,
        this.likes,
      this.like});

  CoAndChallengerModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    email = json['email'];
    profileUrl = json['profileUrl'];
    introVideoUrl = json['introVideoUrl'];
    introVideoCoverUrl = json['introVideoCoverUrl'];
    description = json['description'];
    challenger = json['challenger'];
    mobile = json['mobile'];
    facebookUrl = json['facebookUrl'];
    youtubeUrl = json['youtubeUrl'];
    tweeterUrl = json['tweeterUrl'];
    clubhouseUrl = json['clubhouseUrl'];
    instagramUrl = json['instagramUrl'];
    linkedinUrl = json['linkedinUrl'];
    like = json['like'];
    likes = json['likes'];
    follow = json['follow'];
    followers = json['followers'];
    followings = json['followings'];
    coChallengers = json['coChallengers'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['email'] = this.email;
    data['profileUrl'] = this.profileUrl;
    data['introVideoUrl'] = this.introVideoUrl;
    data['introVideoCoverUrl'] = this.introVideoCoverUrl;
    data['description'] = this.description;
    data['challenger'] = this.challenger;
    data['mobile'] = this.mobile;
    data['facebookUrl'] = this.facebookUrl;
    data['youtubeUrl'] = this.youtubeUrl;
    data['tweeterUrl'] = this.tweeterUrl;
    data['clubhouseUrl'] = this.clubhouseUrl;
    data['instagramUrl'] = this.instagramUrl;
    data['linkedinUrl'] = this.linkedinUrl;
    data['like'] = this.like;
    data['likes'] = this.likes;
    data['follow'] = this.follow;
    data['followings'] = this.followings;
    data['followers'] = this.followers;
    data['coChallengers'] = this.coChallengers;
    return data;
  }
}
