import 'package:challenger/models/Chat.dart';

class ChatListModel {
  String? id;
  String? user1Id;
  String? user2Id;
  bool? hasNewMessage;
  String? createdAt;
  String? lastMessage;
  String? lastMessageDate;
  User? user;
  Chat? chat;

  ChatListModel(
      {this.id,
        this.user1Id,
        this.user2Id,
        this.hasNewMessage,
        this.createdAt,
        this.lastMessage,
        this.lastMessageDate,
        this.user});

  ChatListModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    user1Id = json['user1Id'];
    user2Id = json['user2Id'];
    hasNewMessage = json['hasNewMessage'];
    createdAt = json['createdAt'];
    lastMessage = json['lastMessage'];
    lastMessageDate = json['lastMessageDate'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user1Id'] = this.user1Id;
    data['user2Id'] = this.user2Id;
    data['hasNewMessage'] = this.hasNewMessage;
    data['createdAt'] = this.createdAt;
    data['lastMessage'] = this.lastMessage;
    data['lastMessageDate'] = this.lastMessageDate;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? username;
  String? firstname;
  String? lastname;
  String? email;
  String? profileUrl;
  String? introVideoUrl;
  String? introVideoCoverUrl;
  String? description;
  bool? challenger;
  String? facebookUrl;
  String? youtubeUrl;
  String? tweeterUrl;
  String? clubhouseUrl;
  String? instagramUrl;
  String? linkedinUrl;
  bool? like;
  bool? follow;

  User(
      {this.id,
        this.username,
        this.firstname,
        this.lastname,
        this.email,
        this.profileUrl,
        this.introVideoUrl,
        this.introVideoCoverUrl,
        this.description,
        this.challenger,
        this.facebookUrl,
        this.youtubeUrl,
        this.tweeterUrl,
        this.clubhouseUrl,
        this.instagramUrl,
        this.linkedinUrl,
        this.like,
        this.follow});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    email = json['email'];
    profileUrl = json['profileUrl'];
    introVideoUrl = json['introVideoUrl'];
    introVideoCoverUrl = json['introVideoCoverUrl'];
    description = json['description'];
    challenger = json['challenger'];
    facebookUrl = json['facebookUrl'];
    youtubeUrl = json['youtubeUrl'];
    tweeterUrl = json['tweeterUrl'];
    clubhouseUrl = json['clubhouseUrl'];
    instagramUrl = json['instagramUrl'];
    linkedinUrl = json['linkedinUrl'];
    like = json['like'];
    follow = json['follow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['email'] = this.email;
    data['profileUrl'] = this.profileUrl;
    data['introVideoUrl'] = this.introVideoUrl;
    data['introVideoCoverUrl'] = this.introVideoCoverUrl;
    data['description'] = this.description;
    data['challenger'] = this.challenger;
    data['facebookUrl'] = this.facebookUrl;
    data['youtubeUrl'] = this.youtubeUrl;
    data['tweeterUrl'] = this.tweeterUrl;
    data['clubhouseUrl'] = this.clubhouseUrl;
    data['instagramUrl'] = this.instagramUrl;
    data['linkedinUrl'] = this.linkedinUrl;
    data['like'] = this.like;
    data['follow'] = this.follow;
    return data;
  }
}
