
class CreateChallengeRequestModel {
  String? title;
  String? description;
  int? cover;
  int? introVideo;
  String? coverLocal;
  String? introLocal;
  String? category;
  int? type;
  String? initiator;
  String? advantage;
  String? disadvantage;
  String? influence;
  String? necessities;
  String? honor;
  String? whyYou;
  String? realIdentity;
  String? idealIdentity;
  String? firstStageAchievement;
  String? secondStageAchievement;
  String? endStageAchievement;
  dynamic contributors;
  int? firstStageCount;
  int? secondStageCount;
  int? endStageCount;

  CreateChallengeRequestModel(
      {this.title,
        this.description,
        this.cover,
        this.introVideo,
        this.introLocal,
        this.coverLocal,
        this.category,
        this.type,
        this.initiator,
        this.advantage,
        this.disadvantage,
        this.influence,
        this.necessities,
        this.honor,
        this.whyYou,
        this.realIdentity,
        this.idealIdentity,
        this.firstStageAchievement,
        this.secondStageAchievement,
        this.contributors,
        this.firstStageCount,
        this.secondStageCount,
        this.endStageCount,
        this.endStageAchievement});

  CreateChallengeRequestModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    cover = json['cover'];
    introVideo = json['introVideo'];
    introLocal = json['introLocal'];
    coverLocal = json['coverLocal'];
    category = json['category'];
    type = json['type'];
    initiator = json['initiator'];
    advantage = json['advantage'];
    disadvantage = json['disadvantage'];
    influence = json['influence'];
    necessities = json['necessities'];
    honor = json['honor'];
    whyYou = json['whyYou'];
    realIdentity = json['realIdentity'];
    idealIdentity = json['idealIdentity'];
    secondStageAchievement = json['secondStageAchievement'];
    firstStageAchievement = json['firstStageAchievement'];
    endStageAchievement = json['endStageAchievement'];
    contributors = json['contributors'];
    firstStageCount = json['firstStageCount'];
    secondStageCount = json['secondStageCount'];
    endStageCount = json['endStageCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['title'] = title;
    data['description'] = description;
    data['cover'] = cover;
    data['introVideo'] = introVideo;
    data['introLocal'] = introLocal;
    data['coverLocal'] = coverLocal;
    data['category'] = category;
    data['type'] = type;
    data['initiator'] = initiator;
    data['advantage'] = advantage;
    data['disadvantage'] = disadvantage;
    data['influence'] = influence;
    data['necessities'] = necessities;
    data['honor'] = honor;
    data['whyYou'] = whyYou;
    data['realIdentity'] = realIdentity;
    data['idealIdentity'] = idealIdentity;
    data['firstStageAchievement'] = firstStageAchievement;
    data['secondStageAchievement'] = secondStageAchievement;
    data['endStageAchievement'] = endStageAchievement;
    data['contributors'] = contributors;
    data['firstStageCount'] = firstStageCount;
    data['secondStageCount'] = secondStageCount;
    data['endStageCount'] = endStageCount;
    return data;
  }
}
