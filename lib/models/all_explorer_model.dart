import 'dart:math';

import 'package:challenger/models/challenge_model.dart';
import 'package:challenger/models/co_and_challenger_model.dart';
import 'package:challenger/models/debates_model.dart';
import 'package:challenger/models/user_model.dart';

class AllExplorerModel {
  int type = 0;
  ChallengeModel challengeModel = ChallengeModel();
  CoAndChallengerModel coAndChallengerModel = CoAndChallengerModel();
  // DebateModel debateModel = DebateModel();
}