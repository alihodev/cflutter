class ChatModel {
  String? id;
  String? conversationId;
  String? senderId;
  String? receiverId;
  String? message;
  String? createdAt;
  bool isCurrentUser = false;

  ChatModel(
      {this.id,
        this.conversationId,
        this.senderId,
        this.receiverId,
        this.message,
        this.createdAt});

  ChatModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    conversationId = json['conversationId'];
    senderId = json['senderId'];
    receiverId = json['receiverId'];
    message = json['message'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['conversationId'] = this.conversationId;
    data['senderId'] = this.senderId;
    data['receiverId'] = this.receiverId;
    data['message'] = this.message;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
