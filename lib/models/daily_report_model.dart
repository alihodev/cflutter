class DailyReportModel {
  int? id;
  int? index;
  int? parentChallengeId;
  int? challengeId;
  Challenger? challenger;
  String? stage;
  String? status;
  String? aim;
  String? dailyVideoUrl;
  String? dailyVideoCoverUrl;
  String? result;
  String? date;
  String? successText;
  String? failureText;
  String? challengeName;
  bool? like;
  int? likes;
  bool? editable;

  DailyReportModel(
      {this.id,
        this.index,
        this.parentChallengeId,
        this.challengeId,
        this.challenger,
        this.stage,
        this.status,
        this.aim,
        this.dailyVideoUrl,
        this.dailyVideoCoverUrl,
        this.result,
        this.date,
        this.successText,
        this.like,
        this.likes,
        this.challengeName,
        this.editable,
        this.failureText});

  DailyReportModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    index = json['index'];
    parentChallengeId = json['parentChallengeId'];
    challengeId = json['challengeId'];
    challenger = json['challenger'] != null
        ? new Challenger.fromJson(json['challenger'])
        : null;
    stage = json['stage'];
    status = json['status'];
    aim = json['aim'];
    dailyVideoUrl = json['dailyVideoUrl'];
    dailyVideoCoverUrl = json['dailyVideoCoverUrl'];
    result = json['result'];
    date = json['date'];
    successText = json['successText'];
    failureText = json['failureText'];
    challengeName = json['challengeName'];
    like = json['like'];
    likes = json['likes'];
    editable = json['editable'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['index'] = this.index;
    data['parentChallengeId'] = this.parentChallengeId;
    data['challengeId'] = this.challengeId;
    if (this.challenger != null) {
      data['challenger'] = this.challenger!.toJson();
    }
    data['stage'] = this.stage;
    data['status'] = this.status;
    data['aim'] = this.aim;
    data['dailyVideoUrl'] = this.dailyVideoUrl;
    data['dailyVideoCoverUrl'] = this.dailyVideoCoverUrl;
    data['result'] = this.result;
    data['date'] = this.date;
    data['successText'] = this.successText;
    data['failureText'] = this.failureText;
    data['like'] = this.like;
    data['likes'] = this.likes;
    data['challengeName'] = this.challengeName;
    data['editable'] = this.editable;
    return data;
  }
}

class Challenger {
  int? id;
  String? username;
  String? firstname;
  String? lastname;
  String? email;
  String? profileUrl;
  String? introVideoUrl;
  String? introVideoCoverUrl;
  String? description;
  bool? challenger;
  Null? mobile;
  String? facebookUrl;
  String? youtubeUrl;
  String? tweeterUrl;
  String? clubhouseUrl;
  String? instagramUrl;
  String? linkedinUrl;
  bool? like;

  Challenger(
      {this.id,
        this.username,
        this.firstname,
        this.lastname,
        this.email,
        this.profileUrl,
        this.introVideoUrl,
        this.introVideoCoverUrl,
        this.description,
        this.challenger,
        this.mobile,
        this.facebookUrl,
        this.youtubeUrl,
        this.tweeterUrl,
        this.clubhouseUrl,
        this.instagramUrl,
        this.linkedinUrl,
        this.like});

  Challenger.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    email = json['email'];
    profileUrl = json['profileUrl'];
    introVideoUrl = json['introVideoUrl'];
    introVideoCoverUrl = json['introVideoCoverUrl'];
    description = json['description'];
    challenger = json['challenger'];
    mobile = json['mobile'];
    facebookUrl = json['facebookUrl'];
    youtubeUrl = json['youtubeUrl'];
    tweeterUrl = json['tweeterUrl'];
    clubhouseUrl = json['clubhouseUrl'];
    instagramUrl = json['instagramUrl'];
    linkedinUrl = json['linkedinUrl'];
    like = json['like'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['email'] = this.email;
    data['profileUrl'] = this.profileUrl;
    data['introVideoUrl'] = this.introVideoUrl;
    data['introVideoCoverUrl'] = this.introVideoCoverUrl;
    data['description'] = this.description;
    data['challenger'] = this.challenger;
    data['mobile'] = this.mobile;
    data['facebookUrl'] = this.facebookUrl;
    data['youtubeUrl'] = this.youtubeUrl;
    data['tweeterUrl'] = this.tweeterUrl;
    data['clubhouseUrl'] = this.clubhouseUrl;
    data['instagramUrl'] = this.instagramUrl;
    data['linkedinUrl'] = this.linkedinUrl;
    data['like'] = this.like;
    return data;
  }
}
