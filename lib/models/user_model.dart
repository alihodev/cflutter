class UserModel {
  int? id;
  String? username;
  String? email;
  String? phone;
  String? intro;
  String? profileUrl;
  List<SocialMedias>? socialMedias;
  String? summary;

  UserModel(
      {this.id,
        this.username,
        this.email,
        this.phone,
        this.intro,
        this.profileUrl,
        this.socialMedias,
        this.summary});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    intro = json['intro'];
    profileUrl = json['profileUrl'];
    if (json['socialMedias'] != null) {
      socialMedias = <SocialMedias>[];
      json['socialMedias'].forEach((v) {
        socialMedias!.add(SocialMedias.fromJson(v));
      });
    }
    summary = json['summary'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['email'] = email;
    data['phone'] = phone;
    data['intro'] = intro;
    data['profileUrl'] = profileUrl;
    if (socialMedias != null) {
      data['socialMedias'] = socialMedias!.map((v) => v.toJson()).toList();
    }
    data['summary'] = summary;
    return data;
  }
}

class SocialMedias {
  String? media;
  String? link;

  SocialMedias({this.media, this.link});

  SocialMedias.fromJson(Map<String, dynamic> json) {
    media = json['media'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['media'] = media;
    data['link'] = link;
    return data;
  }
}
