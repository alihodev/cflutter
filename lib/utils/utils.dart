import 'package:intl/intl.dart';


class Utils {
  static String formatCalendarDatePicker(DateTime dateTime) {
    final DateFormat formatter = DateFormat('MMM d, yyyy');
    return formatter.format(dateTime);
  }

  static String formatCalendarDatePickerForDailyReport(DateTime dateTime) {
    final DateFormat formatter = DateFormat('MMMM/dd/yyyy');
    return formatter.format(dateTime);
  }

  static String convertDateToMonthAndYear(double days) {
    return "${days.toInt()} days";
    int intDays = days.toInt();
    int year = days ~/ 365;
    int remainderDays = (intDays % 365);
    int month = remainderDays ~/ 30;
    remainderDays = remainderDays % 30;
    return "$year years and $month month and $remainderDays days";
  }
}