const amplifyconfig = '''{
    "UserAgent": "aws-amplify-cli/2.0",
    "Version": "1.0",
    "api": {
        "plugins": {
            "awsAPIPlugin": {
                "ChallengersChat": {
                    "endpointType": "GraphQL",
                    "endpoint": "https://fz3dzccyhbexlkhexpqpcgrkzu.appsync-api.ca-central-1.amazonaws.com/graphql",
                    "region": "ca-central-1",
                    "authorizationType": "API_KEY",
                    "apiKey": "da2-qhn3d3f4zjci5e2vgley7krtne"
                }
            }
        }
    },
    "auth": {
        "plugins": {
            "awsCognitoAuthPlugin": {
                "UserAgent": "aws-amplify-cli/0.1.0",
                "Version": "0.1.0",
                "IdentityManager": {
                    "Default": {}
                },
                "AppSync": {
                    "Default": {
                        "ApiUrl": "https://fz3dzccyhbexlkhexpqpcgrkzu.appsync-api.ca-central-1.amazonaws.com/graphql",
                        "Region": "ca-central-1",
                        "AuthMode": "API_KEY",
                        "ApiKey": "da2-qhn3d3f4zjci5e2vgley7krtne",
                        "ClientDatabasePrefix": "ChallengersChat_API_KEY"
                    },
                    "ChallengersChat_AWS_IAM": {
                        "ApiUrl": "https://fz3dzccyhbexlkhexpqpcgrkzu.appsync-api.ca-central-1.amazonaws.com/graphql",
                        "Region": "ca-central-1",
                        "AuthMode": "AWS_IAM",
                        "ClientDatabasePrefix": "ChallengersChat_AWS_IAM"
                    }
                },
                "CredentialsProvider": {
                    "CognitoIdentity": {
                        "Default": {
                            "PoolId": "ca-central-1:3211cc56-a6b3-40ba-8f32-d1fcf1075b1f",
                            "Region": "ca-central-1"
                        }
                    }
                },
                "CognitoUserPool": {
                    "Default": {
                        "PoolId": "ca-central-1_iZRQ3IurS",
                        "AppClientId": "6otlbcm8dbipk3mqj1qh8v5ajj",
                        "Region": "ca-central-1"
                    }
                },
                "Auth": {
                    "Default": {
                        "authenticationFlowType": "USER_SRP_AUTH",
                        "mfaConfiguration": "OFF",
                        "mfaTypes": [
                            "SMS"
                        ],
                        "passwordProtectionSettings": {
                            "passwordPolicyMinLength": 8,
                            "passwordPolicyCharacters": [
                                "REQUIRES_LOWERCASE",
                                "REQUIRES_NUMBERS",
                                "REQUIRES_SYMBOLS",
                                "REQUIRES_UPPERCASE"
                            ]
                        },
                        "signupAttributes": [],
                        "socialProviders": [],
                        "usernameAttributes": [],
                        "verificationMechanisms": [
                            "EMAIL"
                        ]
                    }
                }
            }
        }
    }
}''';
